<?php
$page = 'home';
include 'header.php';
if ($_SESSION["role"] == "Super Admin") {
    $sql1 = "SELECT count(paid) AS total FROM users WHERE `admin_id` = '0' AND `role` = 'Admin'";
    $sql2 = "SELECT count(paid) AS total FROM users WHERE `admin_id` = '0' AND `paid` = '1' AND `role` = 'Admin'";
    $sql3 = "SELECT count(paid) AS total FROM users WHERE `admin_id` = '0' AND `paid` = '0' AND `role` = 'Admin'";
    $sql5 = "SELECT count(subscription_expired) AS total FROM users WHERE `admin_id` = '0' AND `subscription_expired` = '1' AND `role` = 'Admin'";
    $sql6 = "SELECT count(is_block) AS total FROM users WHERE `admin_id` = '0' AND `is_block` = '1' AND `role` = 'Admin'";
    $result1 = $con->query($sql1);
    $result2 = $con->query($sql2);
    $result3 = $con->query($sql3);
    $result5 = $con->query($sql5);
    $result6 = $con->query($sql6);
    $row_1 = $result1->fetch_assoc();
    $row_2 = $result2->fetch_assoc();
    $row_3 = $result3->fetch_assoc();
    $row_5 = $result5->fetch_assoc();
    $row_6 = $result6->fetch_assoc();
}
if ($_SESSION["role"] == "Admin") {
    $sql1 = "SELECT source FROM source WHERE source != '' AND `admin_id` = '$userID'";
    $sql2 = "SELECT id FROM users WHERE `role` = 'Agent' AND `admin_id` = '$userID'";
    $sql3 = "SELECT medium FROM lead_capture WHERE medium = 'Internet UPS' AND `admin_id` = '$userID'";
    $sql4 = "SELECT medium FROM lead_capture WHERE medium = 'Phone UPS' AND `admin_id` = '$userID'";
} else {
    $alias = $_SESSION['alais'];
    $sql1 = "SELECT source FROM source WHERE source != '' AND `admin_id` = '$adminID'";
    $sql3 = "SELECT medium FROM lead_capture WHERE medium = 'Internet UPS' AND `agent_id` = '$userID'";
    $sql4 = "SELECT medium FROM lead_capture WHERE medium = 'Phone UPS' AND `agent_id` = '$userID'";
}
$source = $con->query($sql1);
$agents = $con->query($sql2);
$internet = $con->query($sql3);
$phone = $con->query($sql4);
?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Home</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
      <?php if ($_SESSION["role"] == "Super Admin") {?>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=$row_1['total'];?></h3>
              <p>Customers</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-list"></i>
            </div>
            <a href="<?php echo $site_url ?>/pages/tables/view-admin.php" class="small-box-footer">
              More info
              <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?=$row_2['total'];?></h3>
              <p>Paid</p>
            </div>
            <div class="icon">
              <i class="fas fa-user-plus"></i>
            </div>
            <a href="<?php echo $site_url ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?=$row_3['total'];?></h3>
              <p>Unpaid</p>
            </div>
            <div class="icon">
              <i class="fas fa-user-times"></i>
            </div>
            <a href="<?php echo $site_url ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=$row_5['total'];?></h3>
              <p>Subscription Expired</p>
            </div>
            <div class="icon">
              <i class="fas fa-user-minus"></i>
            </div>
            <a href="<?php echo $site_url ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-secondary">
            <div class="inner">
              <h3><?=$row_6['total'];?></h3>
              <p>Blocked</p>
            </div>
            <div class="icon">
              <i class="fas fa-user-slash"></i>
            </div>
            <a href="<?php echo $site_url ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <?php } else {
    ?>
        <?php if ($_SESSION["role"] == "Admin") {?>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=$agents->num_rows?></h3>
              <p>Agents</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-list"></i>
            </div>
            <a href="<?php echo $site_url ?>/pages/forms/register-agent.php" class="small-box-footer">
              More info
              <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <?php }?>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?=$internet->num_rows?></h3>
              <p>Internet medium</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo $site_url ?>/pages/tables/agent-table.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-secondary">
            <div class="inner">
              <h3><?=$phone->num_rows?></h3>
              <p>Phone medium</p>
            </div>
            <div class="icon">
              <i class="ion ion-filing"></i>
            </div>
            <a href="<?php echo $site_url ?>/pages/tables/agent-table.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?=$source->num_rows?></h3>
              <p>Sources</p>
            </div>
            <div class="icon">
              <i class="ion ion-navigate"></i>
            </div>
            <a href="<?php echo $site_url ?>/pages/tables/source-codes.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <?php if ($_SESSION["role"] == "Admin") {?>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3 class="white">BDC</h3>
              <p class="white">BDC Performance</p>
            </div>
            <div class="icon">
              <i class="fas fa-chart-bar"></i>
            </div>
            <a href="<?php echo $site_url ?>/pages/tables/bdc-table.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php }?>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3 class="white">Notification</h3>
              <p class="white">Manage Notification</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
            <a href="<?php echo $site_url ?>/pages/forms/notification.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3 class="white">Coming Soon</h3>
              <p class="white">Service Mining Tracker</p>
            </div>
            <div class="icon">
              <i class="fa fa-spinner"></i>
            </div>
            <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php }?>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include 'footer.php';?>