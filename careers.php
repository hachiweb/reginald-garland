<?php
$page = 'careers';
error_reporting(0);
session_start();
if ($_SESSION["role"] == !"") {
  include('header.php');
} else {
  include('pub-header.php');
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Careers</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Careers</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-12 card card-success card-outline">
            <div class="card-body">
              <div class="text-muted mt-3">
              If you enjoy talking with people on the phone and have great energy, we are hiring! If you feel as though you would be a great fit at Garland Pro Solutions, LP, please do not hesitate to email us at <a href="mailto:garlandprosolutions@gmail.com">Garlandprosolutions@gmail.com.</a> Do not forget to attach your email address when you send us an email. We usually respond within one business-day.
              </div>
            </div>
            <!-- /.card -->
          </div>
          <div class="col-12">
            <div class="callout callout-info">
              <h5> The desired qualities and job experiences include, but are not limited to:</h5>
              <ul class="ml-4">
                <li>Sales experience (preferably auto sales)</li>
                <li>Call Center experience</li>
                <li>Management experience in sales, computer or auto industry</li>
                <li>Proficient in Microsoft Office programs, i.e. Excel, Word</li>
                <li>Bilingual in Spanish is a plus</li>
              </ul>
            </div>
            <div class="callout callout-warning">
              <h5> Must have the following requirements:</h5>
              <ul class="ml-4">
                <li>Be a United States Citizen</li>
                <li>Be at least 18 years of age</li>
                <li>Be able to speak, read, and write English fluently</li>
                <li>Have the ability to uphold and maintain basic bodily hygiene</li>
                <li>Communicate concisely and clearly to customers on the phone or internet daily</li>
                <li>Be self-motivated & driven</li>
                <li>Have great energy</li>
                <li>Have experience with working with computers</li>
                <li>Possess or acquire the ability to issue and receive payment for contracts regarding proprietary lead management services</li>
                </ul>
            </div>
            <div class="col-12 card card-warning card-outline">
              <div class="card-body">
                <div class="text-muted text-center mt-3">
                *We do offer extensive training on computer programs and provide proprietary certification.*
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('footer.php'); ?>
