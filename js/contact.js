$(document).ready(function () {
    $('#contact').on('submit', function (e) {
        e.preventDefault();
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });
        var formData = new FormData(this);
        var urlsite = document.location.origin+"/contact-sub.php"
        $.ajax({
            type: "POST",
            url: urlsite,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data == 1) {
                    Toast.fire({
                        type: 'success',
                        title: 'Thank you for contacting us. You are very important to us, all information received will always remain confidential. We will contact you as soon as we review your message.',
                    });
                }
                // else {
                //     Toast.fire({
                //         type: 'error',
                //         title: 'Something went wrong. Please try again'
                //     });
                // }
            }
        });
    });
});