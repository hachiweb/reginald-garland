/* custom js */

$(document).ready(function(){
	$('.remove').delay(3000).fadeOut();
	console.log($("#field"));
	$(document).on("input click","#notes4_ifr",function(e) {
		alert('here');
        el = $(this);
        if(el.val().length >= 500){
            el.val( el.val().substr(0, 500) );
        } else {
            $("#charNum").text(500-el.val().length);
        }
	});

    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });

	$(document).on("click",".fa-eye",function(e) {
		e.preventDefault();
		var pashow = $("#pashow").val();
		$("#newps").text(pashow);
		$(".fa-eye").removeClass("fa-eye").addClass("fa-eye-slash");
	  });
	  $(document).on("click",".fa-eye-slash",function(e) {
		e.preventDefault();
		$("#newps").text("************");
		$(".fa-eye-slash").removeClass("fa-eye-slash").addClass("fa-eye");
	  });
	$(document).on("click",".iti__country",function() {
		var contcode = ($(this).find(".iti__dial-code").text());
		$("#contcode").val(contcode);
	  });
	if ($(".appt_status").val()=="") {
		var appt_option = '<option class="statusrmv" value="Not Buying">Not Buying</option>'
			+'<option class="statusrmv" value="Flaky">Flaky</option>';
			$(".status").find(".statusrmv").remove();
			$(".status").append(appt_option);
	}
	if ($(".appt_status").val()=="SOLD") {
		var appt_option = '<option class="statusrmv" value="Purchased">Purchased</option>';
			$(".status").find(".statusrmv").remove();
			$(".status").append(appt_option);
	}
	if ($(".appt_status").val()=="SHOW") {
		var appt_option = '<option class="statusrmv" value="Cold">Cold</option>'
			+'<option class="statusrmv" value="Hot">Hot</option>'
			+'<option class="statusrmv" value="Came In/Did Not Buy">Came In/Did Not Buy</option>';
			$(".status").find(".statusrmv").remove();
			$(".status").append(appt_option);
	}
	if ($(".appt_status").val()=="SET") {
		var appt_option = '<option class="statusrmv" value="Cold">Cold</option>'
			+'<option class="statusrmv" value="Hot">Hot</option>'
			+'<option class="statusrmv" value="Came In/Did Not Buy">Came In/Did Not Buy</option>';
			$(".status").find(".statusrmv").remove();
			$(".status").append(appt_option);
	}
	$(document).on("change",".appt_status",function(){
		if ($(this).val()=="SOLD") {
			var appt_option = '<option class="statusrmv" value="Purchased">Purchased</option>';
			$(".status").find(".statusrmv").remove();
			$(".status").append(appt_option);
		}
		if ($(this).val()=="SHOW") {
			var appt_option = '<option class="statusrmv" value="Cold">Cold</option>'
			+'<option class="statusrmv" value="Hot">Hot</option>'
			+'<option class="statusrmv" value="Came In/Did Not Buy">Came In/Did Not Buy</option>';
			$(".status").find(".statusrmv").remove();
			$(".status").append(appt_option);
		}
		if ($(this).val()=="SET") {
			var appt_option = '<option class="statusrmv" value="Cold">Cold</option>'
			+'<option class="statusrmv" value="Hot">Hot</option>'
			+'<option class="statusrmv" value="Came In/Did Not Buy">Came In/Did Not Buy</option>';
			$(".status").find(".statusrmv").remove();
			$(".status").append(appt_option);
		}
		if ($(this).val()=="") {
			var appt_option = '<option class="statusrmv" value="Not Buying">Not Buying</option>'
			+'<option class="statusrmv" value="Flaky">Flaky</option>';
			$(".status").find(".statusrmv").remove();
			$(".status").append(appt_option);
		}
	})
	$(".iti--allow-dropdown").addClass("w-100");
	$('.dataTables_filter').addClass('float-right');
	$('.dataTables_info').addClass('mt-3');
	$('.paging_simple_numbers').addClass('float-right mt-3');
	$('input[name=agent-radio]').change(function() {
		if($('#r1').is(':checked')) { 
			$('.form-dv').removeClass('d-block');
			$('.form-dv.form-internet').addClass('d-block');
		}
		if($('#r2').is(':checked')) { 
			$('.form-dv').removeClass('d-block');
			$('.form-dv.form-phone').addClass('d-block');
		}
		if($('#r3').is(':checked')) { 
			$('.form-dv').removeClass('d-block');
			$('.form-dv.form-extended').addClass('d-block');
		}
		if($('#r4').is(':checked')) { 
			$('.form-dv').removeClass('d-block');
			$('.form-dv.form-reviews').addClass('d-block');
		}
	});

	$('input[name=agent-table-radio]').change(function() {
		if($('#t1').is(':checked')) {
			$('.table-dv').removeClass('d-block');
			$('.table-dv.table-internet').addClass('d-block');
		}
		if($('#t2').is(':checked')) {
			$('.table-dv').removeClass('d-block');
			$('.table-dv.table-phone').addClass('d-block');
		}
		if($('#t3').is(':checked')) {
			$('.table-dv').removeClass('d-block');
			$('.table-dv.table-extended').addClass('d-block');
		}
		if($('#t4').is(':checked')) {
			$('.table-dv').removeClass('d-block');
			$('.table-dv.table-reviews').addClass('d-block');
		}
		if($('#t5').is(':checked')) {
			$('.table-dv').removeClass('d-block');
			$('.table-dv.table-reviews-ex').addClass('d-block');
		}
	});

	$(".totals-area").show();
	$(".bonus-area").hide();
	$(".internet-performance-area").hide();
	$(".phone-performance-area").hide();

	$('input[name=multi-table-radio]').change(function() {
		if($('#r1').is(':checked')) { 
			
			$(".bonus-area").hide();
			$(".internet-performance-area").hide();
			$(".phone-performance-area").hide();

			$(".totals-area").show();
			
			$(".internet-area").show();
			$(".phone-area").hide();
			$(".extended-area").hide();
			$("#r5").prop("checked", true);
		}
		if($('#r2').is(':checked')) { 
			$(".totals-area").hide();
			$(".bonus-area").show();
			$(".internet-performance-area").hide();
			$(".phone-performance-area").hide();
		}
		if($('#r3').is(':checked')) { 
			$(".totals-area").hide();
			$(".bonus-area").hide();
			$(".internet-performance-area").show();
			$(".phone-performance-area").hide();
		}
		if($('#r4').is(':checked')) { 
			$(".totals-area").hide();
			$(".bonus-area").hide();
			$(".internet-performance-area").hide();
			$(".phone-performance-area").show();
		}
	});


	$(".internet-area").show();
	$(".phone-area").hide();
	$(".extended-area").hide();
	
	
	$('input[name=totals-radio]').change(function() {
		if($('#r5').is(':checked')) { 
			$(".internet-area").show();
			$(".phone-area").hide();
			$(".extended-area").hide();
			
		}
		if($('#r6').is(':checked')) { 
			$(".internet-area").hide();
			$(".phone-area").show();
			$(".extended-area").hide();
			
		}
		if($('#r7').is(':checked')) { 
			$(".internet-area").hide();
			$(".phone-area").hide();
			$(".extended-area").show();
			
		}
	});
	
	
	$(".set-internet").show();
	$(".set-phone").hide();
	$('input[name=set-goal-radio]').change(function() {
		if($('#r1').is(':checked')) { 
			
			$(".set-internet").show();
			$(".set-phone").hide();
		}
		if($('#r2').is(':checked')) { 
			$(".set-internet").hide();
			$(".set-phone").show();
		}

	});


	$('input[name=lead-requests-radio]').change(function() {
		if($('#r1').is(':checked')) { 
			
			$(".internet-lead-requests").show();
			$(".phone-lead-requests").hide();
			$(".extended-lead-requests").hide();
			$(".review-lead-requests").hide();


		}
		if($('#r2').is(':checked')) { 
			$(".internet-lead-requests").hide();
			$(".phone-lead-requests").show();
			$(".extended-lead-requests").hide();
			$(".review-lead-requests").hide();
		}
		if($('#r3').is(':checked')) { 
			$(".internet-lead-requests").hide();
			$(".phone-lead-requests").hide();
			$(".extended-lead-requests").show();
			$(".review-lead-requests").hide();
		}
		if($('#r4').is(':checked')) { 
			$(".internet-lead-requests").hide();
			$(".phone-lead-requests").hide();
			$(".extended-lead-requests").hide();
			$(".review-lead-requests").show();
		}
	});


	
	$('input[name=my-leads-radio]').change(function() {
		if($('#r1').is(':checked')) { 
			
			$(".approved").show();
			$(".discarded").hide();
			$(".pending").hide();




		}
		if($('#r2').is(':checked')) { 
			$(".approved").hide();
			$(".discarded").show();
			$(".pending").hide();

		}
		if($('#r3').is(':checked')) { 
			$(".approved").hide();
			$(".discarded").hide();
			$(".pending").show();

		}

	});


	// set internet agent id
	var internet_agent_field = $('#internet_agent_field').find('option:selected'); 
	var internet_agent_field_val = internet_agent_field.attr("agent_id"); 
	$('#internet_agent_id').val(internet_agent_field_val); 
	$('#internet_agent_field').change(function(){
		var element = $(this).find('option:selected'); 
		var agent_id = element.attr("agent_id"); 
		$('#internet_agent_id').val(agent_id); 
	});


	// set phone agent id
	var phone_agent_field = $('#phone_agent_field').find('option:selected'); 
	var phone_agent_field_val = phone_agent_field.attr("agent_id"); 
	$('#phone_agent_id').val(phone_agent_field_val); 
	$('#phone_agent_field').change(function(){
		var element = $(this).find('option:selected'); 
		var agent_id = element.attr("agent_id"); 
		$('#phone_agent_id').val(agent_id); 
	});

	//set warranties agent_id
	var warranty_agent_field = $('#warranty_agent_field').find('option:selected'); 
	var warranty_agent_field_val = warranty_agent_field.attr("agent_id"); 
	$('#warranty_agent_id').val(warranty_agent_field_val); 
	$('#warranty_agent_field').change(function(){
		var element = $(this).find('option:selected'); 
		var agent_id = element.attr("agent_id"); 
		$('#warranty_agent_id').val(agent_id); 
	});

	//set review agent_id
	var review_agent_field = $('#review_agent_field').find('option:selected'); 
	var review_agent_field_val = review_agent_field.attr("agent_id"); 
	$('#review_agent_id').val(review_agent_field_val);
	$('#review_agent_field').change(function(){
		var element = $(this).find('option:selected'); 
		var agent_id = element.attr("agent_id"); 
		$('#review_agent_id').val(agent_id); 
	}); 

	$('#resbtn1').click(function() {
		toastr.success('Profile Picture Has Changed Successfully.')
	});
	$('#resbtn2').click(function() {
		toastr.error('Opps! Something Went Wrong.')
	});
	$('#resbtn3').click(function() {
		toastr.success('Profile Picture Has Removed Successfully.')
	});
	$('#resbtn4').click(function() {
		toastr.success('Details Has Saved Successfully.')
	});
	$('#resbtn5').click(function() {
		toastr.success('Email Varified Successfully.')
	});
	if ($("#res").val()=="1") {
		$("#resbtn1").click();
	}
	if ($("#res").val()=="0") {
		$("#resbtn2").click();
	}
	if ($("#res").val()=="3") {
		$("#resbtn3").click();
	}
	if ($("#res").val()=="4") {
		$("#resbtn4").click();
	}
	if ($("#res").val()=="5") {
		$("#resbtn5").click();
	}
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});


	// form validations

	// internet-form
	$("#internet-form").submit(function(){
		var agent_name = $('select[name=name]').val();
		var lead_name = $('input[name="lead_name"]').val();
		var appt_set = $('select[name="appt_set"]').val();
		var appt_show = $('select[name="appt_show"]').val();
		var appt_sold = $('select[name="appt_sold"]').val();
		var source = $('select[name="source"]').val();
		var status = $('select[name="status"]').val();
		var filled_out = $('select[name="filled_out"]').val();

		$('.error').remove();
		if(agent_name == ''){
			$(this).find('select[name="name"]').after('<div class="error">Please Enter Valid Data</div>');
		}

		if(lead_name == ''){
			$(this).find('input[name="lead_name"]').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_set == ''){
			$(this).find('select[name="appt_set"]').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_show == ''){
			$(this).find('select[name="appt_show"]').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_sold == ''){
			$(this).find('select[name="appt_sold"]').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(source == ''){
			$(this).find('select[name="source"]').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(status == ''){
			$(this).find('select[name="status"]').after('<div class="error">Please Enter Valid Data</div>');
		}

		if($('.error').text() != ''){
			return false;
		}
		else{
			return true;
		}
	});


	//phone-form
	$("#phone-form").submit(function(){
		var agent_name = $('#phone_agent_field').val();
		var lead_name = $('#phone_lead_name').val();
		var appt_set = $('#phone_appt_set').val();
		var appt_show = $('#phone_appt_show').val();
		var appt_sold = $('#phone_appt_sold').val();
		var status = $('#phone_status').val();

		$('.error').remove();		
		if(agent_name == ''){
			$(this).find('#phone_agent_field').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(lead_name == ''){
			$(this).find('#phone_lead_name').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_set == ''){
			$(this).find('#phone_appt_set').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_show == ''){
			$(this).find('#phone_appt_show').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_sold == ''){
			$(this).find('#phone_appt_sold').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(status == ''){
			$(this).find('#phone_status').after('<div class="error">Please Enter Valid Data</div>');
		}
		
		if($('.error').text() != ''){
			return false;
		}
		else{
			return true;
		}
	});


	// extended-form
	$("#extended-form").submit(function(){
		var agent_name = $('#warranty_agent_field').val();
		var lead_name = $('#extended_lead_name').val();
		var appt_set = $('#extended_appt_set').val();
		var appt_show = $('#extended_appt_show').val();
		var appt_sold = $('#extended_appt_sold').val();
		var status = $('#extended_status').val();

		$('.error').remove();		
		if(agent_name == ''){
			$(this).find('#warranty_agent_field').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(lead_name == ''){
			$(this).find('#extended_lead_name').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_set == ''){
			$(this).find('#extended_appt_set').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_show == ''){
			$(this).find('#extended_appt_show').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(appt_sold == ''){
			$(this).find('#extended_appt_sold').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(status == ''){
			$(this).find('#extended_status').after('<div class="error">Please Enter Valid Data</div>');
		}
		
		if($('.error').text() != ''){
			return false;
		}
		else{
			return true;
		}
	});


	// reviews-form
	$("#reviews-form").submit(function(){
		var agent_name = $('#review_agent_field').val();
		var lead_name = $('#review_lead_name').val();
		var filled_out = $('#review_filled_out').val();
		var status = $('#review_status').val();

		$('.error').remove();
		if(agent_name == ''){
			$(this).find('#review_agent_field').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(lead_name == ''){
			$(this).find('#review_lead_name').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(filled_out == ''){
			$(this).find('#review_filled_out').after('<div class="error">Please Enter Valid Data</div>');
		}
		if(status == ''){
			$(this).find('#review_status').after('<div class="error">Please Enter Valid Data</div>');
		}
		
		if($('.error').text() != ''){
			return false;
		}
		else{
			return true;
		}
	});


	// contact form
	$("#contact").submit(function(){
		var first_name = $('#first_name').val();
		var last_name = $('#last_name').val();
		var email = $('#email').val();
		var phone = $('#phone').val();
		var company = $('#company').val();

		var name_regex = /^[A-Za-z]+$/;
		var email_regex = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

		$('.error').remove();
		if (!first_name.match(name_regex) || first_name == ''){
			$(this).find('#first_name').after('<div class="error">Please Enter Frst Name</div>');
		}
		if(!last_name.match(name_regex) || last_name == ''){
			$(this).find('#last_name').after('<div class="error">Please Enter Last Name</div>');
		}
		if(!email.match(email_regex) || email == ''){
			$(this).find('#email').after('<div class="error">Please Enter Valid Email Address</div>');
		}
		if(phone == ''){
			$(this).find('#phone').after('<div class="error">Please Enter Phone Number</div>');
		}
		if(company == ''){
			$(this).find('#company').after('<div class="error">Please Enter Company</div>');
		}
		

		if($('.error')>0){
			return true;
		}
		else{
			return false;
		}
	});

	
	jQuery("#registerAgent").submit(function(e){
		//e.preventDefault();
		var first_name = $('#first_nameee').val();
		var last_name = $('#last_namee').val();
		var email = $('input[name="username"]').val();
		var password = $('#agent_password').val();
		var confirm_password = $('#agent_confirm_password').val();

		var name_regex = /^[A-Za-z]+$/;
		var email_regex = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

		$('.error').remove();
		if (!first_name.match(name_regex) || first_name == ''){
			$(this).find('#first_nameee').after('<div class="error">Please Enter First Name</div>');
		}
		if(!last_name.match(name_regex) || last_name == ''){
			$(this).find('#last_namee').after('<div class="error">Please Enter Last Name</div>');
		}
		if(!email.match(email_regex) || email == ''){
			$(this).find('input[name="username"]').after('<div class="error">Please Enter Valid Email Address</div>');
		}
		if(password == ''){
			$(this).find('#agent_password').after('<div class="error">Please Enter Your Password</div>');
		}
		if(password != confirm_password){
			$(this).find('#agent_confirm_password').after('<div class="error">Please Enter Your Same Password</div>');
		}

		if($('.error').text() != ''){
			return false;
		}
		else{
			return true;
		}
	});

    



	// document close
});

function edit(_id,_medium) {
	$.ajax({
		method: "POST",
		url: "../forms/set-session-for-agent-edit.php",
		data: {id : _id}
	}).done(function( _res ) {
		var res = JSON.parse(_res);
		if (res.status == "200") {

			window.location.assign("/pages/forms/agent-edit.php?from=requests&medium="+_medium);
		} else {
			window.location.assign("agent-table.php?success=err");
		}
	});

}

function hideAll(element) {
	element.value = "";
	$("#searchLead").hide();
	$("#searchAgent").hide();

	if (element.id == "lead") {
		findParams("all","searchLead");
	}
	if (element.id == "agent") {
		findParams("all","searchAgent");
	}

}

  // search

  
// function findParams(str,id) {

// 	$("#"+id).show();
// 	if (str.length==0) { 
// 	  document.getElementById(id).innerHTML="";
// 	  document.getElementById(id).style.border="0px";
// 	  return;
// 	}
// 	if (window.XMLHttpRequest) {
// 	  // code for IE7+, Firefox, Chrome, Opera, Safari
// 	  xmlhttp=new XMLHttpRequest();
// 	} else {  // code for IE6, IE5
// 	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
// 	}
// 	xmlhttp.onreadystatechange=function() {
// 	  if (this.readyState==4 && this.status==200) {
// 		document.getElementById(id).innerHTML=this.responseText;
// 		document.getElementById(id).style.border="1px solid #A5ACB2";
// 	  }
// 	}
// 	if (id == "searchLead") {
// 		xmlhttp.open("GET","sub-search.php?q="+str+"&target=lead",true);	
// 	}
// 	if (id == "searchAgent") {
// 		xmlhttp.open("GET","sub-search.php?q="+str+"&target=agent",true);	
// 	}
// 	if (id == "searchMedium") {
// 		xmlhttp.open("GET","sub-search.php?q="+str+"&target=medium",true);	
// 	}
// 	if (id == "searchStatus") {
// 		xmlhttp.open("GET","sub-search.php?q="+str+"&target=status",true);	
// 	}
// 	xmlhttp.send();
//   }

// get params value
// function selectVal(data,target) {
// 	if (target == "lead") {
// 		document.getElementById("lead").value = data;
// 		$("#searchLead").hide();
// 	}
// 	if (target == "agent") {
// 		document.getElementById("agent").value = data;
// 		$("#searchAgent").hide();
// 	}
// 	if (target == "medium") {
// 		document.getElementById("medium").value = data;
// 		$("#searchMedium").hide();
// 	}
// 	if (target == "status") {
// 		document.getElementById("status").value = data;
// 		$("#searchStatus").hide();
// 	}
// }
//  // final search 

//  function search() {
// 	lead = $("#lead").val();
// 	agent = $("#agent").val();
// 	if (lead == "") {
// 		$("#lead").val("All");
// 	}
// 	if (agent == "") {
// 		$("#agent").val("All");
// 	}

// 	var _data = {
// 	  "action" : "filter Search",
// 	  "lead" : $("#lead").val(),
// 	  "agent" : $("#agent").val(),
// 	  "medium" : $("#medium").val(),
// 	  "status" : $("#status").val(),
// 	};
// 	$.ajax({
// 		url : "/pages/tables/sub-search.php",
// 		method : "POST",
// 		data : _data
// 	}).done((res)=>{
// 	  $("#searchForm").hide();
// 	  $("#response").html(res);
// 	  $("#newSearch").show();

// 	});
// return false;
//  }

//  function showSearch() {
// 	$("#response").html(" ");
// 	 $("#newSearch").hide();
// 	 $("#searchForm").show();

//  }


function approve(_id) {
	$.ajax({
		url: "/pages/forms/sub-lead-requests.php",
		method: "POST",
		data: {id:_id,action:"approved"}
	}).done((res)=>{
		if (res == 1) {
			alert("Approved");
			window.location.reload();
		} else {
			alert("Something Went Wrong !");
		}
	});
}

function discard(_id) {
	if (confirm("Are you sure to Discarded this !")) {
		$.ajax({
			url: "/pages/forms/sub-lead-requests.php",
			method: "POST",
			data: {id:_id,action:"discard"}
		}).done((res)=>{
			if (res == 1) {

				window.location.reload();
			} else {
				alert("Something Went Wrong !");
			}
		});
	}
}

function delRqt(_id) {
	if (confirm("Are you sure to Delete this !")) {
		$.ajax({
			url: "/pages/forms/sub-lead-requests.php",
			method: "POST",
			data: {id:_id,action:"delete"}
		}).done((res)=>{
			if (res == 1) {

				window.location.reload();
			} else {
				alert("Something Went Wrong !");
			}
		});
	}
}





