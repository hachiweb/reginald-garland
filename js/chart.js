
// totals 
var totals = [{
    values: [
        stats.internet_totals.total_of_leads,
         stats.internet_totals.total_set,
          stats.internet_totals.total_show,
           stats.internet_totals.total_sold
        ],
    labels: ['Lead', 'Set', 'Show', 'Sold' ],
    domain: {column: 0},
    name: 'Internet',
    hoverinfo: 'label+percent+name',
    hole: .4,
    type: 'pie'
  },{
    values: [
        stats.phone_totals.total_leads,
         stats.phone_totals.total_set,
          stats.phone_totals.total_show,
           stats.phone_totals.total_sold
        ],
    labels: ['Lead', 'Set', 'Show', 'Sold' ],
    text: 'CO2',
    textposition: 'inside',
    domain: {column: 1},
    name: 'Phone',
    hoverinfo: 'label+percent+name',
    hole: .4,
    type: 'pie'
  }];
  
  var layout = {
    title: 'Totals',
    annotations: [
      {
        font: {
          size: 20
        },
        showarrow: false,
        text: 'INT',
        x: 0.17,
        y: 0.5
      },
      {
        font: {
          size: 20
        },
        showarrow: false,
        text: 'PH',
        x: 0.82,
        y: 0.5
      }
    ],
    height: 400,
    width: 600,
    showlegend: false,
    grid: {rows: 1, columns: 2}
  };
  
  Plotly.newPlot('total', totals, layout);



// stats


  var internet = {
    x: ['Set/Lead', 'Show/Set', 'Sold/Show', 'Closing Ratio'],
    y: [stats.internet_totals.total_set/stats.internet_totals.total_of_leads,
         stats.internet_totals.total_show/stats.internet_totals.total_set, 
         stats.internet_totals.total_sold/stats.internet_totals.total_show,
         stats.internet_totals.total_sold/stats.internet_totals.total_of_leads
        ],
    name: 'Internet %',
    type: 'bar'
  };
  
  var phone = {
    x: ['Set/Lead', 'Show/Set', 'Sold/Show', 'Closing Ratio'],
    y: [stats.phone_totals.total_set/stats.phone_totals.total_leads,
        stats.phone_totals.total_show/stats.phone_totals.total_set, 
        stats.phone_totals.total_sold/stats.phone_totals.total_show,
        stats.phone_totals.total_sold/stats.phone_totals.total_leads
       ],
    name: 'Phone %',
    type: 'bar'
  };
  
  var data = [internet, phone];
  
  var layout = {barmode: 'group'};
  
  Plotly.newPlot('stats', data, layout);