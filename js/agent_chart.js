

//console.log(raw_data);
// internet 
var data = [{
  values: [raw_data.internet_totals.set,
     raw_data.internet_totals.show,
      raw_data.internet_totals.sold
    ],

  labels: ['SET', 'SHOW', 'SOLD'],
  type: 'pie'
}];

var layout = {
  height: 400,
  width: 500
};

Plotly.newPlot('internet', data, layout);



// phone 
var data = [{
  values: [raw_data.phone_totals.leads, 
    raw_data.phone_totals.set,
     raw_data.phone_totals.show,
     raw_data.phone_totals.split_show,
      raw_data.phone_totals.split_sold, 
      raw_data.phone_totals.sold
    ],

  labels: ['LEADS','SET', 'SHOW', 'SPLIT-SHOW' ,'SPLI-SOLD', 'SOLD'],
  type: 'pie'
}];

var layout = {
  height: 400,
  width: 500
};

Plotly.newPlot('phone', data, layout);



// extended 
var data = [{
  values: [raw_data.Extended_totals.set,
     raw_data.Extended_totals.show,
      raw_data.Extended_totals.sold
    ],

  labels: ['SET', 'SHOW', 'SOLD'],
  type: 'pie'
}];

var layout = {
  height: 400,
  width: 500
};

Plotly.newPlot('extended', data, layout);



// bonus 

var show = Number(raw_data.internet_totals.show) + Number(raw_data.phone_totals.show) + Number(raw_data.Extended_totals.show);
var sold = Number(raw_data.internet_totals.sold) + Number(raw_data.phone_totals.sold);


var xValue = ['Set', 'Show', 'Sold' ,'Warranties','Reviews'];

var yValue = [0,
     show*10,
      sold*20,
       raw_data.Extended_totals.sold*50,
        raw_data.five_star_totals.yes*5
    ];

var trace1 = {
  x: xValue,
  y: yValue,
  type: 'bar',
  text: yValue.map(String),
  textposition: 'auto',
  hoverinfo: 'none',
  marker: {
    color: 'rgb(0,162,232)',
    opacity: 0.6,
    line: {
      color: 'rgb(10,22,32)',
      width: 1.5
    }
  }
};

var data = [trace1];

var layout = {
  title: 'Bonus Payout Money (in $)'
};

Plotly.newPlot('bonus', data, layout);


// company goal internet total 

var company = {
    x: ['SET/LEADS', 'SHOW/SET', 'SOLD/ SET', 'SOLD/SHOW', 'SOLD'],
    y: [
      goals.i_set_lead,
      goals.i_show_set,
      goals.i_sold_set,
      goals.i_sold_show,
      goals.i_sold
        ],
    type: 'bar',
    name: 'Company',
    marker: {
      color: 'rgb(49,130,189)',
      opacity: 0.7,
    }
  };
  
  var personal = {
    x: ['SET/LEADS', 'SHOW/SET', 'SOLD/ SET', 'SOLD/SHOW', 'SOLD'],
    y: [raw_data.internet_totals.set/raw_data.internet_totals.of_leads,
         raw_data.internet_totals.show/raw_data.internet_totals.set, 
         raw_data.internet_totals.sold/raw_data.internet_totals.set,
          raw_data.internet_totals.sold/raw_data.internet_totals.show, 
          raw_data.internet_totals.sold/raw_data.internet_totals.of_leads
        ],

    type: 'bar',
    name: 'Personal',
    marker: {
      color: 'rgb(100,100,100)',
      opacity: 0.5
    }
  };
  
  var data = [company, personal];
  
  var layout = {
    title: 'Company Goal for Internet Performance (in %)',
    xaxis: {
      tickangle: -45
    },
    barmode: 'group'
  };
  
  Plotly.newPlot('internetGoal', data, layout);


  // company goal phome total 

var company = {
    x: ['SET/LEADS', 'SHOW/SET', 'SOLD/ SET', 'SOLD/SHOW', 'SOLD'],
    y: [
      goals.p_set_lead,
      goals.p_show_set,
      goals.p_sold_set,
      goals.p_sold_show,
      goals.p_sold
        ],
    type: 'bar',
    name: 'Company',
    marker: {
      color: 'rgb(49,130,189)',
      opacity: 0.7,
    }
  };

  var personal = {
    x: ['SET/LEADS', 'SHOW/SET', 'SOLD/ SET', 'SOLD/SHOW', 'SOLD'],
    y: [raw_data.phone_totals.set/raw_data.phone_totals.leads,
         raw_data.phone_totals.show/raw_data.phone_totals.set, 
         raw_data.phone_totals.sold/raw_data.phone_totals.set,
          raw_data.phone_totals.sold/raw_data.phone_totals.show, 
          raw_data.phone_totals.sold/raw_data.phone_totals.leads
        ],

    type: 'bar',
    name: 'Personal',
    marker: {
      color: 'rgb(100,100,100)',
      opacity: 0.5
    }
  };
  
  var data = [company, personal];
  
  var layout = {
    title: 'Company Goal for Phone Performance (in %)',
    xaxis: {
      tickangle: -45
    },
    barmode: 'group'
  };
  
  Plotly.newPlot('phoneGoal', data, layout);