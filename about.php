<?php
$page = 'about';
error_reporting(0);
session_start();
if ($_SESSION["role"] == !"") {
  include('header.php');
} else {
  include('pub-header.php');
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>About</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">About</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="callout callout-info">
              <h5><i class="fas fa-question"></i> Who is the team comprised of:</h5>
              We have two owners, a robust team of international data specialists, and a sales team with over 30 plus years of procurement, automotive sales experience, and business development management and call center management experience.
            </div>
            <div class="callout callout-warning">
              <h5><i class="fa fa-map-marker"></i> Where are we:</h5>
              We are located in Valdosta, GA, of the United States of America.
            </div>
            <div class="callout callout-success">
              <h5><i class="fa fa-puzzle-piece"></i> What do we specialize in:</h5>
              We have a leadership team that has extensive knowledge in business, ranging from:international business practices, automotive trends, energy renewal, medical practices and more. Not only are we familiar with data tracking, but we also have experience with industry-leading CRM programs, predictive dialing programs, and automotive service mining tools.
            </div>
            <div class="callout callout-danger">
              <h5><i class="fa fa-check-circle"></i> Why choose us:</h5>
              This is, by far, most important question you can ask yourself as a business partner, manager, or owner. With the current trend in business becoming more dependent upon having a digital presence, how valuable is the data you have regarding your clients? The answer is, it is PRICELESS! At Garland Pro Solutions, we not only understand this fact, but have built an entire platform around this need to accurately track and monitor the performance of keeping up with your clientele. Once a company becomes absorbed by another more progressive entity, the only thing that the dissolved company can offer to its newly appointed parent-company is the on-hand merchandise and the data regarding their former customers. So, if you can collect and accurately manage this data you will be much more valuable and the rate in which your customer will defect will go down tremendously. Our team at Garland Pro Solutions also specializes in making sure that our program is progressive and adapts to the ever-changing environment of the digital age. Mainly, we understand that accessibility and simplicity equals money for the business who can provide both and consistent, but professional communication withyour customers equals lasting relationships; these lasting relationships translates into referrals, which allows your business to not just operate well, but to thrive!
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('footer.php'); ?>
