<?php
$page = 'terms-of-service';
error_reporting(0);
session_start();
if ($_SESSION["role"] == !"") {
  include('header.php');
} else {
  include('pub-header.php');
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>TERMS OF SERVICE</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">TERMS OF SERVICE</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-12 card card-success card-outline">
            <div class="card-body">
              <div class="text-muted mt-3">
              <p>Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the <a href="http://www.garlandprosolutions.net"> http://www.garlandprosolutions.net</a> website operated by Garland Pro Solutions, LP ("us", "we", or "our").</p> 
              <p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>
              <p><strong>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</strong></p>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <div class="col-12">
            <div class="callout callout-info">
              <h5> Purchases</h5>
              If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including, without limitation, your store number, name, title, phone number, email address, employee number or ID and/or affiliation with the organization.
            </div>
            <div class="callout callout-warning">
              <h5> Subscriptions</h5>
              Some parts of the Service are billed on a subscription basis ("Subscription(s)"). You will be billed in advance on a recurring monthly or yearly basis. The terms of this renewal are subject to how you established your contract with Garland Pro Solutions, LP.
            </div>
            <div class="callout callout-danger">
              <h5> Content</h5>
              Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). You are responsible for the allowable users to input this information, delete this information and manage this personal information.
            </div>
            <div class="callout callout-success">
              <h5> Changes</h5>
              We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.
            </div>
            <div class="callout callout-info">
              <h5> Contact Us</h5>
              If you have any questions about these Terms, please contact us at <a href="mailto:garlandprosolutions@gmail.com">Garlandprosolutions@gmail.com.</a>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('footer.php'); ?>
