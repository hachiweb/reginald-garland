<?php
session_start();
include('dbconfig.php');
$otp = $_POST['otp'];
$resetotp = $_SESSION['otp'];
$resetid = $_SESSION['resetid'];
$username     = $_POST['username'];
$password     = $_POST['password'];
$confirm_password     = $_POST['confirm_password'];
if ($otp != $resetotp) {
    header('location: index.php?success=1&msg=OTP not match');
    die();
}
if ($password != $confirm_password ) {
    header('location: index.php?success=1&msg=Password not match');
    die();
}
$uppercase = preg_match('@[A-Z]@', $password);
$lowercase = preg_match('@[a-z]@', $password);
$number    = preg_match('@[0-9]@', $password);
if(!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
    header('location: index.php?success=1&msg=Password Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters');
    die(); 
}

if(!empty($_POST)){
    $password = md5($password);
    $sql = "UPDATE `users` SET `password` = '$password' WHERE `id` = '$resetid'";
    $result = $con->query($sql);
    if ($con->query($sql) === TRUE) {
        header('location: login/index.php?success=2&msg=Your password has been changed successfully!');
        unset($_SESSION['otp']);
      die();
      
    } else {
        header('location: forgot.php?success=1&msg=Something went wrong!');
        die();
        }
   
}
$con->close();
?>