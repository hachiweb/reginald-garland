<?php
session_start();

$page = 'search';
  include('../../header.php');
  include('../../dbconfig.php');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-8 m-auto">

                <!-- general form elements -->
                <div class="card card-primary custom-form-card">
                    <div class="card-header">
                        <h3 class="card-title">Search Filter</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="search-result.php" method="post">
                        <div class="card-body">
                            <div class="row form-group ">


                                <div class="col-md-3"> <label for="lead">Lead First Name:</label> </div>
                                <div class="col-md-9">

                                    <input type="text" class="form-control" placeholder="Enter Lead First Name" name="Fname">
                                </div>


                            </div>
                            <div class="row form-group ">


                                <div class="col-md-3"> <label for="lead">Lead Last Name:</label> </div>
                                <div class="col-md-9">

                                    <input type="text" class="form-control" placeholder="Enter Lead Last Name" name="Lname">
                                </div>


                            </div>
                            <?php
                              if ($_SESSION['role'] == "Admin") {
                                $sql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' GROUP BY agent_id ORDER BY `agent_name` ASC";
                                $result = $con->query($sql); 
                              ?>
                            <div class="row form-group ">
                                <div class="col-md-3"> <label for="Agent">Agent Name:</label> </div>
                                <div class="col-md-9">
                                    <select name="Agent" class="form-control">
                                        <option selected value="">--All--</option>
                                        <?php
                                            while ($row = $result->fetch_assoc()) {
                                            ?>
                                        <option value="<?=$row['agent_name'];?>"><?=$row['agent_name'];?></option>
                                        <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                                }
                                else {
                                    ?>
                                    <input required type="hidden" id="agent" value="<?=$_SESSION['alais']?>">
                                    <?php
                                }
                                  ?>

                            <div class="row form-group ">


                                <div class="col-md-3"> <label for="medium">Medium Type:</label> </div>
                                <div class="col-md-9">
                                    <select name="medium" class="form-control">
                                        <option selected value="">--All--</option>
                                        <option value="Internet UPS">Internet UPS</option>
                                        <option value="Phone UPS">Phone UPS</option>
                                        <option value="Extended Warranties">Extended Warranties</option>
                                        <option value="5 Star Reviews">5 Star Reviews</option>
                                    </select>


                                    <!-- <input required onclick="hideAll(this)" type="text" id="medium" onkeyup="findParams(this.value,'searchMedium')" class="form-control" placeholder="Enter Medium Type" value="Any">
        <div id="searchMedium" class="text-center bg-white" style="display:none; position:absolute; width:200px;z-index: 1; border-radius:5px;padding:7px;"></div> -->



                                </div>


                            </div>
                            <div class="row form-group ">


                                <div class="col-md-3"> <label for="status">Status Type:</label> </div>
                                <div class="col-md-9">

                                    <select name="status" class="form-control">
                                        <option selected value="">--All--</option>
                                        <option value="Purchased">Purchased</option>
                                        <option value="Cold">Cold</option>
                                        <option value="Hot">Hot</option>
                                        <option value="No Number">No Number</option>
                                        <option value="Came In/Did Not Buy">Came In/Did Not Buy</option>
                                        <option value="Not Buying">Not Buying</option>
                                        <option value="Flaky">Flaky</option>
                                    </select>


                                    <!-- <input required onclick="hideAll(this)" type="text" id="status" onkeyup="findParams(this.value,'searchStatus')" class="form-control" placeholder="Enter Status Type" value="Any">
        <div id="searchStatus" class="text-center bg-white" style="display:none; position:absolute; width:200px;z-index: 1; border-radius:5px;padding:7px;"></div> -->

                                </div>


                            </div>

                            <!-- /.card-body -->

                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>

                        </div><!-- form-dv -->
                        <!-- /.card -->
                    </form>
                </div>

            </div><!-- col -->
        </div><!-- col -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>