<?php
error_reporting(0);
$page = 'multi-table';
include('../../header.php');
$id = $_SESSION['id'];


if ($_SESSION["role"]=="Admin" ) {
   
    //fetch agent
  $sql = "SELECT * FROM lead_capture WHERE `admin_id` = '$userID' GROUP BY `agent_id`";
  $agent_result = $con->query($sql);
// toatls and stats

  // internet totals 
  $sql5 = "SELECT count(source) AS total FROM lead_capture WHERE  `medium` = 'Internet UPS' AND `admin_id` = '$userID'";
  $sql6 = "SELECT count(appt_set) AS total FROM lead_capture WHERE `medium` = 'Internet UPS' AND `appt_set` != '' AND `admin_id` = '$userID'";
  $sql7 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `medium` = 'Internet UPS' AND `appt_show` != '' AND `admin_id` = '$userID'";
  $sql8 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `medium` = 'Internet UPS' AND `appt_sold` != '' AND `admin_id` = '$userID'"; 

  $result5 = $con->query($sql5);
  $result6 = $con->query($sql6);
  $result7 = $con->query($sql7);
  $result8 = $con->query($sql8);

  $IUPSrow5 = $result5->fetch_assoc();
  $IUPSrow6 = $result6->fetch_assoc();
  $IUPSrow7 = $result7->fetch_assoc();
  $IUPSrow8 = $result8->fetch_assoc();

  // phone totals 
  $sql7 = "SELECT count(name) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND `admin_id` = '$userID'";
  $sql8 = "SELECT count(appt_set) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_set != '' AND appt_set = 'SET' AND `admin_id` = '$userID'";
  $sql9 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_show != '' AND appt_show = 'SPLIT SHOW' AND `admin_id` = '$userID'";
  $sql10 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_sold != '' AND appt_sold = 'SPLIT SOLD' AND `admin_id` = '$userID'";
  $sql11 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_show != '' AND appt_show = 'SHOW' AND `admin_id` = '$userID'";
  $sql12 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_sold != '' AND appt_sold = 'SOLD' AND `admin_id` = '$userID'";

  $result7 = $con->query($sql7);
  $result8 = $con->query($sql8);
  $result9 = $con->query($sql9);
  $result10 = $con->query($sql10);
  $result11 = $con->query($sql11);
  $result12 = $con->query($sql12);

  $phone_up_row_7 = $result7->fetch_assoc();
  $phone_up_row_8 = $result8->fetch_assoc();
  $phone_up_row_9 = $result9->fetch_assoc();
  $phone_up_row_10 = $result10->fetch_assoc();
  $phone_up_row_11 = $result11->fetch_assoc();
  $phone_up_row_12 = $result12->fetch_assoc();


  $total_n_stats = array(
    "internet_totals" => array(
      "total_of_leads" => $IUPSrow5["total"],
      "total_set" =>  $IUPSrow6["total"],
      "total_show" => $IUPSrow7["total"],
      "total_sold" => $IUPSrow8["total"]
    ),
    "phone_totals" => array(
      "total_leads" => $phone_up_row_7["total"],
      "total_set" => $phone_up_row_8["total"],
      "total_show" => $phone_up_row_11["total"] + ($phone_up_row_9["total"]/2),
      "total_sold" => $phone_up_row_12["total"] + ($phone_up_row_10["total"]/2)
    )
  );
  
}
    

if (isset($_POST["agent_id"]) && !empty($_POST["agent_id"]) ) {
   
    
  if ($_SESSION["role"]=="Admin" ) {
    $sql = "SELECT * FROM company_goal WHERE `admin_id` = '$userID'";
  }
  else {
    $sql = "SELECT * FROM company_goal WHERE `admin_id` = '$adminID'";
  }
  $result = $con->query($sql);
  $goals = $result->fetch_assoc();

  $agent = $_POST["agent_id"];
    // internet ups
  $sql1 = "SELECT count(appt_set) AS total FROM lead_capture WHERE `medium` = 'Internet UPS' AND `appt_set` != '' AND `agent_id` = '$agent'";
  $sql2 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `medium` = 'Internet UPS' AND `appt_show` != '' AND `agent_id` = '$agent'";
  $sql3 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `medium` = 'Internet UPS' AND `appt_sold` != '' AND `agent_id` = '$agent'";
  $sql4 = "SELECT count(source) AS total FROM lead_capture WHERE `medium` = 'Internet UPS' AND `agent_id` = '$agent'";
   

  $result1 = $con->query($sql1);
  $result2 = $con->query($sql2);
  $result3 = $con->query($sql3);
  $result4 = $con->query($sql4);
  
    $IUPSrow1 = $result1->fetch_assoc();
    $IUPSrow2 = $result2->fetch_assoc();
    $IUPSrow3 = $result3->fetch_assoc();
    $IUPSrow4 = $result4->fetch_assoc();
   

  

 // phone ups
  $sql1 = "SELECT count(name) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND `agent_id` = '$agent'";
  $sql2 = "SELECT count(appt_set) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_set != '' AND appt_set = 'SET' AND `agent_id` = '$agent'";
  $sql3 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_show != '' AND appt_show = 'SHOW' AND `agent_id` = '$agent'";
  $sql4 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_sold != '' AND appt_sold = 'SOLD' AND `agent_id` = '$agent'";
  $sql5 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_show != '' AND appt_show = 'SPLIT SHOW' AND `agent_id` = '$agent'";
  $sql6 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `medium` = 'Phone UPS' AND appt_sold != '' AND appt_sold = 'SPLIT SOLD' AND `agent_id` = '$agent'";


  $result1 = $con->query($sql1);
  $result2 = $con->query($sql2);
  $result3 = $con->query($sql3);
  $result4 = $con->query($sql4);
  $result5 = $con->query($sql5);
  $result6 = $con->query($sql6);


  $phone_up_row_1 = $result1->fetch_assoc();
  $phone_up_row_2 = $result2->fetch_assoc();
  $phone_up_row_3 = $result3->fetch_assoc();
  $phone_up_row_4 = $result4->fetch_assoc();
  $phone_up_row_5 = $result5->fetch_assoc();
  $phone_up_row_6 = $result6->fetch_assoc();


    // Extended Warranties 
  $sql1 = "SELECT count(appt_set) AS total FROM lead_capture WHERE `medium` = 'Extended Warranties' AND `appt_set` != '' AND `agent_id` = '$agent'";
  $sql2 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `medium` = 'Extended Warranties' AND `appt_show` != '' AND `agent_id` = '$agent'";
  $sql3 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `medium` = 'Extended Warranties' AND `appt_sold` != '' AND `agent_id` = '$agent'";
  $result1 = $con->query($sql1);
  $result2 = $con->query($sql2);
  $result3 = $con->query($sql3);

  $Extended_row1 = $result1->fetch_assoc();
  $Extended_row2 = $result2->fetch_assoc();
  $Extended_row3 = $result3->fetch_assoc();

    // 5 star 
  $sql1 = "SELECT count(filled_out) AS total FROM lead_capture WHERE `medium` = '5 Star Reviews' AND `filled_out` = 'Yes' AND `agent_id` = '$agent'";
  $result1 = $con->query($sql1);
  $five_star_row1 = $result1->fetch_assoc();

  $data = array(
    "internet_totals" => array(
      "set" => $IUPSrow1["total"],
      "show" => $IUPSrow2["total"],
      "sold" => $IUPSrow3["total"],
      "of_leads" => $IUPSrow4["total"]
    ),
    "phone_totals" => array(
      "leads" => $phone_up_row_1["total"],
      "set" => $phone_up_row_2["total"],
      "show" => $phone_up_row_3["total"] + ($phone_up_row_5["total"]/2),
      "sold" => $phone_up_row_4["total"] + ($phone_up_row_6["total"]/2),
      "split_show" => $phone_up_row_5["total"]/2,
      "split_sold" => $phone_up_row_6["total"]/2
    ),
    "Extended_totals" => array(
      "set" => $Extended_row1["total"],
      "show" => $Extended_row2["total"],
      "sold" => $Extended_row3["total"]
    ),
    "five_star_totals" => array(
      "yes" => $five_star_row1["total"]
    )
  );


 
// }
}

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Agent Performance &nbsp;<small><?php  if(isset($_POST["agent_id"]) && $_POST["agent_id"] != ""){echo "(".$_POST["agent_id"].")";} ?> </small> </h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Total Performance </li>
          </ol>
        </div>
      </div>
      <!-- <div class="row">
      <form>
        <input type="text" onkeyup="showResult(this.value)" class="form-control">
        <div id="livesearch" class="text-center bg-white" style="display:none; position:absolute; width:200px;z-index: 1; border-radius:5px;padding:7px;"></div>
      </form>
    </div> -->
    <?php
    if ($_SESSION["role"] == "Admin") {
      ?>
      <div class="row">
        <form action="multi-table.php" method="POST" id="form_submit">
        <div class="row">
          <div class="col-lg-4">
            <div class="form-group">
            <select class="form-control  font-weight-bold" name="agent_id" id="agent_id" required>
              <option value=""selected>--Select Agent--</option>
              <?php

              while ($row = $agent_result->fetch_assoc()) {
                if (isset($_POST["agent_id"]) && !empty($_POST["agent_id"] && $_POST["agent_id"] == $row["agent_id"])) {
                  echo "<option selected value='".$_POST["agent_id"]."' >".$row["agent_name"]."</option>";
                }
                else{?>
                  <option <?php if (isset($_POST['agent_id']) && $_POST['agent_id']==$row["agent_name"]) echo "selected";?>  value="<?=$row["agent_name"];?>"><?=$row["agent_name"];?></option>
               <?php } ?>
                
              <?php }
              ?>
              
            </select>
            
          </div>
          </div>
          <div class="col-lg-2">
            <div class="row">
                <input type="text" class="form-control font-weight-bold fromdate" autocomplete="off" placeholder="From Date"id="form"
              value="<?php echo $_POST['fromdate']; ?>"  <?php if(isset($_POST['fromdate'])=='1'){echo 'selected';} ?> name="fromdate" required></div>
          </div>
          <div class="col-lg-2">
              <input type="text" class="form-control  font-weight-bold todate" autocomplete="off" value="<?php echo $_POST['todate'];?>" placeholder="To Date" <?php if(isset($_POST['todate'])=='1'){echo 'selected';} ?>  id="to"
                  name="todate" required>
          </div>
          <div class="col-lg-2">
           <div class="form-group">
           
           <button style="" id="filter"name="filter" type="submit" class="btn-dark form-control"><i class="fa fa-search"></i> Apply Filter</button>
           </div>
          </div>
          </div>
        </form>
      </div>
      <?php
    } 
    
    if($_SESSION["role"] == "Agent" && isset($_GET) && !empty($_GET["agent_id"])  ) {

      ?>
      <form action="multi-table.php" method="POST">
       <!-- <input  type="hidden" name="agent_id" value='<?=$_GET["agent_id"]?>'>
       <input  type="hidden" name="agent_name" value='<?=$_GET["agent_name"]?>'>
       <button style="display:none;" id="agent_form_btn" type="submit" class="ml-2 p-1 btn"><i class="fa fa-search"></i></button> -->
       
     </form>
     <script>
       document.getElementById("agent_form_btn").click();
     </script>
     <?php
   }
   ?>
 </div><!-- /.container-fluid -->
</section>

<?php
// if (isset($_POST["agent_id"]) && !empty($_POST["agent_id"])) {
  if(isset($_POST["filter"])){
    $agent_id =$_POST['agent_id'];
    $from_date = $_POST['fromdate'];
    $to_date = $_POST['todate']; 
     
   
    if($_SESSION["role"]=="Admin"){
      $sqli1 = "SELECT * FROM lead_capture WHERE ((date(created_at)) BETWEEN '$from_date' AND '$to_date')  AND  `agent_name` = '$agent_id' ";
      // print_r($sqli1);
      }{
        
        $sqli1 = "SELECT * FROM lead_capture WHERE ((date(created_at)) BETWEEN '$from_date' AND '$to_date')  AND  `agent_name` = '$agent_id' ";
        // print_r($sqli1);
      }
    
    // print_r($sqli1);
  $fetch_data1 = $con->query($sqli1);
  $agent_data1 =$fetch_data1->fetch_assoc();
   
  if(($agent_data1 > 0)){ 
  ?>
  <!-- Main content -->
  <section class="content">
    <div class="bg-white mb-4 radio-select-type-dv">
      <div class="card-body">
        <div class="form-group clearfix text-center m-0">
          <div class="icheck-primary d-inline">
            <input type="radio" name="multi-table-radio" id="r1" checked="checked">
            <label for="r1">Totals</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="multi-table-radio" id="r2">
            <label for="r2">Bonus Payout</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="multi-table-radio" id="r3">
            <label for="r3">Company Goal for Internet Performance</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="multi-table-radio" id="r4">
            <label for="r4">Company Goal for Phone Performance</label>
          </div>
        </div>
      </div>
    </div>

    <div class="row totals-area">

     <div class=" col-md-12 bg-white mb-4 radio-select-type-dv">
      <div class="card-body">
        <div class="form-group clearfix text-center m-0">
          <div class="icheck-primary d-inline">
            <input type="radio" name="totals-radio" id="r5" checked="checked">
            <label for="r5">Internet Total</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="totals-radio" id="r6">
            <label for="r6">Phone Totals</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="totals-radio" id="r7">
            <label for="r7">Extended Totals</label>
          </div>

        </div>
      </div>
    </div>
  </div>

  <div class="row totals-area internet-area">
    <div class="col-md-6 my-auto">
      <div class="card custom-table-card table-dv table-internet d-block">
        <div class="card-header bg-blue text-white">
          <h3 class="card-title">INTERNET TOTALS</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="jsgrid-table" id="xls1">
            <tbody>
              <tr class="jsgrid-row">
                <td class="jsgrid-cell">SET</td>
                <td class="jsgrid-cell"><?php 
                if (empty($data["internet_totals"]["set"])) {
                  echo "0";
                } else {
                  echo $data["internet_totals"]["set"];
                }
                ?></td>
              </tr>
              <tr class="jsgrid-alt-row">
                <td class="jsgrid-cell">SHOW</td>
                <td class="jsgrid-cell"><?php 
                if (empty($data["internet_totals"]["show"])) {
                  echo "0";
                } else {
                  echo $data["internet_totals"]["show"];
                }
                ?></td>
              </tr>
              <tr class="jsgrid-alt-row">
                <td class="jsgrid-cell">SOLD</td>
                <td class="jsgrid-cell"><?php 
                if (empty($data["internet_totals"]["sold"])) {
                  echo "0";
                } else {
                  echo $data["internet_totals"]["sold"];
                }
                ?></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="text-center pb-3">
          <button type="button" class="btn btn-outline-primary" id="exp1"><i class="fas fa-save"></i> Save </button>

          <button type="button" class="btn btn-outline-primary printBdc">
            <i class="fa fa-print" aria-hidden="true"></i> Print 
          </button>
        </div>
      </div>
      <!-- /.card -->
    </div><!-- col -->
    <div class="col-md-6">
      <div class="table-responsive pb-5" id="internet"></div>
    </div>
  </div><!-- row -->

  <div class="row totals-area phone-area">
    <div class="col-md-6 my-auto">
      <div class="card custom-table-card table-dv table-internet d-block">
        <div class="card-header bg-blue text-white">
          <h3 class="card-title">PHONE TOTALS</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="jsgrid-table" id="xls2">
            <tbody>
              <tr class="jsgrid-row">
                <td class="jsgrid-cell">LEADS</td>
                <td class="jsgrid-cell"><?php
                if (empty($data["phone_totals"]["leads"])) {
                 echo "0";
               } else {
                 echo $data["phone_totals"]["leads"];
               }
               ?></td>
             </tr>
             <tr class="jsgrid-alt-row">
              <td class="jsgrid-cell">SET</td>
              <td class="jsgrid-cell"><?php
              if (empty($data["phone_totals"]["set"])) {
               echo "0";
             } else {
               echo $data["phone_totals"]["set"];
             }
             ?></td>
           </tr>
           <tr class="jsgrid-alt-row">
            <td class="jsgrid-cell">SHOW</td>
            <td class="jsgrid-cell"><?php 
            if (empty($data["phone_totals"]["show"])) {
             echo "0";
           } else {
             echo $data["phone_totals"]["show"];
           }
           ?></td>
         </tr>
         <tr class="jsgrid-alt-row">
          <td class="jsgrid-cell">SPLIT SHOW</td>
          <td class="jsgrid-cell"><?php 
          if (empty($data["phone_totals"]["split_show"])) {
            echo "0";
          } else {
           echo $data["phone_totals"]["split_show"];
         }
         ?></td>
       </tr>
       <tr class="jsgrid-alt-row">
        <td class="jsgrid-cell">SPLIT SOLD</td>
        <td class="jsgrid-cell"><?php 
        if (empty($data["phone_totals"]["split_sold"])) {
         echo "0";
       } else {
         echo $data["phone_totals"]["split_sold"];
       }
       ?></td>
     </tr>
     <tr class="jsgrid-alt-row">
      <td class="jsgrid-cell">SOLD</td>
      <td class="jsgrid-cell"><?php
      if (empty($data["phone_totals"]["sold"])) {
       echo "0";
     } else {
       echo $data["phone_totals"]["sold"];
     }
     ?></td>
   </tr>
 </tbody>
</table>
</div>
<!-- /.card-body -->
<div class="text-center pb-3">
  <button type="button" class="btn btn-outline-primary" id="exp2"><i class="fas fa-save"></i> Save </button>
</div>
</div>
<!-- /.card -->
</div><!-- col -->
<div class="col-md-6">
  <div class="table-responsive pb-5" id="phone"></div>
</div>
</div><!-- row -->
<div class="row totals-area extended-area">
  <div class="col-md-6 my-auto">
    <div class="card custom-table-card table-dv table-internet d-block">
      <div class="card-header bg-blue text-white">
        <h3 class="card-title">EXTENDED WARRANTIES TOTALS</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">

        <table class="jsgrid-table" id="xls3">
          <tbody>
            <tr class="jsgrid-row">
              <td class="jsgrid-cell">SET</td>
              <td class="jsgrid-cell"><?php 
              if (empty($data["Extended_totals"]["set"])) {
                echo "0";
              } else {
                echo $data["Extended_totals"]["set"];
              }
              ?></td>
            </tr>
            <tr class="jsgrid-alt-row">
              <td class="jsgrid-cell">SHOW</td>
              <td class="jsgrid-cell"><?php 
              if (empty($data["Extended_totals"]["show"])) {
                echo "0";
              } else {
                echo $data["Extended_totals"]["show"];
              }
              ?></td>
            </tr>
            <tr class="jsgrid-alt-row">
              <td class="jsgrid-cell">SOLD</td>
              <td class="jsgrid-cell"><?php 
              if (empty($data["Extended_totals"]["sold"])) {
                echo "0";
              } else {
                echo $data["Extended_totals"]["sold"];
              }
              ?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="text-center pb-3">
        <button type="button" class="btn btn-outline-primary" id="exp3"><i class="fas fa-save"></i> Save </button>

        <button type="button" class="btn btn-outline-primary printBdc">
          <i class="fa fa-print" aria-hidden="true"></i> Print 
        </button>
      </div>
    </div>
    <!-- /.card -->
  </div><!-- col -->
  <div class="col-md-6">
    <div class="table-responsive pb-5" id="extended"></div>
  </div>
</div><!-- row -->

<div class="row bonus-area">
  <div class="col-lg-6  my-auto">
    <div class="card custom-table-card table-dv table-internet d-block">
      <div class="card-header bg-blue text-white">
        <h3 class="card-title">BONUS PAYOUT</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <?php $total = 0; ?>
        <table class="jsgrid-table" id="xls4">
          <tbody>
            <tr class="jsgrid-row">
              <td class="jsgrid-cell">BONUS PAYOUT</td>
              <td class="jsgrid-cell">AMOUNT</td>
              <td class="jsgrid-cell">MONEY</td>
            </tr>
            <tr class="jsgrid-alt-row">
              <td class="jsgrid-cell">TOTAL SET</td>
              <td class="jsgrid-cell"><?php 
              if (empty($data["internet_totals"]["set"] + $data["phone_totals"]["set"] + $data["Extended_totals"]["set"])) {
                echo "0";
              } else {
               echo $data["internet_totals"]["set"] + $data["phone_totals"]["set"] + $data["Extended_totals"]["set"];
             }
             ?></td>
             <td class="jsgrid-cell">N/A</td>
           </tr>
           <tr class="jsgrid-row">
            <td class="jsgrid-cell">TOTAL SHOWS</td>
            <td class="jsgrid-cell"><?php
            if (empty($data["internet_totals"]["show"] + $data["phone_totals"]["show"] + $data["Extended_totals"]["show"])) {
             echo "0";
           } else {
            echo $data["internet_totals"]["show"] + $data["phone_totals"]["show"] + $data["Extended_totals"]["show"];
          }
          ?></td>
          <td class="jsgrid-cell">$<?php 
          if (empty($data["internet_totals"]["show"] + $data["phone_totals"]["show"] + $data["Extended_totals"]["show"])) {
           echo "0";
         } else {
           $total += ($data["internet_totals"]["show"] + $data["phone_totals"]["show"] + $data["Extended_totals"]["show"])*10;
           echo ($data["internet_totals"]["show"] + $data["phone_totals"]["show"] + $data["Extended_totals"]["show"])*10;
         }
         ?>.00</td>
       </tr>
       <tr class="jsgrid-alt-row">
        <td class="jsgrid-cell">TOTAL SOLD</td>
        <td class="jsgrid-cell"><?php
        if (empty($data["internet_totals"]["sold"] + $data["phone_totals"]["sold"] )) {
          echo "0";
        } else {
         echo $data["internet_totals"]["sold"] + $data["phone_totals"]["sold"];
       }
       ?></td>
       <td class="jsgrid-cell">$<?php
       if (empty($data["internet_totals"]["sold"] + $data["phone_totals"]["sold"])) {
         echo "0";
       } else {
        $total += ($data["internet_totals"]["sold"] + $data["phone_totals"]["sold"])*20;
        echo ($data["internet_totals"]["sold"] + $data["phone_totals"]["sold"])*20;
      }
      ?>.00</td>
    </tr>
    <tr class="jsgrid-row">
      <td class="jsgrid-cell">TOTAL EXTENDED WARRANTIES</td>
      <td class="jsgrid-cell"><?php 
      if (empty($data["Extended_totals"]["sold"])) {
       echo "0";
     } else {
       echo $data["Extended_totals"]["sold"];
     }
     ?></td>
     <td class="jsgrid-cell">$<?php
     if (empty($data["Extended_totals"]["sold"])) {
       echo "0";
     } else {
      $total += $data["Extended_totals"]["sold"]*50;
      echo $data["Extended_totals"]["sold"]*50;
    }
    ?>.00</td>
  </tr>
  <tr class="jsgrid-alt-row">
    <td class="jsgrid-cell">5 STAR REVIEWS</td>
    <td class="jsgrid-cell"><?php
    if (empty($data["five_star_totals"]["yes"])) {
      echo "0";
    } else {
     echo $data["five_star_totals"]["yes"];
   }
   ?></td>
   <td class="jsgrid-cell">$<?php
   if (empty($data["five_star_totals"]["yes"])) {
     echo "0";
   } else {
    $total += $data["five_star_totals"]["yes"]*5;
    echo $data["five_star_totals"]["yes"]*5;
  }
  ?>.00</td>
</tr>
<tr class="jsgrid-row">
  <td class="jsgrid-cell">TOTAL COMMISSION</td>
  <td class="jsgrid-cell"></td>
  <td class="jsgrid-cell">$<?php
  echo $total;
  ?>.00</td>
</tr>
</tbody>
</table>
</div>
<!-- /.card-body -->
<div class="text-center pb-3">
  <button type="button" class="btn btn-outline-primary" id="exp4"><i class="fas fa-save"></i> Save </button>

  <button type="button" class="btn btn-outline-primary printBdc">
    <i class="fa fa-print" aria-hidden="true"></i> Print 
  </button>
</div>
</div>
<!-- /.card -->
</div><!-- col -->
<div class="col-lg-6 my-auto">
  <div class="table-responsive" id="bonus"></div>
</div>
</div><!-- row -->
<div class="row internet-performance-area">
  <div class="col-lg-6  my-auto">
    <div class="card custom-table-card table-dv table-internet d-block">
      <div class="card-header bg-blue text-white">
        <h3 class="card-title">COMPANY GOAL FOR INTERNET PERFORMANCE</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="jsgrid-table" id="xls5">
          <tbody>
            <tr class="jsgrid-alt-row">
              <th class="jsgrid-cell text-center" colspan="5">COMPANY GOAL FOR INTERNET PERFORMANCE</th>
            </tr>
            <tr class="jsgrid-row">
              <td class="jsgrid-cell">APPT SET/LEADS</td>
              <td class="jsgrid-cell">APPT SHOW/SET</td>
              <td class="jsgrid-cell">SOLD/SET</td>
              <td class="jsgrid-cell">SOLD/SHOW</td>
              <td class="jsgrid-cell">SOLD</td>
            </tr>
            <tr class="jsgrid-alt-row">                  
              <td class="jsgrid-cell"><?=$goals["i_set_lead"]?>%</td>
              <td class="jsgrid-cell"><?=$goals["i_show_set"]?>%</td>
              <td class="jsgrid-cell"><?=$goals["i_sold_set"]?>%</td>
              <td class="jsgrid-cell"><?=$goals["i_sold_show"]?>%</td>
              <td class="jsgrid-cell"><?=$goals["i_sold"]?>%</td>
            </tr>

            <tr class="jsgrid-alt-row">
              <th class="jsgrid-cell text-center" colspan="5">PERSONAL PERFORMANCE</th>
            </tr>

            <tr class="jsgrid-row">
              <td class="jsgrid-cell">APPT SET/LEADS</td>
              <td class="jsgrid-cell">APPT SHOW/SET</td>
              <td class="jsgrid-cell">SOLD/ SET</td>
              <td class="jsgrid-cell">SOLD/SHOW</td>
              <td class="jsgrid-cell">SOLD</td>
            </tr>
            <tr class="jsgrid-alt-row">                  
              <td class="jsgrid-cell"><?php 
              if (empty($data["internet_totals"]["set"]/$data["internet_totals"]["of_leads"])) {
                echo "0";
              } else {
               echo round(($data["internet_totals"]["set"]/$data["internet_totals"]["of_leads"])*100);

             }
             ?>%</td>
             <td class="jsgrid-cell"><?php          if (empty($data["internet_totals"]["show"]/$data["internet_totals"]["set"])) {
              echo "0";
            } else {
             echo round(($data["internet_totals"]["show"]/$data["internet_totals"]["set"])*100);

           }
           ?>%</td>
           <td class="jsgrid-cell"><?php    if (empty($data["internet_totals"]["sold"]/$data["internet_totals"]["set"])) {
            echo "0";
          } else {
           echo round(($data["internet_totals"]["sold"]/$data["internet_totals"]["set"])*100);

         }
         ?>%</td>
         <td class="jsgrid-cell"><?php          if (empty($data["internet_totals"]["sold"]/$data["internet_totals"]["show"])) {
          echo "0";
        } else {
         echo round(($data["internet_totals"]["sold"]/$data["internet_totals"]["show"])*100);

       }
       ?>%</td>
       <td class="jsgrid-cell"><?php 
       if (empty($data["internet_totals"]["sold"]/$data["internet_totals"]["of_leads"])) {
        echo "0";
      } else {
       echo round(($data["internet_totals"]["sold"]/$data["internet_totals"]["of_leads"])*100);

     }
     ?>%</td>
   </tr>
 </tbody>
</table>
</div>
<!-- /.card-body -->
<div class="text-center pb-3">
  <button type="button" class="btn btn-outline-primary" id="exp5"><i class="fas fa-save"></i> Save </button>

  <button type="button" class="btn btn-outline-primary printBdc">
    <i class="fa fa-print" aria-hidden="true"></i> Print 
  </button>
</div>
</div>
<!-- /.card -->
</div><!-- col -->
<div class="col-lg-6 my-auto">
  <div class="table-responsive" id="internetGoal"></div>
</div>
</div><!-- row -->

<div class="row phone-performance-area">
  <div class="col-lg-6  my-auto">
    <div class="card custom-table-card table-dv table-internet d-block">
      <div class="card-header bg-blue text-white">
        <h3 class="card-title">COMPANY GOAL FOR PHONE PERFORMANCE</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="jsgrid-table" id="xls6">
          <tbody>
            <tr class="jsgrid-alt-row">
              <th class="jsgrid-cell text-center" colspan="5">COMPANY GOAL FOR PHONE PERFORMANCE</th>
            </tr>
            <tr class="jsgrid-row">
              <td class="jsgrid-cell">APPT SET/LEADS</td>
              <td class="jsgrid-cell">APPT SHOW/SET</td>
              <td class="jsgrid-cell">SOLD/SET</td>
              <td class="jsgrid-cell">SOLD/SHOW</td>
              <td class="jsgrid-cell">SOLD</td>
            </tr>
            <tr class="jsgrid-alt-row">                  
              <td class="jsgrid-cell"><?=$goals["p_set_lead"]?>%</td>
              <td class="jsgrid-cell"><?=$goals["p_show_set"]?>%</td>
              <td class="jsgrid-cell"><?=$goals["p_sold_set"]?>%</td>
              <td class="jsgrid-cell"><?=$goals["p_sold_show"]?>%</td>
              <td class="jsgrid-cell"><?=$goals["p_sold"]?>%</td>
            </tr>

            <tr class="jsgrid-alt-row">
              <th class="jsgrid-cell text-center" colspan="5">PERSONAL PERFORMANCE</th>
            </tr>

            <tr class="jsgrid-row">
              <td class="jsgrid-cell">APPT SET/LEADS</td>
              <td class="jsgrid-cell">APPT SHOW/SET</td>
              <td class="jsgrid-cell">SOLD/ SET</td>
              <td class="jsgrid-cell">SOLD/SHOW</td>
              <td class="jsgrid-cell">SOLD</td>
            </tr>
            <tr class="jsgrid-alt-row">                  
              <td class="jsgrid-cell"><?php 
              if (empty($data["phone_totals"]["set"]/$data["phone_totals"]["leads"])) {
                echo "0";
              } else {
               echo round(($data["phone_totals"]["set"]/$data["phone_totals"]["leads"])*100);

             }
             ?>%</td>
             <td class="jsgrid-cell"><?php     if (empty($data["phone_totals"]["show"]/$data["phone_totals"]["set"])) {
              echo "0";
            } else {
             echo round(($data["phone_totals"]["show"]/$data["phone_totals"]["set"])*100);

           }
           ?>%</td>
           <td class="jsgrid-cell"><?php    if (empty($data["phone_totals"]["sold"]/$data["phone_totals"]["set"])) {
            echo "0";
          } else {
           echo round(($data["phone_totals"]["sold"]/$data["phone_totals"]["set"])*100);

         }
         ?>%</td>
         <td class="jsgrid-cell"><?php     if (empty($data["phone_totals"]["sold"]/$data["phone_totals"]["show"])) {
          echo "0";
        } else {
         echo round(($data["phone_totals"]["sold"]/$data["phone_totals"]["show"])*100);

       }
       ?>%</td>
       <td class="jsgrid-cell"><?php 
       if (empty($data["phone_totals"]["sold"]/$data["phone_totals"]["leads"])) {
        echo "0";
      } else {
       echo round(($data["phone_totals"]["sold"]/$data["phone_totals"]["leads"])*100);

     }
     ?>%</td>
   </tr>
 </tbody>
</table>
</div>
<!-- /.card-body -->
<div class="text-center pb-3">
  <button type="button" class="btn btn-outline-primary" id="exp6"><i class="fas fa-save"></i> Save </button>

  <button type="button" class="btn btn-outline-primary printBdc">
    <i class="fa fa-print" aria-hidden="true"></i> Print 
  </button>
</div>
</div>
<!-- /.card -->
</div><!-- col -->
<div class="col-lg-6 my-auto">
  <div class="table-responsive" id="phoneGoal"></div>
</div>
</div><!-- row -->

<!-- chart -->

</section>
<?php
echo "<script>  raw_data = ".json_encode($data)." ;   
goals = ".json_encode($goals).";
</script> 
<script src='/js/agent_chart.js'></script>  ";
// echo $fetch_data['agent_name'];


 
?>

<?php
if ($_SESSION["role"] == "Admin") {
  ?>
  <div class="row pt-5" style="border-top:solid 1px grey">

    <div class="col-lg-8 m-auto">
      <h2 class="text-center">For Admin Use Only</h2><br>
      <div class="card custom-table-card table-dv table-internet d-block">
        <div class="card-header bg-blue text-white">
          <h3 class="card-title">TOTAL</h3>

        </div>
        <!-- /.card-header -->
        <div class="card-body">

          <table class="jsgrid-table" id="xls7">
            <tbody>
              <tr class="jsgrid-alt-row">
                <th class="jsgrid-cell text-center" colspan="4">Internet Totals</th>
              </tr>
              <tr class="jsgrid-row">
                <td class="jsgrid-cell">Total Internet Leads</td>
                <td class="jsgrid-cell">Total Appts SET</td>
                <td class="jsgrid-cell">Total Appt SHOWS</td>
                <td class="jsgrid-cell">Total Appts SOLD</td>
              </tr>
              <tr class="jsgrid-alt-row">                  
                <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["internet_totals"]["total_of_leads"])) {
                   echo "0";
                 } else {
                   echo $total_n_stats["internet_totals"]["total_of_leads"];

                 }
                 ?></td>
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["internet_totals"]["total_set"])) {
                   echo "0";
                 } else {
                   echo $total_n_stats["internet_totals"]["total_set"];

                 }
                 ?></td>

                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["internet_totals"]["total_show"])) {
                   echo "0";
                 } else {
                   echo $total_n_stats["internet_totals"]["total_show"];

                 }
                 ?></td>

                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["internet_totals"]["total_sold"])) {
                   echo "0";
                 } else {
                   echo $total_n_stats["internet_totals"]["total_sold"];

                 }
                 ?></td>

               </tr>

               <tr class="jsgrid-alt-row">
                <th class="jsgrid-cell text-center" colspan="4">Phone Totals Leads</th>
              </tr>

              <tr class="jsgrid-row">
                <td class="jsgrid-cell">Total Phone Leads</td>
                <td class="jsgrid-cell">Total Appts SET</td>
                <td class="jsgrid-cell">Total Appt SHOWS</td>
                <td class="jsgrid-cell">Total Appts SOLD</td>
              </tr>
              <tr class="jsgrid-alt-row">                  
                <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["phone_totals"]["total_leads"])) {
                   echo "0";
                 } else {
                   echo $total_n_stats["phone_totals"]["total_leads"];

                 }
                 ?></td>
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["phone_totals"]["total_set"])) {
                   echo "0";
                 } else {
                   echo $total_n_stats["phone_totals"]["total_set"];

                 }
                 ?></td>
                 <!-- pending -->
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["phone_totals"]["total_show"])) {
                   echo "0";
                 } else {
                   echo $total_n_stats["phone_totals"]["total_show"];

                 }
                 ?></td>

                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["phone_totals"]["total_sold"])) {
                   echo "0";
                 } else {
                   echo $total_n_stats["phone_totals"]["total_sold"];

                 }
                 ?></td>

               </tr>
             </tbody>
           </table>
         </div>
         <!-- /.card-body -->
         <div class="text-center pb-3">
          <button type="button" class="btn btn-outline-primary" id="exp7"><i class="fas fa-save"></i> Save </button>

          <button type="button" class="btn btn-outline-primary printBdc">
            <i class="fa fa-print" aria-hidden="true"></i> Print 
          </button>
        </div>
      </div>
      <!-- /.card -->
    </div><!-- col -->
  </div><!-- row -->

  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-9" style="margin-left:-20px;">
      <div class="table-responsive" id="total">

      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-8 m-auto">
      <div class="card custom-table-card table-dv table-internet d-block">
        <div class="card-header bg-blue text-white">
          <h3 class="card-title">TEAM STATS</h3>
          <p>PERCENTAGES</p>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

          <table class="jsgrid-table" id="xls8">
            <tbody>
              <tr class="jsgrid-alt-row">
                <th class="jsgrid-cell text-center" colspan="4">Internet</th>
              </tr>
              <tr class="jsgrid-row">
                <td class="jsgrid-cell">Set/Lead</td>
                <td class="jsgrid-cell">Show/Set</td>
                <td class="jsgrid-cell">Sold/Show</td>
                <td class="jsgrid-cell">Closing Ratio</td>
              </tr>
              <tr class="jsgrid-alt-row">                  
                <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["internet_totals"]["total_set"]/$total_n_stats["internet_totals"]["total_of_leads"])) {
                   echo "0";
                 } else {
                   echo round(($total_n_stats["internet_totals"]["total_set"]/$total_n_stats["internet_totals"]["total_of_leads"])*100);

                 }
                 ?>%</td>
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["internet_totals"]["total_show"]/$total_n_stats["internet_totals"]["total_set"])) {
                   echo "0";
                 } else {
                   echo round(($total_n_stats["internet_totals"]["total_show"]/$total_n_stats["internet_totals"]["total_set"])*100);

                 }
                 ?>%</td>
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["internet_totals"]["total_sold"]/$total_n_stats["internet_totals"]["total_show"])) {
                   echo "0";
                 } else {
                   echo round(($total_n_stats["internet_totals"]["total_sold"]/$total_n_stats["internet_totals"]["total_show"])*100);

                 }
                 ?>%</td>
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["internet_totals"]["total_sold"]/$total_n_stats["internet_totals"]["total_of_leads"])) {
                   echo "0";
                 } else {
                   echo round(($total_n_stats["internet_totals"]["total_sold"]/$total_n_stats["internet_totals"]["total_of_leads"])*100);

                 }
                 ?>%</td>
               </tr>

               <tr class="jsgrid-alt-row">
                <th class="jsgrid-cell text-center" colspan="4">Phone</th>
              </tr>

              <tr class="jsgrid-row">
                <td class="jsgrid-cell">Set/Lead</td>
                <td class="jsgrid-cell">Show/Set</td>
                <td class="jsgrid-cell">Sold/Show</td>
                <td class="jsgrid-cell">Closing Ratio</td>
              </tr>
              <tr class="jsgrid-alt-row">                  
                <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["phone_totals"]["total_set"]/$total_n_stats["phone_totals"]["total_leads"])) {
                   echo "0";
                 } else {
                   echo round(($total_n_stats["phone_totals"]["total_set"]/$total_n_stats["phone_totals"]["total_leads"])*100);

                 }
                 ?>%</td>
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["phone_totals"]["total_show"]/$total_n_stats["phone_totals"]["total_set"])) {
                   echo "0";
                 } else {
                   echo round(($total_n_stats["phone_totals"]["total_show"]/$total_n_stats["phone_totals"]["total_set"])*100);

                 }
                 ?>%</td>
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["phone_totals"]["total_sold"]/$total_n_stats["phone_totals"]["total_show"])) {
                   echo "0";
                 } else {
                   echo round(($total_n_stats["phone_totals"]["total_sold"]/$total_n_stats["phone_totals"]["total_show"])*100);

                 }
                 ?>%</td>
                 <td class="jsgrid-cell">
                  <?php
                  if (empty($total_n_stats["phone_totals"]["total_sold"]/$total_n_stats["phone_totals"]["total_leads"])) {
                   echo "0";
                 } else {
                   echo round(($total_n_stats["phone_totals"]["total_sold"]/$total_n_stats["phone_totals"]["total_leads"])*100);

                 }
                 ?>%</td>
               </tr>
             </tbody>
           </table>
         </div>
         <!-- /.card-body -->
         <div class="text-center pb-3">
          <button type="button" class="btn btn-outline-primary" id="exp8"><i class="fas fa-save"></i> Save </button>

          <button type="button" class="btn btn-outline-primary printBdc">
            <i class="fa fa-print" aria-hidden="true"></i> Print 
          </button>
        </div>
      </div>
      <!-- /.card -->
    </div><!-- col -->
  </div><!-- row -->
  <br><br><br><br>
  <h4 class="text-center">TEAM STATS PERCENTAGES (in %)</h4>
  <div class="row">

    <div class="col-md-12">
      <div class="table-responsive" id="stats">

      </div>
    </div>
  </div>
  <?php
}
}
else {
  echo '<h4 class="ml-5">Result Not found.</h4>';
}

}

else {
  echo '<h4 class="ml-5">No Result.</h4>';
}

?>
<!-- /.content -->


</div>



<!-- chart  -->

<?php
echo "<script>var stats = ".json_encode($total_n_stats)."; </script>";
?>
<script>
  $(document).ready(function() {
    $(".fromdate").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });
    $(".todate").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });
    $("#exp1").click(function() {
      $("#xls1").table2excel({
        exclude: ".excludeThisClass",
        name: "Internet Total",
        filename: "Internet Total",
        preserveColors: false
      });
    });
    $("#exp2").click(function() {
      $("#xls2").table2excel({
        exclude: ".excludeThisClass",
        name: "Phone Total",
        filename: "Phone Total",
        preserveColors: false
      });
    });
    $("#exp3").click(function() {
      $("#xls3").table2excel({
        exclude: ".excludeThisClass",
        name: "Extended Totals",
        filename: "Extended Totals",
        preserveColors: false
      });
    });
    $("#exp4").click(function() {
      $("#xls4").table2excel({
        exclude: ".excludeThisClass",
        name: "Bonus Payout",
        filename: "Bonus Payout",
        preserveColors: false
      });
    });
    $("#exp5").click(function() {
      $("#xls5").table2excel({
        exclude: ".excludeThisClass",
        name: "Company Goal for Internet Performance",
        filename: "Company Goal for Internet Performance",
        preserveColors: false
      });
    });
    $("#exp6").click(function() {
      $("#xls6").table2excel({
        exclude: ".excludeThisClass",
        name: "Company Goal for Phone Performance",
        filename: "Company Goal for Phone Performance",
        preserveColors: false
      });
    });
    $("#exp7").click(function() {
      $("#xls7").table2excel({
        exclude: ".excludeThisClass",
        name: "Total",
        filename: "Total",
        preserveColors: false
      });
    });
    $("#exp8").click(function() {
      $("#xls8").table2excel({
        exclude: ".excludeThisClass",
        name: "Team Stats Percentage",
        filename: "Team Stats Percentage",
        preserveColors: false
      });
    });

    $("#day").change(function() {
        // console.log('sdfee');
        var d= $(this).val();
        // alert(d);
        if(d != 'all'){
          var week =$('#week').val('all'); 
        }
         
        
  });
  $("#week").change(function() {
        // console.log('sdfee');
        var week= $(this).val();
        // alert(week);
        if(week!='all'){
          // alert(week);
          $('#day').val('all');
          // alert( "Handler for .change() called."+week );
        }
        
  });
     
     

  });
</script>
<script>
//    $(document).ready(function () {
//     $('#openSideBar')[0].click();  //$('#about').get(0).click();
// }); 
// function myFunction(val) {
//   var day =val;
//   if()
//   alert(day);
// }

function autoClick() {
  $("#agent_name").val($("#agent_id option:selected").text());
  document.getElementById("agent_form_btn").click();
}

</script>
<script src="/js/chart.js"></script>

<script>
  $('.printBdc').on('click', function() {  
    window.print();  
    return false;
  });
</script>
<!-- /.content-wrapper -->

<?php include('../../footer.php'); ?>