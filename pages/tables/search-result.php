<?php
error_reporting(0);
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
$LOC = $_POST['LOC'];
$TERM = $_POST['TERM'];
$role = $_SESSION['role'];
$Agent = $_POST['Agent'];
if ($role == 'Admin') {
$Fname = $_POST['Fname'];
$Lname = $_POST['Lname'];
if (empty($Fname)) {
    $lead_name = $Lname;
}elseif (empty($Lname)) {
    $lead_name = $Fname;
}else {
    $lead_name = $Fname.' '.$Lname;
}
}
$medium = $_POST['medium'];
$status = $_POST['status'];
include('../../dbconfig.php');
$page = 'search';
  include('../../header.php');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Main content -->
  <section class="content">
    <div class="row table-responsive" id="response">

<table class="table table-striped" id="resTable">
<?php
if ($role == 'Admin') {
    if ($LOC == 'HED') {
        $sql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `name` LIKE '%$TERM%'";
    } else {
        $sql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `name` LIKE '%$lead_name%' AND `agent_name` LIKE '%$Agent%' AND `medium` LIKE '%$medium%' AND `status` LIKE '%$status%'";
    }
} else {
    $unid = $_SESSION['id'];
    if ($LOC == 'HED') {
        $sql = "SELECT * FROM `lead_capture` WHERE `agent_id` = '$unid' AND `name` LIKE '%$TERM%'";
    } else {
        $sql = "SELECT * FROM `lead_capture` WHERE `agent_id` = '$unid' AND `name` LIKE '%$lead_name%' AND `medium` LIKE '%$medium%' AND `status` LIKE '%$status%'";
    }
    
}
$result = $con->query($sql);
?>
<thead>
<tr>
   <th>#</th>
   <th>Agent Name</th>
   <th>Lead Name</th>
    <th>Appt SET</th>
   <th>Appt SHOW</th>
   <th>Appt SOLD</th>
   <th>Filled Out</th>
   <th>Source</th>
    <th>Status</th>
   <th>Medium</th>
   <!-- <th>Verification</th> -->
</tr>
</thead>

<tbody>
<?php
while ($row = $result->fetch_assoc()) {
    ?>
<tr>
<td><?=$row['id'];?></td>
<td><?=$row['agent_name'];?></td>
<td><?=$row['name'];?></td>


<td><?=$row['appt_set'];?></td>
<td><?=$row['appt_show'];?></td>
<td><?=$row['appt_sold'];?></td>
<td><?=$row['filled_out'];?></td>
<td><?=$row['source'];?></td>
        
<td><?=$row['status'];?></td>
<td><?=$row['medium'];?></td>
<!-- <td><?=$row['verification'];?></td> -->
</tr>
<?php
}
?>
</tbody>

</table>
    <div class="text-center pb-3">
        <button type="button" class="btn btn-outline-primary" id="exp1"><i class="fas fa-save"></i> Save </button>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#exp1").click(function() {
      $("#resTable").table2excel({
          exclude: ".excludeThisClass",
          name: "Search Result",
          filename: "Search Result",
          preserveColors: false
      });
    });
});
</script>
<?php include('../../footer.php'); ?>