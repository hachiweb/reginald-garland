<?php
include('../../header.php');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Notification</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Notification</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
      <div class="col-7 mx-auto">
        <?php
            if(isset($_GET['status']) && $_GET['status']=='true'){
                echo '<h6 class="alert alert-success text-center remove">Notification Added</h6>';
            }
            if(isset($_GET['status']) && $_GET['status']=='update'){
              echo '<h6 class="alert alert-success text-center remove">Notification Updated</h6>';
            }
            if(isset($_GET['status']) && $_GET['status']=='delete'){
              echo '<h6 class="alert alert-success text-center remove">Notification Delete</h6>';
            }
        ?>
      </div>
      </div>
    <div class="row">
      <div class="col-lg-12 m-auto">
        <div class="card custom-table-card table-dv table-internet d-block">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Past/Expired Notifications</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table text-center alltable table-striped" id="xls1">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell"># NOTIFICATION</th>
                  <?php
                    if($role == "Admin") {
                  ?>
                  <th class="jsgrid-cell">Agent/Created By</th>
                  <?php } ?>
                  <th class="jsgrid-cell">TITLE</th>
                  <th class="jsgrid-cell">SCHEDULED AT</th>
                  <th class="jsgrid-cell">MESSAGE</th>
                  <th class="jsgrid-cell excludeThisClass">OPERATIONS</th>                  
                </tr>
              </thead>

              <tbody>
                  <?php 
                      if($role == "Admin") {
                        $query=mysqli_query($con, "SELECT * FROM `notification` WHERE `admin_id`='$userID' OR `user_id`='$userID' AND DATE(`date_set`) < CURDATE()");
                        while($row=mysqli_fetch_array($query)){?>
                        <tr class="jsgrid">
                          <td class="jsgrid-cell"><?php echo $row['id']; ?></td>             <?php 
                            $agent_id = $row['user_id'];
                            $agent_detail = mysqli_query($con, "SELECT * FROM `users` WHERE `id`='$agent_id' "); 
                            $agent_name = mysqli_fetch_array($agent_detail);
                            $agent_name = $agent_name['first_name'].' '.$agent_name['last_name'];
                          ?> 
                          <td class=""><?= !empty(trim($agent_name))?$agent_name:'Self'; ?></td>
                          <td class=""><?php echo $row['title']; ?></td>
                          <td class=""><?php echo $row['date_time']; ?></td>
                          <td class=""><span class="message pop" data-container="body" data-toggle="popover" title="Message:"  data-content="<?php echo $row['message']; ?>"><?php echo $row['message']; ?></span></td>
                          <td class="">
                          <a href="<?php echo $site_url ?>/pages/forms/update.php?id=<?= $row['id']; ?>"><button type="button" class="btn btn-primary"> <i class="fa fa-edit"></i> </button></a>
                          <a href="<?php echo $site_url ?>/pages/forms/update.php?action=delete&id=<?= $row['id']?>"><button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button></a> </td>
                      </tr>                      
                      <?php 
                        } 
                    }
                     elseif($role == "Agent"){?>                                                                    
                      <?php 
                        $query=mysqli_query($con, "SELECT * FROM `notification` WHERE `user_id`='$userID' AND DATE(`date_set`) < CURDATE()");
                        while($row=mysqli_fetch_array($query)){?>
                        <tr class="jsgrid">
                          <td class="jsgrid-cell"><?php echo $row['id']; ?></td>          
                          <td class=""><?php echo $row['title']; ?></td>
                          <td class=""><?php echo $row['date_time']; ?></td>
                          <td class=""><span class="message pop" data-container="body" data-toggle="popover" title="Message:"  data-content="<?php echo $row['message']; ?>"><?php echo $row['message']; ?></span></td>
                          <td class="">
                          <a href="<?php echo $site_url ?>/pages/forms/update.php?id=<?= $row['id']; ?>"><button type="button" class="btn btn-primary"> <i class="fa fa-edit"></i> </button></a>
                          <a href="<?php echo $site_url ?>/pages/forms/update.php?action=delete&id=<?= $row['id']?>"><button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button></a> </td>
                      </tr> 

                    <?php } } else { ?>
                      <?php 
                        $query=mysqli_query($con, "SELECT * FROM `notification`");
                        while($row=mysqli_fetch_array($query)){?>
                        <tr class="jsgrid">
                          <td class="jsgrid-cell"><?php echo $row['id']; ?></td>          
                          <td class=""><?php echo $row['title']; ?></td>
                          <td class=""><?php echo $row['date_time']; ?></td>
                          <td class=""><span class="message pop" data-container="body" data-toggle="popover" title="Message:"  data-content="<?php echo $row['message']; ?>"><?php echo $row['message']; ?></span></td>
                          <td class="">
                          <a href="<?php echo $site_url ?>/pages/forms/update.php?id=<?= $row['id']; ?>"><button type="button" class="btn btn-primary"> <i class="fa fa-edit"></i> </button></a>
                          <a href="<?php echo $site_url ?>/pages/forms/update.php?action=delete&id=<?= $row['id']?>"><button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button></a> </td>
                      </tr> 
                    <?php } }   ?>
                       

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="text-center pb-3">
              <button type="button" class="btn btn-outline-primary" id="exp1"><i class="fas fa-save"></i> Save </button>
              <button type="button" class="btn btn-outline-primary printBdc">
                <i class="fa fa-print" aria-hidden="true"></i> Print 
              </button>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.card -->
            </div><!-- col -->
          </div><!-- col -->
        </section>
        <!-- /.content -->
      </div>
<script>
    $(function () {
      $(".alltable").DataTable();
    });

    function del(_id) {
      $.ajax({
        method: "POST",
        url: "../forms/delete-agent-data.php",
        data: {id : _id}
      }).done(function( _res ) {
        var res = JSON.parse(_res);
        if (res.status == "200") {
          window.location.assign("agent-table.php?success=del");
        } else {
        window.location.assign("agent-table.php?success=err");
      }
    });
      
    }

    setTimeout(function(){ $("#alert").hide(); }, 3000);

    $('.printBdc').on('click', function() {  
      window.print();  
      return false;
    });

    $("tr").not(':first').hover(
    function () {
      $(this).css("background","#b8d1f3");
      $(this).css("cursor","pointer");
    }, 
    function () {
      $(this).css("background","");
    }
  );
  $("[data-toggle=popover]").popover();

</script>

<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>