<?php
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: /index.php");
}
$page = 'source-code';
include('../../header.php');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Total Lead Stats</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Total Lead Stats</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-10 m-auto">

        <div class="card custom-table-card table-dv table-internet d-block">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">SOURCE CODES</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table" id="xls1">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell">SOURCE CODES</th>
                  <th class="jsgrid-cell"># OF LEADS</th>
                  
                  <th class="jsgrid-cell"> SET</th>
                  <th class="jsgrid-cell"> SHOW</th>
                  <th class="jsgrid-cell"> SOLD</th>
                </tr>
              </thead>

              <tbody>

                <?php
                $sql = "SELECT * FROM source WHERE `admin_id` = '$userID'";
                $result = $con->query($sql);
                while($row = $result->fetch_assoc()){
                 $source = $row["source"];

                 $set_code = $row["set_code"];
                 $show_code = $row["show_code"] ;
                 $sold_code = $row["sold_code"] ;

                 $sql = "SELECT source FROM lead_capture WHERE `admin_id` = '$userID' AND source = '$source'";
                 $lead = $con->query($sql);

                 $sql = "SELECT appt_set FROM lead_capture WHERE `admin_id` = '$userID' AND appt_set = 'SET' AND medium = 'Internet UPS' AND source = '$source'";
                 $set = $con->query($sql);

                 $sql = "SELECT appt_show FROM lead_capture WHERE `admin_id` = '$userID' AND appt_show = 'SHOW' AND medium = 'Internet UPS' AND source = '$source'";
                 $show = $con->query($sql);

                 $sql = "SELECT appt_sold FROM lead_capture WHERE `admin_id` = '$userID' AND appt_sold = 'SOLD' AND medium = 'Internet UPS' AND source = '$source'";
                 $sold = $con->query($sql);

                 ?>
                 <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$row["source"] ?></td>
                  <td class="jsgrid-cell"><?=$lead->num_rows ?></td>
                  <td class="jsgrid-cell"><?=$set->num_rows ?></td>
                  <td class="jsgrid-cell"><?=$show->num_rows ?></td>
                  <td class="jsgrid-cell"><?=$sold->num_rows ?></td>
                </tr>
                <?php
              }
                 // totals
              $sql = "SELECT source FROM lead_capture WHERE source != '' ";
              $lead = $con->query($sql); 

              $sql = "SELECT appt_set FROM lead_capture WHERE appt_set = 'SET' AND medium = 'Internet UPS' ";
              $set = $con->query($sql);

              $sql = "SELECT appt_show FROM lead_capture WHERE appt_show = 'SHOW' AND medium = 'Internet UPS' ";
              $show = $con->query($sql);

              $sql = "SELECT appt_sold FROM lead_capture WHERE appt_sold = 'SOLD' AND medium = 'Internet UPS' ";
              $sold = $con->query($sql);
              ?>
              <tr class="jsgrid-row">
                <td class="jsgrid-cell"><b>TOTALS</b></td>
                <td class="jsgrid-cell"><b><?=$lead->num_rows ?></b></td>
                <td class="jsgrid-cell"><b><?=$set->num_rows ?></b></td>
                <td class="jsgrid-cell"><b><?=$show->num_rows ?></b></td>
                <td class="jsgrid-cell"><b><?=$sold->num_rows ?></b></td>
              </tr>

            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="text-center pb-3">
          <button type="button" class="btn btn-outline-primary" id="exp1"><i class="fas fa-save"></i> Save </button>
          <button type="button" class="btn btn-outline-primary printBdc">
            <i class="fa fa-print" aria-hidden="true"></i> Print 
          </button>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.card -->


    </div><!-- col -->
  </div><!-- col -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#exp1").click(function() {
      $("#xls1").table2excel({
        exclude: ".excludeThisClass",
        name: "Total Lead Stats",
        filename: "Total Lead Stats",
        preserveColors: false
      });
    });
  });
</script>

<script>
  $('.printBdc').on('click', function() {  
    window.print();  
    return false;
  });
</script>

<?php include('../../footer.php'); ?>