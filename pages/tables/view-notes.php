<?php
session_start();
include('../../header.php');

$agent_id = $_GET['id'];

if(isset($_POST['update_notes']))
{
    $id = $_POST['row_id'];
    // $agent_name = '';
    // $customer_name = '';
    // $notes = '';

    if(isset($_POST['agent_name']))
        $agent_name = $_POST['agent_name'];

    if(isset($_POST['customer_name']))
        $customer_name = $_POST['customer_name'];

    if(isset($_POST['notes']))
        $notes = $_POST['notes'];

    $query = "update lead_capture set  notes='$notes' where id=$id";
    
    // echo $query;
    mysqli_query($con, $query);
}

  if(isset($_GET['status']) && $_GET['status']=='true'){
      // echo '<h6 class="alert alert-success text-center deletesuccess">Record added</h6>';
      echo "<script>
          $('document').ready(function(){ 
          $('.toastrDefaultSuccess').click(); 
          }); 
      </script>";
      }
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Notes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
                        <li class="breadcrumb-item active">Notes</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-12 m-auto">
                <div class="card custom-table-card table-dv table-internet d-block">
                    <div class="card-header bg-blue text-white">
                        <h3 class="card-title">View Notes</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table text-center table-bordered alltable" id="xls1">
                            <thead class="jsgrid-grid-header">
                                <tr class="jsgrid-alt-row">
                                    <th class="jsgrid-cell"># ID</th>
                                    <th class="jsgrid-cell">Agent Name</th>
                                    <th class="jsgrid-cell">Customer Name</th>
                                    <th class="jsgrid-cell">Notes</th>
                                    <th class="jsgrid-cell excludeThisClass">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
              if($role == 'Admin'){
                $query = mysqli_query($con, "SELECT * FROM lead_capture WHERE `admin_id`='$userID' AND `notes`!=''");
              } 
              else if($role == 'Agent'){
                $query = mysqli_query($con, "SELECT * FROM lead_capture WHERE `agent_id`='$userID' AND `notes`!=''");
              }
                while($row=mysqli_fetch_array($query)){
                  if($agent_id == $row['id']){
                    
                    ?>
                                <tr class="jsgrid notes_id">
                                    <td class="jsgrid-cell"><?php echo $row['id']; ?></td>
                                    <td class=""><?php echo $row['agent_name']; ?></td>
                                    <td class=""><?php echo $row['name']; ?></td>
                                    <td class=""><span class="message" data-toggle="popover" title="Notes:"
                                            data-content="<?php echo strip_tags($row['notes']); ?>"><?php echo strip_tags($row['notes']); ?></span>
                                    </td>
                                    <td class="">
                                        <button type="button" class="btn btn-primary "
                                            onclick="openModal(<?php echo $row['id'];?>)"><i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger toastrdeleatesuccess"
                                            onclick="del('<?= $row['id']?>','<?php echo isset($_GET['id']) && !empty($_GET['id'])?$_GET['id']:0; ?>')"> <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <?php } 
                    else { ?>
                                <tr class="jsgrid">
                                    <td class="jsgrid-cell"><?php echo $row['id']; ?></td>
                                    <td class=""><?php echo $row['agent_name']; ?></td>
                                    <td class=""><?php echo $row['name']; ?></td>
                                    <td class=""><span class="message" data-toggle="popover" title="Notes:"
                                            data-content="<?php echo strip_tags($row['notes']); ?>"><?php echo strip_tags($row['notes']); ?></span>
                                    </td>
                                    <td class="">
                                        <button type="button" class="btn btn-primary "
                                            onclick="openModal(<?php echo $row['id'];?>)"><i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger" onclick="del('<?= $row['id']?>','<?php echo isset($_GET['id'])&&!empty($_GET['id'])?$_GET['id']:0; ?>')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="text-center pb-3">
                        <button type="button" class="btn btn-outline-primary" id="exp1"><i class="fas fa-save"></i> Save
                        </button>
                        <button type="button" class="btn btn-outline-primary printBdc">
                            <i class="fa fa-print" aria-hidden="true"></i> Print
                        </button>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.card -->
            </div><!-- col -->
        </div><!-- col -->
    </section><!-- /.content -->
    <form action="?status=true&edit&id=<?=$_GET['id']?>" method="POST">
        <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-blue text-white">
                        <h4 class="modal-title" id="modalLabel">Update Notes</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input type="hidden" name="row_id" id="row_id">
                    <div class="modal-body">
                        <div class='row'>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="agent_name">Agent Name:</label>
                                    <input type="text" name="agent_name" id="agent_name" class="form-control"
                                        autocomplete="off" disabled required />
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="customer_name">Customer Name:</label>
                                    <input type="text" id="customer_name" name="customer_name" class="form-control"
                                        autocomplete="off" disabled required />
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label for="notes">Notes:</label>
                                    <textarea id="notes" class="form-control" name="notes" rows="2" autocomplete="off"
                                        required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="update_notes" class="btn btn-primary"><i
                                class="fas fa-paper-plane"></i> UPDATE
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <button type="hidden" class="toastrDefaultSuccess"></button>
    <!-- <button type="hidden" class="toastrdeleatesuccess"></button> -->
</div>
<script>
$(function() {
    $(".alltable").DataTable();
});

// for updation
function openModal(id, action) {
    $('#row_id').val(id);
    $.ajax({
        type: "POST",
        url: "load-notes.php",
        data: "id=" + id,
        cache: false,
        dataType: "JSON",
        success: function(response) {
            console.log(response);
            $("#agent_name").val(response[0].agent_name);
            $("#customer_name").val(response[0].customer_name);
            $("#notes").val(response[0].notes);
            $('#viewModal').modal('show');
            // if (res.status == "200") {
            //     window.location.assign("view-notes.php?status=true&edit");
            // } else {
            //     window.location.assign("view-notes.php?status=false&edit");
            // }
        }
    });
}

// for delete

function del(_id,curr) {
    $.ajax({
        method: "POST",
        url: "../tables/delete-notes-data.php",
        data: {
            id: _id,
            current_id:curr
        }
    }).done(function(_res) {
        var res = JSON.parse(_res);
        if (res.status == "200") {
            window.location.assign("view-notes.php?status=true&del&id="+res.current_id);

        } else {
            window.location.assign("view-notes.php?status=false&del&id="+res.current_id);
        }
    });
}

$('.toastrDefaultSuccess').click(function() {
    let searchParams = new URLSearchParams(window.location.search);
    let param = searchParams.get('status')
    if (searchParams.has('edit') && param == 'true') {
        toastr.success('Record updated successfully!')
    } else if (searchParams.has('del') && param == 'true') {
        toastr.success('Record deleted successfully!')
    } else {
        toastr.success('Record added successfully!')
    }
});

setTimeout(function() {
    $("#alert").hide();
}, 3000);

$('.printBdc').on('click', function() {
    window.print();
    return false;
});


$("tr").not(':first').hover(
    function() {
        $(this).css("background", "#b8d1f3");
        $(this).css("cursor", "pointer");
    },
    function() {
        $(this).css("background", "");
    }
);
</script>

<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>