<?php
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: /index.php");
}
  $page = 'lead requests';
  include('../../header.php');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Lead Requests Table</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Lead Requests Table</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
  
      <div class="col-lg-10 m-auto">

      <div class="bg-white mb-4 radio-select-type-dv">
        <div class="card-body">
          <div class="form-group clearfix text-center m-0">
            <div class="icheck-primary d-inline">
              <input type="radio" name="lead-requests-radio" id="r1" checked="checked">
              <label for="r1">Internet UPS</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="lead-requests-radio" id="r2">
              <label for="r2">Phone UPS</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="lead-requests-radio" id="r3">
              <label for="r3">Extended Warranties</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="lead-requests-radio" id="r4">
              <label for="r4">5 Star Reviews</label>
            </div>
          </div>
        </div>
      </div>

        <div class="card custom-table-card table-dv internet-lead-requests" style="display:block;">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Internet Lead Requests</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell">Agent</th>
                  <th class="jsgrid-cell">Lead</th>
                  <th class="jsgrid-cell">APPT  SET  </th>
                  <th class="jsgrid-cell"> APPT SHOW  </th>
                  <th class="jsgrid-cell">APPT  SOLD  </th>
                  <th class="jsgrid-cell">Source</th>
                  <th class="jsgrid-cell">Status</th>
                  <th class="jsgrid-cell">Operations</th>
                </tr>
              </thead>

              <tbody>

              <?php
                 $sql = "SELECT * FROM lead_capture WHERE verification = 'pending' AND medium = 'Internet UPS' ";
                 $result = $con->query($sql);
                 while($row = $result->fetch_assoc()){
                   ?>
                  <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$row["agent_name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_set"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_show"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_sold"] ?></td>
                  <td class="jsgrid-cell"><?=$row["source"] ?></td>
                  <td class="jsgrid-cell"><?=$row["status"] ?></td>
                  <td class="jsgrid-cell">
                  <button class="btn btn-success" onclick="approve(<?=$row['id']?>)" ><i class="fa fa-check"></i></button>
                  <button class="btn btn-danger" onclick="discard(<?=$row['id']?>)" ><i class="fa fa-times"></i></button>
                  </td>
                </tr>
                   <?php
                 }
              ?>
               
             
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <!-- /.card -->
        </div>
        <!-- /.card -->

        <div class="card custom-table-card table-dv phone-lead-requests" style="display:none;">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Phone Lead Requests</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell">Agent</th>
                  <th class="jsgrid-cell">Lead</th>
                  <th class="jsgrid-cell">APPT  SET  </th>
                  <th class="jsgrid-cell"> APPT SHOW  </th>
                  <th class="jsgrid-cell">APPT  SOLD  </th>
                
                  <th class="jsgrid-cell">Status</th>
                  <th class="jsgrid-cell">Operations</th>
                </tr>
              </thead>

              <tbody>

              <?php
                 $sql = "SELECT * FROM lead_capture WHERE verification = 'pending' AND medium = 'Phone UPS' ";
                 $result = $con->query($sql);
                 while($row = $result->fetch_assoc()){
                   ?>
                  <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$row["agent_name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_set"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_show"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_sold"] ?></td>
             
                  <td class="jsgrid-cell"><?=$row["status"] ?></td>
                  <td class="jsgrid-cell">
                  <button class="btn btn-success" onclick="approve(<?=$row['id']?>)" ><i class="fa fa-check"></i></button>
                  <button class="btn btn-danger" onclick="discard(<?=$row['id']?>)" ><i class="fa fa-times"></i></button>
                  </td>
                </tr>
                   <?php
                 }
              ?>
               
             
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <!-- /.card -->
        </div>      <div class="card custom-table-card table-dv extended-lead-requests" style="display:none;">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Extended Lead Requests</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell">Agent</th>
                  <th class="jsgrid-cell">Lead</th>
                  <th class="jsgrid-cell">APPT  SET  </th>
                  <th class="jsgrid-cell"> APPT SHOW  </th>
                  <th class="jsgrid-cell">APPT  SOLD  </th>
                  <th class="jsgrid-cell">Status</th>
                  <th class="jsgrid-cell">Operations</th>
                </tr>
              </thead>

              <tbody>

              <?php
                 $sql = "SELECT * FROM lead_capture WHERE verification = 'pending' AND medium = 'Extended Warranties' ";
                 $result = $con->query($sql);
                 while($row = $result->fetch_assoc()){
                   ?>
                  <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$row["agent_name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_set"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_show"] ?></td>
                  <td class="jsgrid-cell"><?=$row["appt_sold"] ?></td>
                  
                  <td class="jsgrid-cell"><?=$row["status"] ?></td>
                  <td class="jsgrid-cell">
                  <button class="btn btn-success" onclick="approve(<?=$row['id']?>)" ><i class="fa fa-check"></i></button>
                  <button class="btn btn-danger" onclick="discard(<?=$row['id']?>)" ><i class="fa fa-times"></i></button>
                  </td>
                </tr>
                   <?php
                 }
              ?>
               
             
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <!-- /.card -->
        </div>      <div class="card custom-table-card table-dv review-lead-requests" style="display:none;">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Review Lead Requests</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell">Agent</th>
                  <th class="jsgrid-cell">Lead</th>
                  <th class="jsgrid-cell">Filled Out  </th>
                 
                  <th class="jsgrid-cell">Status</th>
                  <th class="jsgrid-cell">Operations</th>
                </tr>
              </thead>

              <tbody>

              <?php
                 $sql = "SELECT * FROM lead_capture WHERE verification = 'pending' AND medium = '5 Star Reviews' ";
                 $result = $con->query($sql);
                 while($row = $result->fetch_assoc()){
                   ?>
                  <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$row["agent_name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["filled_out"] ?></td>
                  <td class="jsgrid-cell"><?=$row["status"] ?></td>
                  <td class="jsgrid-cell">
                  <button class="btn btn-success" onclick="approve(<?=$row['id']?>)" ><i class="fa fa-check"></i></button>
                  <button class="btn btn-danger" onclick="discard(<?=$row['id']?>)" ><i class="fa fa-times"></i></button>
                  </td>
                </tr>
                   <?php
                 }
              ?>
               
             
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <!-- /.card -->
        </div>
      </div><!-- col -->
    </div><!-- col -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>