<?php
session_start();
if ($_SESSION["role"] !== "Super Admin") {
    header("location: /index.php");
}
$page = 'view-admin';
include('../../header.php');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All Customer</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= $site_url; ?>">Home</a></li>
                        <li class="breadcrumb-item active">All Customer</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (isset($_GET['success']) && $_GET['success'] == 'edit') {
                        echo '<div id="alert" class="alert alert-success text-center" role="alert">';
                        echo ' Customer data has been successfully edited';
                        echo '</div>';
                    }
                    if (isset($_GET['success']) && $_GET['success'] == 'block') {
                        echo '<div id="alert" class="alert alert-success text-center" role="alert">';
                        echo ' Customer has been successfully blocked';
                        echo '</div>';
                    }
                    if (isset($_GET['success']) && $_GET['success'] == 'unblock') {
                        echo '<div id="alert" class="alert alert-success text-center" role="alert">';
                        echo ' Customer has been successfully unblocked';
                        echo '</div>';
                    }
                    if (isset($_GET['success']) && $_GET['success'] == 'del') {
                        echo '<div id="alert" class="alert alert-success text-center" role="alert">';
                        echo ' Customer has been successfully Deleted';
                        echo '</div>';
                    }
                    if (isset($_GET['success']) && $_GET['success'] == 'err') {
                        echo '<div id="alert" class="alert alert-danger text-center" role="alert">';
                        echo ' Opps! something went wrong';
                        echo '</div>';
                    }
                    ?>
                    <div class="card">
                        <?php
                        $sql = "SELECT * FROM `users` WHERE `admin_id` = '0' AND `role` = 'Admin'";
                        $result = $con->query($sql);
                        ?>
                        <div class="card-header">
                            <h3 class="card-title">Customers</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table table-striped border">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Paid</th>
                                    <th>Subscription</th>
                                    <th>Registered</th>
                                    <th>Block</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                <?php
                                while ($row = $result->fetch_assoc()) {
                                    $name = $row['first_name'] . " " . $row['last_name'];
                                    if ($row['paid'] == "1") {
                                        $paid = "Yes";
                                    } else {
                                        $paid = "No";
                                    }
                                    $today = time();
                                    $subscription = strtotime($row['subscription']);
                                    $datediff = $subscription - $today;
                                    $expire = round($datediff / (60 * 60 * 24));
                                    if ($expire<1) {
                                        $expire = "Expired";
                                    } else {
                                        $expire = $expire." days remaining";
                                    }
                                    ?>
                                    <tr>
                                        <td><?= $row['id']; ?>.</td>
                                        <td><a href="<?= $site_url; ?>/myaccount.php?D=<?= $row['id'];?>"><?= $name; ?></a></td>
                                        <td><?= $row['username']; ?></td>
                                        <td><?= $paid; ?></td>
                                        <td><?= $expire; ?></td>
                                        <td><?= date('d-m-Y h:i:sa', strtotime($row['created_at'])); ?></td>
                                        <?php if ($row['is_block'] == 0) { ?>
                                            <td><button type="button" class="btn btn-success" onclick="userBlock(<?php echo $row['id']; ?>)"><i class="fas fa-lock-open"></i></button></td>
                                        <?php } else { ?>
                                            <td><button type="button" class="btn btn-danger" onclick="userUnblock(<?php echo $row['id']; ?>)"><i class="fas fa-lock"></i></button></td>
                                        <?php } ?>
                                        <td><a href="<?= $site_url; ?>/pages/tables/edit-admin.php?id=<?= $row['id']; ?>"><button type="button" class="btn btn-primary"> <i class="fa fa-edit"></i> </button></a></td>
                                        <td><button type="button" class="btn btn-danger" onclick="deladmin(<?php echo $row['id']; ?>)"> <i class="fa fa-trash"></i> </button></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script>
        function deladmin(_id) {
            if (confirm("Are you sure you want to Delete?")) {
                $.ajax({
                    method: "POST",
                    url: "delete-admin.php",
                    data: {
                        id: _id
                    }
                }).done(function(_res) {
                    var res = JSON.parse(_res);
                    if (res.status == "200") {
                        window.location.assign("view-admin.php?success=del");
                    } else {
                        window.location.assign("view-admin.php?success=err");
                    }
                });
            }
        }

        function userBlock(id) {
            if (confirm("Are you sure you want to Block?")) {
                block = "../forms/block-user.php?id=" + id + "&action=block";
                window.location.href = block;
            }

        }

        function userUnblock(id) {
            if (confirm("Are you sure you want to Unblock?")) {
                unblock = "../forms/block-user.php?id=" + id + "&action=unblock";
                window.location.href = unblock;
            }

        }
        setTimeout(function() {
            $("#alert").hide();
        }, 3000);
    </script>
</div>
<!-- /.content-wrapper -->
<?php
include '../../footer.php';
?>