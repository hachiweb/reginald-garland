<?php
session_start();
if ($_SESSION["role"] != "Super Admin") {
  header("location: /index.php");
}
  $page = 'edit-admin';
  include('../../header.php'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Edit Customer</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Edit Customer</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <!-- left column -->
        <div class="col-md-8 mx-auto">
        <?php 
            $getid = $_GET['id'];
            if(isset($_GET['success']) && $_GET['success'] == 1){
                echo '<div class="alert alert-success" role="alert">';
                echo 'Admin <i>'.$_GET['name'].' & Password '.$_GET['pass'].'</i> has been created successfuly';
                echo '</div>';
            }
            $sql = "SELECT * FROM `users` WHERE `admin_id` = '0' AND `id` = '$getid'";
            $result = $con->query($sql);
            $row = $result->fetch_assoc();
          ?>    
          <!-- general form elements -->
          <div class="card card-primary custom-form-card">
            <div class="card-header">
              <h3 class="card-title">Edit Customer</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="../forms/sub-admin-edit.php" method="post" enctype="multipart/form-data" >
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>First Name</label>
                      <input required type="text" name="first_name" class="form-control" value="<?=$row['first_name'];?>" placeholder="First Name">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Last Name</label>
                      <input required type="text" name="last_name" class="form-control" value="<?=$row['last_name'];?>" placeholder="Last Name">
                    </div>
                  </div>
                  <input type="hidden" name="id" value="<?=$row['id'];?>">
                <div class="col-sm-6">
                <div class="form-group">
                  <label>Contact No.</label>
                  <input required type="tel" name="phone" class="form-control" value="<?=$row['contact'];?>" placeholder="Contact">
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group">
                  <label>Email</label>
                  <input required type="email" name="username" class="form-control" value="<?=$row['username'];?>" placeholder="Email">
                </div>
                </div>
                </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Profile Photo</label>
                    <div class="input-group">
                    <div class="custom-file">
                      <input type="file" accept="image/*" name="Profile" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    </div>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div><!-- form-dv -->
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('../../footer.php');
  ?>