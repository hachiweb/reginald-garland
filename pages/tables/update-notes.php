<?php
    ob_start();
    session_start();
    $page = 'agent-form';
    include('../../header.php');
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $notes = "SELECT * FROM `lead_capture` WHERE `id`='$id'";
        $result = mysqli_query($con, $notes);
        if($result)
        {
            // echo '<pre>';
            $row = $result->fetch_assoc();
            
            // print_r($row);

        }
        else
        {   
            echo $notes;
            echo mysqli_error($con);
        }    
    }
    else{
        echo '<div class="jumbotron jumbotron-fluid text-center "><div class="container alert alert-danger"><h4 class="display-6">No Notification id passed</h4></div></div>';
        exit;
    }

// updation

    if(isset($_POST['update']))
    {
        $agent_name = $_POST['agent_name'];
        $name   = $_POST['name'];
        $notes = $_POST['notes'];
        $sql = "UPDATE `lead_capture` SET `agent_name`='$agent_name',`name`='$name',`notes`='$notes' WHERE `id`='$id'";
        $query_run = mysqli_query($con, $sql);
        if($query_run)
        {
            header('location:'.$site_url.'/pages/tables/view-notes.php?id='.$id.'&status=update');
            exit();
        }
        else
        {
            echo $sql;
            echo mysqli_error($con);
        }

    }
 

    mysqli_close($con);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Notes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
                        <li class="breadcrumb-item active">Update Notes</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8 mx-auto">
                    <!-- general form elements -->
                    <?php
                        if(isset($_GET['status']) && $_GET['status']=='true'){
                            echo '<h6 class="alert alert-success text-center deletesuccess">Record updated</h6>';
                        }
                    ?>
                    <div class="card card-primary custom-form-card form-dv form-internet d-block">
                        
                        <div class="card-header">
                            <h3 class="card-title text-center">Update Notes</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="" method="POST">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Agent Name</label>
                                    <input type="text" name="agent_name" class="form-control" id="field"
                                        value="<?php echo $row['agent_name']; ?>" />
                                    <p>Characters left <span id="charNum"></span></p>
                                </div>
                                <div class="form-group">
                                    <label>Customer Name</label>
                                    <input type="text" name="name" class="form-control" id="field"
                                        value="<?php echo $row['name']; ?>" />
                                    <p>Characters left <span id="charNum"></span></p>
                                </div>

                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea name="notes" class="form-control"
                                        rows="3"><?php echo $row['notes']; ?></textarea>
                                </div>
                                 
                                </div>
                            </div>
                            <div class="card-footer">
                                <input type="submit" class="btn btn-primary" name="update" value="UPDATE">
                            </div>
                        </form>
                    </div><!-- form-dv -->
                    <!-- /.card -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
$(document).ready(function() {
    $("#field").on('input', function(e) {
        el = $(this);
        if (el.val().length >= 40) {
            el.val(el.val().substr(0, 40));
        } else {
            $("#charNum").text(40 - el.val().length);
        }
    });
});
</script>

<?php include('../../footer.php'); ?>


<!-- *****************************update notes************************* -->
