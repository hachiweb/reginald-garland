<?php
$auth ="admin";
include '../../header.php';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Agents</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">All Agents</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <?php
                $db = new DB();
                $sql = "SELECT * FROM `referral`";
                $result = $db->executeQuery($sql);
                ?>
              <div class="card-header">
                <h3 class="card-title">Agent</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-striped">
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Email</th>
                    <th>Referral Code</th>
                    <th>Total Referred</th>
                  </tr>
                  <?php
                  while ($agent = mysqli_fetch_array($result)) {
                        $refcode = $agent['referral_code'];
                        $sqltax = "SELECT count(referral_code) AS total  FROM `tax_preparation_questionnaire` WHERE `referral_code`='$refcode'";
                        $resulttax = $db->executeQuery($sqltax);
                        $tax = mysqli_fetch_array($resulttax)
                        ?>
                  <tr>
                    <td><?=$agent['id'];?>.</td>
                    <td><?=$agent['name'];?></td>
                    <td><?=$agent['company'];?></td>
                    <td><?=$agent['email'];?></td>
                    <td><?=$agent['referral_code'];?></td>
                    <td><?=(!empty($tax['total']))?$tax['total']:"N/A";?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
include '../../footer.php';
?>