<?php
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: /index.php");
}
$page = 'group-table';
include('../../header.php');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Lead Source Legend</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Lead Source Legend</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-10 m-auto">

        <div class="card custom-table-card table-dv table-internet d-block">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Lead Source Legend</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table" id="xls1">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell">Group</th>

                  <th class="jsgrid-cell"> SET  CODE</th>
                  <th class="jsgrid-cell"> SHOW  CODE</th>
                  <th class="jsgrid-cell"> SOLD CODE </th>
                </tr>
              </thead>

              <tbody>

                <?php
                $sql = "SELECT * FROM `source` WHERE `admin_id` = '$userID'";
                $result = $con->query($sql);
                while($row = $result->fetch_assoc()){
                 ?>
                 <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$row["source"] ?></td>

                  <td class="jsgrid-cell"><?=$row["set_code"] ?></td>
                  <td class="jsgrid-cell"><?=$row["show_code"] ?></td>
                  <td class="jsgrid-cell"><?=$row["sold_code"] ?></td>
                </tr>
                <?php
              }
              ?>


            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="text-center pb-3">
          <button type="button" class="btn btn-outline-primary" id="exp1"><i class="fas fa-save"></i> Save </button>
          <button type="button" class="btn btn-outline-primary printBdc">
            <i class="fa fa-print" aria-hidden="true"></i> Print 
          </button>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.card -->


    </div><!-- col -->
  </div><!-- col -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#exp1").click(function() {
      $("#xls1").table2excel({
        exclude: ".excludeThisClass",
        name: "Lead Source Legend",
        filename: "Lead Source Legend",
        preserveColors: false
      });
    });
  });
</script>

<script>
  $('.printBdc').on('click', function() {  
    window.print();  
    return false;
  });
</script>
<?php include('../../footer.php'); ?>