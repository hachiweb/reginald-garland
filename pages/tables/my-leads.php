<?php
session_start();
if ($_SESSION["role"] != "Agent") {
  header("location: /index.php");
}
  $page = 'lead requests';
  include('../../header.php');
  $agent_name = $_SESSION['alais'];
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Lead Requests Table</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Lead Requests Table</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-10 m-auto">
      <div class="bg-white mb-4 radio-select-type-dv">
        <div class="card-body">
          <div class="form-group clearfix text-center m-0">
            <div class="icheck-primary d-inline">
              <input type="radio" name="my-leads-radio" id="r1" checked="checked">
              <label for="r1">Approved</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="my-leads-radio" id="r2">
              <label for="r2">Discarded</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="my-leads-radio" id="r3">
              <label for="r3">Pending</label>
            </div>
          </div>
        </div>
      </div>
        <div class="card custom-table-card table-dv approved" style="display:block;">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Approved</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                <th class="jsgrid-cell">#</th>
                  <th class="jsgrid-cell">Lead</th>
                  <th class="jsgrid-cell">Medium</th>
                </tr>
              </thead>
              <tbody>
              <?php
              $i=0;
                 $sql = "SELECT * FROM `lead_capture` WHERE `verification` = 'approved' AND `agent_id` = '$userID'";
                 $result = $con->query($sql);
                 while($row = $result->fetch_assoc()){
                     $i++;
                   ?>
                  <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$i ?></td>
                  <td class="jsgrid-cell"><?=$row["name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["medium"] ?></td>
                </tr>
                   <?php
                 }
              ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <!-- /.card -->
        </div>
        <!-- /.card -->
        <div class="card custom-table-card table-dv discarded" style="display:none;">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Discarded</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                <th class="jsgrid-cell">#</th>
                  <th class="jsgrid-cell">Lead</th>
                  <th class="jsgrid-cell">Medium</th>
                  <th class="jsgrid-cell">Operations</th>
                </tr>
              </thead>
              <tbody>
              <?php
              $i=0;
              $sql = "SELECT * FROM `lead_capture` WHERE `verification` = 'discard' AND `agent_id` = '$userID'";
                 $result = $con->query($sql);
                 while($row = $result->fetch_assoc()){   
                     $i++;
                   ?>
                  <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$i ?></td>
                  <td class="jsgrid-cell"><?=$row["name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["medium"] ?></td>
                  <td class="jsgrid-cell">
                  <button type="button" class="btn btn-primary" onclick="edit(<?=$row['id'] ?>,'<?=$row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                  <button class="btn btn-danger" onclick="delRqt(<?=$row['id']?>)" ><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                   <?php
                 }
              ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <!-- /.card -->
        </div>     
        <div class="card custom-table-card table-dv pending" style="display:none;">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Pending</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                <th class="jsgrid-cell">#</th>
                  <th class="jsgrid-cell">Lead</th>
                  <th class="jsgrid-cell">Medium</th>
                  <th class="jsgrid-cell">Operations</th>
                </tr>
              </thead>
              <tbody>
              <?php
              $i=0;
              $sql = "SELECT * FROM `lead_capture` WHERE `verification` = 'pending' AND `agent_id` = '$userID'";
                 $result = $con->query($sql);
                 while($row = $result->fetch_assoc()){   
                     $i++;
                   ?>
                  <tr class="jsgrid-row">
                  <td class="jsgrid-cell"><?=$i ?></td>
                  <td class="jsgrid-cell"><?=$row["name"] ?></td>
                  <td class="jsgrid-cell"><?=$row["medium"] ?></td>
                  <td class="jsgrid-cell">
                  <button type="button" class="btn btn-primary" onclick="edit(<?=$row['id'] ?>,'<?=$row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                  <button class="btn btn-danger" onclick="delRqt(<?=$row['id']?>)" ><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                   <?php
                 }
              ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <!-- /.card -->
        </div>     
      </div><!-- col -->
    </div><!-- col -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>