<?php
  include('../../header.php');
  $yearfrom = (!empty($_POST['year_from']))?$_POST['year_from']:date("Y");
  $yearto = (!empty($_POST['year_to']))?$_POST['year_to']:date("Y");
  $MonthYear = (!empty($_POST['MonthYear']))?$_POST['MonthYear']:date("Y");
  $MonthFrom = (!empty($_POST['MonthFrom']))?$_POST['MonthFrom']:date('m', strtotime('-1 month'));
  $MonthTo = (!empty($_POST['MonthTo']))?$_POST['MonthTo']:date('m', strtotime('-1 month'));
  $MonthFromObj   = DateTime::createFromFormat('!m', $MonthFrom);
  $MonthFromName = $MonthFromObj->format('F');
  $MonthToObj   = DateTime::createFromFormat('!m', $MonthTo);
  $MonthToName = $MonthToObj->format('F');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>GPS Service Mining Tracker</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
                        <li class="breadcrumb-item active">GPS Service Mining Tracker</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="bg-white mb-4 radio-select-type-dv">
            <div class="card-body">
                <div class="form-group clearfix text-center m-0">
                    <div class="icheck-primary d-inline">
                        <input type="radio" name="agent-table-radio" id="t1" checked="checked">
                        <label for="t1">TRACK BY YEAR</label>
                    </div>
                    <div class="icheck-primary d-inline">
                        <input type="radio" name="agent-table-radio" id="t2">
                        <label for="t2">TRACK BY MONTH</label>
                    </div>
                    <div class="icheck-primary d-inline">
                        <input type="radio" name="agent-table-radio" id="t3">
                        <label for="t3">TRACK BY DATE</label>
                    </div>
                </div>
            </div>
        </div><!-- radio-select-type-dv -->

        <div class="row">

            <div class="col-lg-10 m-auto">
                <div class="card custom-table-card table-dv table-internet d-block">
                    <div class="card-header bg-blue text-white">
                        <h3 class="card-title">TRACK BY YEAR</h3>
                    </div>
                    <div class="card-header text-center text-bold" style="background-color:rgb(242,242,242);">
                        <h3 class="card-title">YEARLY SERVICE REPORT</h3>
                    </div>

                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="jsgrid-table text-center">
                            <thead class="jsgrid-grid-header">
                                <tr class="jsgrid-alt-row">
                                <form action="" method="post">
                                    <th class="jsgrid-cell" colspan="2" class="text-center">
                                        <div class="form-group ">
                                            <label for="">SELECT FROM YEAR</label>
                                                <select name="year_from" id="year_from" class="form-control">
                                                <option value="<?=$yearfrom;?>" selected><?=$yearfrom;?></option>
                                                <?php for ($i=2000; $i < 2100; $i++) { ?>
                                                    <option value="<?=$i;?>"><?=$i;?></option>
                                                <?php
                                                }
                                                ?>
                                                </select>
                                        </div>
                                    </th>
                                    <th class="jsgrid-cell" colspan="2">
                                        <div class="form-group">
                                            <label for="">SELECT TO YEAR</label>
                                                <select name="year_to" id="year_to" class="form-control">
                                                <option value="<?=$yearto;?>" selected><?=$yearto;?></option>
                                                <?php for ($i=2000; $i < 2100; $i++) { ?>
                                                    <option value="<?=$i;?>"><?=$i;?></option>
                                                <?php
                                                }
                                                ?>
                                                </select>
                                        </div>
                                    </th>
                                    <th class="jsgrid-cell" colspan="">
                                        <div class="">
                                            <button type="submit" class="btn btn-primary">SEARCH</button>
                                        </div>
                                    </th>
                                </form>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div id="year_show_again">

                            <table class="jsgrid-table text-center mt-3">
                                <thead class="jsgrid-grid-header">
                                    <tr class="jsgrid-alt-row">
                                        <th class="jsgrid-cell" colspan="">YEAR</th>
                                        <th class="jsgrid-cell" colspan="">INTRESTED</th>
                                        <th class="jsgrid-cell" colspan="">NOT INTRESTED</th>
                                        <th class="jsgrid-cell" colspan="">NO ANSWER</th>
                                        <th class="jsgrid-cell" colspan="">BAD CONTACT INFO</th>
                                        <th class="jsgrid-cell" colspan="">SOLD</th>
                                        <th class="jsgrid-cell" colspan="">TOTAL LEADS</th>
                                    </tr>
                                </thead>

                                <tbody>
                                <?php 
                                    $yearbysql ="SELECT YEAR(created_at) AS Year FROM lead_capture WHERE YEAR(created_at) BETWEEN '$yearfrom' AND '$yearto' GROUP BY Year";
                                    $result = $con->query($yearbysql);
                                    while ($row = $result->fetch_assoc()) {
                                        $year = $row['Year'];
                                        $sqlHot = "SELECT * FROM lead_capture WHERE `verification` = 'approved' AND YEAR(created_at) = '$year'";
                                        $resultHot = $con->query($sqlHot);
                                        while ($rowHot = $resultHot->fetch_assoc()) {
                                            $status [$year][] = $rowHot['status'];
                                            $yearcreated = $rowHot['yearcreated'];
                                         }
                                        }
                                        foreach ($status as $key => $value) { 
                                            $data = (array_count_values($value));
                                            $INTRESTED = (!empty($data['Hot']))?$data['Hot']:"0";
                                            $cold = (!empty($data['Cold']))?$data['Cold']:"0";
                                            $DidNotBuy = (!empty($data['Came In/Did Not Buy']))?$data['Came In/Did Not Buy']:"0";
                                            $NotBuying = (!empty($data['Not Buying']))?$data['Not Buying']:"0";
                                            $NOTINTRESTED = $cold+$DidNotBuy+$NotBuying;
                                            $NOANSWER = (!empty($data['No Number']))?$data['No Number']:"0";
                                            $BADCONTACTINFO = (!empty($data['Flaky']))?$data['Flaky']:"0";
                                            $SOLD = (!empty($data['Purchased']))?$data['Purchased']:"0";
                                            $TOTAL = $INTRESTED+$NOTINTRESTED+$NOANSWER+$BADCONTACTINFO+$SOLD;
                                            $total_n_stats = array(
                                                  "INTRESTED" => $INTRESTED,
                                                  "NOTINTRESTED" =>  $NOTINTRESTED,
                                                  "NOANSWER" => $NOANSWER,
                                                  "BADCONTACTINFO" => $BADCONTACTINFO,
                                                  "SOLD" => $SOLD,
                                                  "TOTAL" => $TOTAL
                                                );
                                            ?>
                                            <tr class="jsgrid-row">
                                                <td class="jsgrid-cell text-info"><?=$key;?></td>
                                                <td class="jsgrid-cell"><?=$INTRESTED;?></td>
                                                <td class="jsgrid-cell"><?=$NOTINTRESTED;?></td>
                                                <td class="jsgrid-cell"><?=$NOANSWER;?></td>
                                                <td class="jsgrid-cell"><?=$BADCONTACTINFO;?></td>
                                                <td class="jsgrid-cell"><?=$SOLD;?></td>
                                                <td class="jsgrid-cell text-info text-bold"><?=$TOTAL;?></td>
                                            </tr>
                                            <?php
                                        }
                                        echo "<script>var stats = ".json_encode($total_n_stats)."; </script>";
                                        ?>
                                </tbody>

                            </table>
                            <section class="row mt-5 p-3" style="background-color:rgb(252,252,252);">
                                <div class="col-lg-12 mb-3" style="background-color:rgb(238,238,238);">
                                    <div class="text-center">
                                        <div id="myDivchart" class="mx-auto"></div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 border">
                                    <div class="">
                                        <h3 class="text-center lead_clr_back">LEADS/INTERESTED</h3>
                                        <div id="myDiv"></div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 border">
                                    <div class=" ">
                                        <h3 class="text-center lead_clr_back">LEADS/NOT INTERESTED</h3>
                                        <div id="myDiv2"></div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 border">
                                    <div class=" ">
                                        <h3 class="text-center lead_clr_back">LEADS/NO ANSWER</h3>
                                        <div id="myDiv3" class=""></div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 border mt-2">
                                    <div class=" ">
                                        <h3 class="text-center lead_clr_back">LEADS/BAD CONTACT INFO</h3>
                                        <div id="myDiv4"></div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 border mt-2">
                                    <div class="text-center">
                                        <h3 class="text-center lead_clr_back">LEADS/SOLD</h3>
                                        <div id="myDiv5"></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <!-- /.card -->
                </div>
                <!-- /.card -->
                <div class="card custom-table-card table-dv table-phone">
                    <div class="card-header bg-blue text-white">
                        <h3 class="card-title">TRACK BY MONTH</h3>
                    </div>
                    <div class="card-header text-center text-bold" style="background-color:rgb(242,242,242);">
                        <h3 class="card-title">MONTHLY SERVICE REPORT</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="jsgrid-table text-center">
                            <thead class="jsgrid-grid-header">
                                <tr class="jsgrid-alt-row">
                                <form action="" method="post">
                                    <th class="jsgrid-cell" class="text-center" colspan="2">
                                        <div class="form-group">
                                            <label for="">SELECT YEAR</label>
                                            <select name="MonthYear" id="MonthYear" class="form-control">
                                                <option value="<?=$MonthYear;?>" selected><?=$MonthYear;?></option>
                                                <?php for ($i=2000; $i < 2100; $i++) { ?>
                                                    <option value="<?=$i;?>"><?=$i;?></option>
                                                <?php
                                                }
                                                ?>
                                                </select>
                                        </div>
                                    </th>
                                    <th class="jsgrid-cell" colspan="2" class="text-center">
                                        <div class="form-group ">
                                            <label for="">SELECT FROM MONTH</label>
                                            <select name="MonthFrom" id="MonthFrom" class="form-control">
                                                <option value="<?=$MonthFrom;?>" selected><?=$MonthFromName;?></option>
                                                <option value="1">JANUARY</option>
                                                <option value="2">FEBRUARY</option>
                                                <option value="3">MARCH</option>
                                                <option value="4">APRIL</option>
                                                <option value="5">MAY</option>
                                                <option value="6">JUNE</option>
                                                <option value="7">JULY</option>
                                                <option value="8">AUGUST</option>
                                                <option value="9">SEPTEMBER</option>
                                                <option value="10">OCTOBER</option>
                                                <option value="11">NOVEMBER</option>
                                                <option value="12">DECEMBER</option>
                                            </select>
                                        </div>
                                    </th>

                                    <th class="jsgrid-cell" colspan="2">
                                        <div class="form-group">
                                            <label for="">SELECT TO MONTH</label>
                                            <select name="MonthTo" id="MonthTo" class="form-control">
                                                <option value="<?=$MonthTo;?>" selected><?=$MonthToName;?></option>
                                                <option value="1">JANUARY</option>
                                                <option value="2">FEBRUARY</option>
                                                <option value="3">MARCH</option>
                                                <option value="4">APRIL</option>
                                                <option value="5">MAY</option>
                                                <option value="6">JUNE</option>
                                                <option value="7">JULY</option>
                                                <option value="8">AUGUST</option>
                                                <option value="9">SEPTEMBER</option>
                                                <option value="10">OCTOBER</option>
                                                <option value="11">NOVEMBER</option>
                                                <option value="12">DECEMBER</option>
                                            </select>
                                        </div>
                                    </th>
                                    <th class="jsgrid-cell" colspan="">
                                        <div class="">
                                            <button type="submit" class="btn btn-primary" id="month_show">SEARCH</button>
                                        </div>
                                    </th>
                                </form>
                                </tr>
                            </thead>
                        </table>
                        <div id="month_show_again">
                        <table class="jsgrid-table text-center mt-3">
                            <thead class="jsgrid-grid-header">
                                <tr class="jsgrid-alt-row">
                                    <th class="jsgrid-cell" colspan="">MONTH</th>
                                    <th class="jsgrid-cell" colspan="">INTRESTED</th>
                                    <th class="jsgrid-cell" colspan="">NOT INTRESTED</th>
                                    <th class="jsgrid-cell" colspan="">NO ANSWER</th>
                                    <th class="jsgrid-cell" colspan="">BAD CONTACT INFO</th>
                                    <th class="jsgrid-cell" colspan="">SOLD</th>
                                    <th class="jsgrid-cell" colspan="">TOTAL LEADS</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr class="jsgrid-row">
                                    <td class="jsgrid-cell text-info">JANUARY</td>
                                    <td class="jsgrid-cell">40</td>
                                    <td class="jsgrid-cell">55</td>
                                    <td class="jsgrid-cell">40</td>
                                    <td class="jsgrid-cell">7</td>
                                    <td class="jsgrid-cell">11</td>
                                    <td class="jsgrid-cell text-info text-bold">153</td>
                                </tr>
                                <tr class="jsgrid-row">
                                    <td class="jsgrid-cell text-info">FEBRUARY</td>
                                    <td class="jsgrid-cell">40</td>
                                    <td class="jsgrid-cell">55</td>
                                    <td class="jsgrid-cell">40</td>
                                    <td class="jsgrid-cell">7</td>
                                    <td class="jsgrid-cell">11</td>
                                    <td class="jsgrid-cell text-info text-bold">153</td>
                                </tr>
                                <tr class="jsgrid-row">
                                    <td class="jsgrid-cell text-danger text-bold">TOTAL</td>
                                    <td class="jsgrid-cell text-bold">40</td>
                                    <td class="jsgrid-cell text-bold">55</td>
                                    <td class="jsgrid-cell text-bold">40</td>
                                    <td class="jsgrid-cell text-bold">7</td>
                                    <td class="jsgrid-cell text-bold">11</td>
                                    <td class="jsgrid-cell text-bold">153</td>
                                </tr>
                            </tbody>
                        </table>
                       <div class="row p-2 mt-4">
                       <div class="col-lg-5 border"style="background-color:rgb(252,252,252);">
                            <div class="">
                                <h3 class="text-center lead_clr_back">JANUARY 2019</h3>
                                <div id="myDivmonth"></div>
                            </div>
                        </div>
                       </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->


                <div class="card custom-table-card table-dv table-extended">
                    <div class="card-header bg-blue text-white">
                        <h3 class="card-title">TRACK BY DATE</h3>
                    </div>
                    <div class="card-header text-center text-bold" style="background-color:rgb(242,242,242);">
                        <h3 class="card-title ">DALY SERVICE REPORT</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="jsgrid-table text-center">
                            <thead class="jsgrid-grid-header">
                                <tr class="jsgrid-alt-row">
                                    <th class="jsgrid-cell">
                                        <div class="form-group">
                                            <label for="">SELECT FROM DATE</label>
                                            <input type="date" value="" class="form-control">
                                        </div>
                                    </th>
                                    <th class="jsgrid-cell">
                                        <div class="form-group">
                                            <label for="">SELECT TO DATE</label>
                                            <input type="date" value="" class="form-control">
                                        </div>
                                    </th>
                                    <th class="jsgrid-cell" colspan="">
                                        <div class="">
                                            <button type="button" class="btn btn-primary" id="date_show">SEARCH</button>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div style="display:none;" id="date_show_again">
                            <table class="jsgrid-table text-center mt-3">
                                <thead class="jsgrid-grid-header">
                                    <tr class="jsgrid-alt-row">
                                        <th class="jsgrid-cell" colspan="">DATE</th>
                                        <th class="jsgrid-cell" colspan="">INTRESTED</th>
                                        <th class="jsgrid-cell" colspan="">NOT INTRESTED</th>
                                        <th class="jsgrid-cell" colspan="">NO ANSWER</th>
                                        <th class="jsgrid-cell" colspan="">BAD CONTACT INFO</th>
                                        <th class="jsgrid-cell" colspan="">SOLD</th>
                                        <th class="jsgrid-cell" colspan="">TOTAL LEADS</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr class="jsgrid-row">
                                        <td class="jsgrid-cell text-info">mm/dd/yyy</td>
                                        <td class="jsgrid-cell">40</td>
                                        <td class="jsgrid-cell">55</td>
                                        <td class="jsgrid-cell">40</td>
                                        <td class="jsgrid-cell">7</td>
                                        <td class="jsgrid-cell">11</td>
                                        <td class="jsgrid-cell text-info text-bold">153</td>
                                    </tr>
                                    <tr class="jsgrid-row">
                                        <td class="jsgrid-cell text-info">mm/dd/yyy</td>
                                        <td class="jsgrid-cell">40</td>
                                        <td class="jsgrid-cell">55</td>
                                        <td class="jsgrid-cell">40</td>
                                        <td class="jsgrid-cell">7</td>
                                        <td class="jsgrid-cell">11</td>
                                        <td class="jsgrid-cell text-info text-bold">153</td>
                                    </tr>
                                    <tr class="jsgrid-row">
                                        <td class="jsgrid-cell text-danger text-bold">TOTAL</td>
                                        <td class="jsgrid-cell text-bold">40</td>
                                        <td class="jsgrid-cell text-bold">55</td>
                                        <td class="jsgrid-cell text-bold">40</td>
                                        <td class="jsgrid-cell text-bold">7</td>
                                        <td class="jsgrid-cell text-bold">11</td>
                                        <td class="jsgrid-cell text-bold">153</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="jsgrid-table text-center mt-3">
                                <thead class="jsgrid-grid-header">
                                    <tr class="jsgrid-alt-row">
                                        <th class="jsgrid-cell" colspan="">DATE</th>
                                        <th class="jsgrid-cell" colspan="">NAME OF LEADS</th>
                                        <th class="jsgrid-cell" colspan="">CALL RESULT</th>
                                        <th class="jsgrid-cell" colspan="2">ADDITIONAL NOTES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="jsgrid-row">
                                        <td class="jsgrid-cell">mm/dd/yyy</td>
                                        <td class="jsgrid-cell text-info">DIPENKUMER PATEL</td>
                                        <td class="jsgrid-cell">INTRESTED</td>
                                        <td class="jsgrid-cell" colspan="2"></td>
                                    </tr>
                                    <tr class="jsgrid-row">
                                        <td class="jsgrid-cell">mm/dd/yyy</td>
                                        <td class="jsgrid-cell text-info">DIPENKUMER PATEL</td>
                                        <td class="jsgrid-cell">NOT INTRESTED</td>
                                        <td class="jsgrid-cell" colspan="2"></td>
                                    </tr>
                                    <tr class="jsgrid-row">
                                        <td class="jsgrid-cell">mm/dd/yyy</td>
                                        <td class="jsgrid-cell text-info">DIPENKUMER PATEL</td>
                                        <td class="jsgrid-cell">NO ANSWER</td>
                                        <td class="jsgrid-cell" colspan="2"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card -->

                    </div><!-- col -->
                </div><!-- col -->
    </section>
    <!-- /.content -->
</div>

<script>
function del(_id) {
    $.ajax({
        method: "POST",
        url: "../forms/delete-agent-data.php",
        data: {
            id: _id
        }
    }).done(function(_res) {
        var res = JSON.parse(_res);
        if (res.status == "200") {
            window.location.assign("agent-table.php?success=del");
        } else {
            window.location.assign("agent-table.php?success=err");
        }
    });

}

function edit(_id, _medium) {
    $.ajax({
        method: "POST",
        url: "../forms/set-session-for-agent-edit.php",
        data: {
            id: _id
        }
    }).done(function(_res) {
        var res = JSON.parse(_res);
        if (res.status == "200") {

            window.location.assign("/pages/forms/agent-edit.php?medium=" + _medium);
        } else {
            window.location.assign("agent-table.php?success=err");
        }
    });

}
setTimeout(function() {
    $("#alert").hide();
}, 3000);

//----year month date hide/show-----
$(document).ready(function() {
    $("#year_show").click(function() {
        $("#year_show_again").toggle();
    });

    $("#month_show").click(function() {
        $("#month_show_again").toggle();
    });

    $("#date_show").click(function() {
        $("#date_show_again").toggle();
    });
});
//----year month date hide/show-----

//------All pie Charts------
var data = [{
    values: [stats.TOTAL, stats.INTRESTED],
    labels: ['Leads Annual', 'Interested'],
    type: 'pie'
}];

var layout = {
    height: 320,
    width: 400,
};

Plotly.newPlot('myDiv', data, layout);
// Plotly pie chart 1

var data = [{
    values: [stats.TOTAL, stats.NOTINTRESTED],
    labels: ['Leads Annual', 'Not Interested'],
    type: 'pie'
}];

var layout = {
    height: 320,
    width: 400
};

Plotly.newPlot('myDiv2', data, layout);
// Plotly pie chart 2

var data = [{
    values: [stats.TOTAL, stats.NOANSWER],
    labels: ['Leads Annual', 'No Answer'],
    type: 'pie'
}];

var layout = {
    height: 320,
    width: 400
};

Plotly.newPlot('myDiv3', data, layout);
// Plotly pie chart 3

var data = [{
    values: [stats.TOTAL, stats.BADCONTACTINFO],
    labels: ['Leads Annual', 'Bad Contact Info'],
    type: 'pie'
}];

var layout = {
    height: 320,
    width: 400
};

Plotly.newPlot('myDiv4', data, layout);
// Plotly pie chart 4


var data = [{
    values: [stats.TOTAL, stats.SOLD],
    labels: ['Leads Annual', 'Sold'],
    type: 'pie'
}];

var layout = {
    height: 320,
    width: 400
};

Plotly.newPlot('myDiv5', data, layout);
// Plotly pie chart 5


var data = [{
    x: ['BAD CONTACT INFO', 'NO ANSWER', 'NOT INTERESTED', 'SOLD', 'TOTAL LEADS'],
    y: [20, 14, 23, 23, 44],
    type: 'bar'
}];

Plotly.newPlot('myDivchart', data);
// Plotly bar chart for year


var data = [{
    values: [40, 55,40,7,11,153],
    labels: ['INTERESTED', 'NOT INTERESTED','NO ANSWER','BAD CONTACT INFO','SOLD','TOTAL LEADS'],
    type: 'pie'
}];

var layout = {
    height: 400,
    width: 500,
};

Plotly.newPlot('myDivmonth', data, layout);
// Plotly pie chart 1 for month
</script>
<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>