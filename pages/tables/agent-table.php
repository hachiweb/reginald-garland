<?php
session_start();
$page = 'agent-table';
include('../../header.php');
$agent_alais = $_SESSION['alais'];
$sql = "SELECT * FROM lead_capture WHERE `admin_id` = '$userID' GROUP BY `agent_id`";
  $agent_result = $con->query($sql);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Leads</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Leads</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- filter sections -->
  <section class="content-header  mx-5">
    <div class="container-fluid">
      <div class="row mb-2">
        
      </div>
      <!-- <div class="row">
      <form>
        <input type="text" onkeyup="showResult(this.value)" class="form-control">
        <div id="livesearch" class="text-center bg-white" style="display:none; position:absolute; width:200px;z-index: 1; border-radius:5px;padding:7px;"></div>
      </form>
    </div> -->
    <?php

    // // echo $_SESSION["role"];
    // echo $userID;
    if ($_SESSION["role"] == "Admin") {
      ?>
      <div class="row">
        <form action="" method="POST" id="form_submit">
        <div class="row">
           
          <div class="col-lg-4">
          <label for="from">From</label>
              <input type="text" class="form-control font-weight-bold" autocomplete="off" placeholder="From Date" id="st_date"
              value="<?php echo $_POST['st_date']; ?>"  <?php if(isset($_POST['st_date'])=='1'){echo 'selected';} ?>  name="st_date" required>
          </div>
          <div class="col-lg-4">
          <label for="to">To</label>
              <input type="text" class="form-control font-weight-bold"value="<?php echo $_POST['end_date']; ?>"  <?php if(isset($_POST['end_date'])=='1'){echo 'selected';} ?> autocomplete="off" placeholder="To Date" id="end_date"
                  name="end_date" required>
          </div>
          <div class="col-lg-4">
           <div class="form-group">
           <label for="search">Search</label>
           <button style="" id="apply_filter"name="apply_filter" type="submit" class="btn-dark form-control"><i class="fa fa-search"></i></button>
           </div>
          </div>
          </div>
        </form>
      </div>
      <?php
    }
    // if($_SESSION["role"] == "Agent" && isset($_GET) && !empty($_GET["agent_id"])  ) {

      ?>
      
     <?php
  //  }
   ?>
    </div>
  </section>
  
<?php 
//  if ($_SESSION["role"] == "Admin"){
//  if(isset($_POST["apply_filter"])){
//   // $agent_id =$_POST['agent_id'];
//   $from_date = $_POST['st_date'];
//   $to_date = $_POST['end_date']; 
   
//   $sqli1 = "SELECT * FROM lead_capture WHERE ((date(created_at)) BETWEEN '$from_date' AND    '$to_date') ";
//   // print_r($sqli1);
// $fetch_data1 = $con->query($sqli1);
// $agent_data1 =$fetch_data1->fetch_assoc();
 
// if(($agent_data1 > 0)){

  
  ?>
  <!-- Main content -->
  <section class="content">
    <div class="bg-white mb-4 radio-select-type-dv">
      <div class="card-body">
        <div class="form-group clearfix text-center m-0">
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t1" checked="checked">
            <label for="t1">Internet UPS</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t2">
            <label for="t2">Phone UPS</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t3">
            <label for="t3">Extended Warranties</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t4">
            <label for="t4">5 Star Reviews</label>
          </div>
        </div>
      </div>
    </div><!-- radio-select-type-dv -->

    <div class="row">

      <div class="col-lg-12 m-auto">
        <?php 
          if(isset($_GET['status']) && $_GET['status']=='edit'){
            // echo '<h6 class="alert alert-success text-center deletesuccess">Record added</h6>';
            echo "<script>
                $('document').ready(function(){ 
                $('.toastrDefaultSuccess').click(); 
                }); 
            </script>";
            }
            if(isset($_GET['status']) && $_GET['status']=='del'){
              // echo '<h6 class="alert alert-success text-center deletesuccess">Record added</h6>';
              echo "<script>
                  $('document').ready(function(){ 
                  $('.toastrDefaultSuccess').click(); 
                  }); 
              </script>";
              }
        ?> 
        <div class="card custom-table-card table-dv table-internet d-block">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title">Internet UPS</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <?php 
            if ($_SESSION["role"] == "Admin") {
              $Internet_UPS_sql = "SELECT * FROM `lead_capture` WHERE `medium` = 'Internet UPS' AND `admin_id` = '$userID'";
            }
            else {
              $Internet_UPS_sql = "SELECT * FROM `lead_capture` WHERE `medium` = 'Internet UPS' AND `agent_id` = '$userID'";
            }
            
            $Internet_UPS_result = $con->query($Internet_UPS_sql);
            ?>
            <table class="jsgrid-table text-center alltable table-striped" id="xls1">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell"># LEAD</th>
                  <th class="jsgrid-cell">NAME</th>
                  <th class="jsgrid-cell">AGENT</th>
                  <th class="jsgrid-cell">APPT SET</th>
                  <th class="jsgrid-cell">APPT SHOW</th>
                  <th class="jsgrid-cell">APPT SOLD</th>
                  <th class="jsgrid-cell">SOURCE</th>
                  <th class="jsgrid-cell">STATUS</th>
                  <th class="jsgrid-cell">DATE</th>
                  <th class="jsgrid-cell excludeThisClass">OPERATIONS</th>
                  
                </tr>
              </thead>

              <tbody>
                <?php
                // if ($_SESSION["role"] == "Admin") {
                  // $Internet_UPS_sql = "SELECT * FROM `lead_capture` WHERE `medium` = 'Internet UPS' AND `admin_id` = '$userID'";
                  if(isset($_POST["apply_filter"])){
                      // $agent_id =$_POST['agent_id'];
                      $from_date = $_POST['st_date'];
                      $to_date = $_POST['end_date']; 
                       
                      $sqli1 = "SELECT * FROM lead_capture WHERE ((date(created_at)) BETWEEN '$from_date' AND    '$to_date') AND`medium` = 'Internet UPS' AND `admin_id` = '$userID' ";
                      // print_r($sqli1);

                    $Filer_result = $con->query($sqli1);
                    // $agent_data1 =$fetch_data1->fetch_assoc();
                    if ($Filer_result->num_rows > 0) {
                      $i=0;
                      while($Filer_Internet_UPS_row = $Filer_result->fetch_assoc()) { $i++; 
                     ?>
                        <tr class="jsgrid-ro">
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><?php echo $i; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><a href="view-notes.php?id=<?= $Filer_Internet_UPS_row['id']; ?>" class="text-dark"><?php echo $Filer_Internet_UPS_row['name']; ?></a></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><?php echo $Filer_Internet_UPS_row['agent_name']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><?php echo $Filer_Internet_UPS_row['appt_set']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><?php echo $Filer_Internet_UPS_row['appt_show']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><?php echo $Filer_Internet_UPS_row['appt_sold']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><?php echo $Filer_Internet_UPS_row['source']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><?php echo $Filer_Internet_UPS_row['status']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Internet_UPS_row['notes']); ?>"><?= date('d-m-Y h:i:sa',strtotime($Filer_Internet_UPS_row['created_at'])) ;?></td>
                          <td class=""> 
                            <button type="button" class="btn btn-primary" onclick="edit(<?=$Filer_Internet_UPS_row['id'] ?>,'<?=$Filer_Internet_UPS_row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                            <button type="button" class="btn btn-danger" onclick="del(<?php echo $Filer_Internet_UPS_row['id']; ?>)"> <i class="fa fa-trash"></i> </button> </td>
                          </tr>
                          <?php 
                        }
                      }


                  }
               else{
                  // echo "by defaullts";
               
                if ($Internet_UPS_result->num_rows > 0) {
                  $i=0;
                  while($Internet_UPS_row = $Internet_UPS_result->fetch_assoc()) { $i++; 
                 ?>
                    <tr class="jsgrid-ro">
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><?php echo $i; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><a href="view-notes.php?id=<?= $Internet_UPS_row['id']; ?>" class="text-dark"><?php echo $Internet_UPS_row['name']; ?></a></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><?php echo $Internet_UPS_row['agent_name']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><?php echo $Internet_UPS_row['appt_set']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><?php echo $Internet_UPS_row['appt_show']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><?php echo $Internet_UPS_row['appt_sold']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><?php echo $Internet_UPS_row['source']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><?php echo $Internet_UPS_row['status']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Internet_UPS_row['notes']); ?>"><?= date('d-m-Y h:i:sa',strtotime($Internet_UPS_row['created_at'])) ;?></td>
                      <td class=""> 
                        <button type="button" class="btn btn-primary" onclick="edit(<?=$Internet_UPS_row['id'] ?>,'<?=$Internet_UPS_row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                        <button type="button" class="btn btn-danger" onclick="del(<?php echo $Internet_UPS_row['id']; ?>)"> <i class="fa fa-trash"></i> </button> </td>
                      </tr>
                      <?php 
                    }
                  }
                }
                  ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.card-body -->
            <div class="text-center pb-3">
              <button type="button" class="btn btn-outline-primary" id="exp1"><i class="fas fa-save"></i> Save </button>
              <button type="button" class="btn btn-outline-primary printBdc">
                <i class="fa fa-print" aria-hidden="true"></i> Print 
              </button>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.card -->


          <div class="card custom-table-card table-dv table-phone">
            <div class="card-header bg-blue text-white">
              <h3 class="card-title">Phone UPS</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php 
              if ($_SESSION["role"] == "Admin") {
               $Phone_UPS_sql = "SELECT * FROM `lead_capture` WHERE `medium` = 'Phone UPS' AND `admin_id` = '$userID'";
             } else {
               $Phone_UPS_sql = "SELECT * FROM `lead_capture` WHERE `medium` = 'Phone UPS' AND `agent_id` = '$userID'";
             }
             
             
             $Phone_UPS_result = $con->query($Phone_UPS_sql);
             ?>
             <table class="jsgrid-table text-center  alltable table-striped" id="xls2">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell">#  LEAD</th>
                  <th class="jsgrid-cell">NAME</th>
                  <th class="jsgrid-cell">AGENT</th>
                  <th class="jsgrid-cell">APPT SET</th>
                  <th class="jsgrid-cell">APPT SHOW</th>
                  <th class="jsgrid-cell">APPT SOLD</th>
                  <th class="jsgrid-cell">STATUS</th>
                  <th class="jsgrid-cell">DATE</th>
                  <th class="jsgrid-cell excludeThisClass">OPERATIONS</th>
                </tr>
              </thead>

              <tbody>
                <?php
                if(isset($_POST["apply_filter"])){
                  // $agent_id =$_POST['agent_id'];
                  $from_date = $_POST['st_date'];
                  $to_date = $_POST['end_date']; 
                   
                  $Filer_UPS = "SELECT * FROM lead_capture WHERE ((date(created_at)) BETWEEN '$from_date' AND    '$to_date') AND `medium` = 'Phone UPS' AND `admin_id` = '$userID' ";
                  // print_r($Filer_UPS);

                $Filer_UPS_result = $con->query($Filer_UPS);
                // $agent_data1 =$fetch_data1->fetch_assoc();
                if ($Filer_UPS_result->num_rows > 0) {
                  $i=0;
                  while($Filer_UPS_row = $Filer_UPS_result->fetch_assoc()) { $i++; ?>
                    <tr class="">
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_UPS_row['notes']); ?>"><?php echo $i; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_UPS_row['notes']); ?>"><a href="view-notes.php?id=<?= $Filer_UPS_row['id']; ?>" class="text-dark"><?php echo $Filer_UPS_row['name']; ?></a></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_UPS_row['notes']); ?>"><?php echo $Filer_UPS_row['agent_name']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_UPS_row['notes']); ?>"><?php echo $Filer_UPS_row['appt_set']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_UPS_row['notes']); ?>"><?php echo $Filer_UPS_row['appt_show']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_UPS_row['notes']); ?>"><?php echo $Filer_UPS_row['appt_sold']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_UPS_row['notes']); ?>"><?php echo $Filer_UPS_row['status']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_UPS_row['notes']); ?>"><?= date('d-m-Y h:i:sa',strtotime($Filer_UPS_row['created_at'])) ;?></td>
                      <td class=""> 
                        <button type="button" class="btn btn-primary" onclick="edit(<?=$Filer_UPS_row['id'] ?>,'<?=$Filer_UPS_row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                        <button type="button" class="btn btn-danger" onclick="del(<?php echo $Filer_UPS_row['id']; ?>)"> <i class="fa fa-trash"></i> </button> </td>
                      </tr>
                      <?php 
                    }
                  }


              }
           else{
                if ($Phone_UPS_result->num_rows > 0) {
                  $i=0;
                  while($Phone_UPS_row = $Phone_UPS_result->fetch_assoc()) { $i++; ?>
                    <tr class="">
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Phone_UPS_row['notes']); ?>"><?php echo $i; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Phone_UPS_row['notes']); ?>"><a href="view-notes.php?id=<?= $Phone_UPS_row['id']; ?>" class="text-dark"><?php echo $Phone_UPS_row['name']; ?></a></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Phone_UPS_row['notes']); ?>"><?php echo $Phone_UPS_row['agent_name']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Phone_UPS_row['notes']); ?>"><?php echo $Phone_UPS_row['appt_set']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Phone_UPS_row['notes']); ?>"><?php echo $Phone_UPS_row['appt_show']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Phone_UPS_row['notes']); ?>"><?php echo $Phone_UPS_row['appt_sold']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Phone_UPS_row['notes']); ?>"><?php echo $Phone_UPS_row['status']; ?></td>
                      <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Phone_UPS_row['notes']); ?>"><?= date('d-m-Y h:i:sa',strtotime($Phone_UPS_row['created_at'])) ;?></td>
                      <td class=""> 
                        <button type="button" class="btn btn-primary" onclick="edit(<?=$Phone_UPS_row['id'] ?>,'<?=$Phone_UPS_row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                        <button type="button" class="btn btn-danger" onclick="del(<?php echo $Phone_UPS_row['id']; ?>)"> <i class="fa fa-trash"></i> </button> </td>
                      </tr>
                      <?php 
                    }
                  }
                }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="text-center pb-3">
              <button type="button" class="btn btn-outline-primary" id="exp2"><i class="fas fa-save"></i> Save </button>
              <button type="button" class="btn btn-outline-primary printBdc">
                <i class="fa fa-print" aria-hidden="true"></i> Print 
              </button>
            </div>
          </div>
          <!-- /.card -->
          

          <div class="card custom-table-card table-dv table-extended">
            <div class="card-header bg-blue text-white">
              <h3 class="card-title">Extended Warranties</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php 
              if ($_SESSION["role"] == "Admin") {
                $Extended_Warranties_sql = "SELECT * FROM `lead_capture` WHERE `medium` = 'Extended Warranties' AND `admin_id` = '$userID'";
              } else {
                $Extended_Warranties_sql = "SELECT * FROM `lead_capture` WHERE `medium` = 'Extended Warranties' AND `agent_id` = '$userID'";
              }
              
              
              $Extended_Warranties_result = $con->query($Extended_Warranties_sql);
              ?>
              <table class="jsgrid-table text-center  alltable table-striped" id="xls3">
                <thead class="jsgrid-grid-header">
                  <tr class="jsgrid-alt-row">
                    <th class="jsgrid-cell">#  LEAD</th>
                    <th class="jsgrid-cell">NAME</th>
                    <th class="jsgrid-cell">AGENT</th>
                    <th class="jsgrid-cell">APPT SET</th>
                    <th class="jsgrid-cell">APPT SHOW</th>
                    <th class="jsgrid-cell">APPT SOLD</th>
                    <th class="jsgrid-cell">STATUS</th>
                    <th class="jsgrid-cell">DATE</th>
                    <th class="jsgrid-cell excludeThisClass">OPERATIONS</th>
                  </tr>
                </thead>

                <tbody>
                  <?php
                   if(isset($_POST["apply_filter"])){
                    // $agent_id =$_POST['agent_id'];
                    $from_date = $_POST['st_date'];
                    $to_date = $_POST['end_date']; 
                     
                    $Warranties = "SELECT * FROM lead_capture WHERE ((date(created_at)) BETWEEN '$from_date' AND    '$to_date') AND  `medium` = 'Extended Warranties' AND `admin_id` = '$userID' ";
                    // print_r($sqli1);
  
                  $Filer_Warranties_result = $con->query($Warranties);
                  // $agent_data1 =$fetch_data1->fetch_assoc();
                  if ($Filer_Warranties_result->num_rows > 0) {
                    $i=0;
                    while($Filer_Warranties_row = $Filer_Warranties_result->fetch_assoc()) { $i++; ?>
                      <tr class="">
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Warranties_row['notes']); ?>"><?php echo $i; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Warranties_row['notes']); ?>"><a href="view-notes.php?id=<?= $Filer_Warranties_row['id']; ?>" class="text-dark"><?php echo $Filer_Warranties_row['name']; ?></a></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Warranties_row['notes']); ?>"><?php echo $Filer_Warranties_row['agent_name']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Warranties_row['notes']); ?>"><?php echo $Filer_Warranties_row['appt_set']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Warranties_row['notes']); ?>"><?php echo $Filer_Warranties_row['appt_show']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Warranties_row['notes']); ?>"><?php echo $Filer_Warranties_row['appt_sold']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Warranties_row['notes']); ?>"><?php echo $Filer_Warranties_row['status']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Filer_Warranties_row['notes']); ?>"><?= date('d-m-Y h:i:sa',strtotime($Filer_Warranties_row['created_at'])) ;?></td>
                        <td class=""> 


                          <button type="button" class="btn btn-primary" onclick="edit(<?=$Filer_Warranties_row['id'] ?>,'<?=$Filer_Warranties_row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                          <button type="button" class="btn btn-danger" onclick="del(<?php echo $Filer_Warranties_row['id']; ?>)"> <i class="fa fa-trash"></i> </button> </td>
                        </tr>
                        <?php 
                      }
                    }
  
  
                }
             else{
                  if ($Extended_Warranties_result->num_rows > 0) {
                    $i=0;
                    while($Extended_Warranties_row = $Extended_Warranties_result->fetch_assoc()) { $i++; ?>
                      <tr class="">
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Extended_Warranties_row['notes']); ?>"><?php echo $i; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Extended_Warranties_row['notes']); ?>"><a href="view-notes.php?id=<?= $Extended_Warranties_row['id']; ?>" class="text-dark"><?php echo $Extended_Warranties_row['name']; ?></a></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Extended_Warranties_row['notes']); ?>"><?php echo $Extended_Warranties_row['agent_name']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Extended_Warranties_row['notes']); ?>"><?php echo $Extended_Warranties_row['appt_set']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Extended_Warranties_row['notes']); ?>"><?php echo $Extended_Warranties_row['appt_show']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Extended_Warranties_row['notes']); ?>"><?php echo $Extended_Warranties_row['appt_sold']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Extended_Warranties_row['notes']); ?>"><?php echo $Extended_Warranties_row['status']; ?></td>
                        <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Extended_Warranties_row['notes']); ?>"><?= date('d-m-Y h:i:sa',strtotime($Extended_Warranties_row['created_at'])) ;?></td>
                        <td class=""> 


                          <button type="button" class="btn btn-primary" onclick="edit(<?=$Extended_Warranties_row['id'] ?>,'<?=$Extended_Warranties_row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                          <button type="button" class="btn btn-danger" onclick="del(<?php echo $Extended_Warranties_row['id']; ?>)"> <i class="fa fa-trash"></i> </button> </td>
                        </tr>
                        <?php 
                      }
                    }
                  }
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="text-center pb-3">
                <button type="button" class="btn btn-outline-primary" id="exp3"><i class="fas fa-save"></i> Save </button>
                <button type="button" class="btn btn-outline-primary printBdc">
                  <i class="fa fa-print" aria-hidden="true"></i> Print 
                </button>
              </div>
            </div>
            <!-- /.card -->
            

            <div class="card custom-table-card table-dv table-reviews">
              <div class="card-header bg-blue text-white">
                <h3 class="card-title">5 Star Reviews</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php 
                if ($_SESSION["role"] == "Admin") {
                  $Reviews_sql = "SELECT * FROM `lead_capture` WHERE `medium` = '5 Star Reviews' AND `admin_id` = '$userID'";
                } else {
                  $Reviews_sql = "SELECT * FROM `lead_capture` WHERE `medium` = '5 Star Reviews'  AND `agent_id` = '$userID'";
                }
                
                
                $Reviews_result = $con->query($Reviews_sql);
                ?>
                <table class="jsgrid-table text-center  alltable table-striped" id="xls4">
                  <thead class="jsgrid-grid-header">
                    <tr class="jsgrid-alt-row">
                      <th class="jsgrid-cell"># LEAD</th>
                      <th class="jsgrid-cell">NAME</th>
                      <th class="jsgrid-cell">AGENT</th>
                      <th class="jsgrid-cell">FILLED OUT</th>
                      <th class="jsgrid-cell">STATUS</th>
                      <th class="jsgrid-cell">DATE</th>
                      <th class="jsgrid-cell excludeThisClass">OPERATIONS</th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php
                    if(isset($_POST["apply_filter"])){
                      // $agent_id =$_POST['agent_id'];
                      $from_date = $_POST['st_date'];
                      $to_date = $_POST['end_date']; 
                       
                      $Reviews = "SELECT * FROM lead_capture WHERE ((date(created_at)) BETWEEN '$from_date' AND    '$to_date') AND `medium` = '5 Star Reviews' AND `admin_id` = '$userID' ";
                      // print_r($Reviews);
    
                    $Filer_Reviews_result = $con->query($Reviews);
                    // $agent_data1 =$fetch_data1->fetch_assoc();
                    if ($Filer_Reviews_result->num_rows > 0) {
                      $i = 0;
                      while($filter_Reviews_row = $Filer_Reviews_result->fetch_assoc()) { $i++; ?>
                        <tr class="">
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($filter_Reviews_row['notes']); ?>"><?php echo $i ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($filter_Reviews_row['notes']); ?>"><a href="view-notes.php?id=<?= $filter_Reviews_row['id']; ?>" class="text-dark"><?php echo $filter_Reviews_row['name']; ?></a></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($filter_Reviews_row['notes']); ?>"><?php echo $filter_Reviews_row['agent_name']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($filter_Reviews_row['notes']); ?>"><?php echo $filter_Reviews_row['filled_out']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($filter_Reviews_row['notes']); ?>"><?php echo $filter_Reviews_row['status']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($filter_Reviews_row['notes']); ?>"><?= date('d-m-Y h:i:sa',strtotime($filter_Reviews_row['created_at'])) ;?></td>
                          <td class=""> 
                            <button type="button" class="btn btn-primary" onclick="edit(<?=$filter_Reviews_row['id'] ?>,'<?=$filter_Reviews_row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                            <button type="button" class="btn btn-danger" onclick="del(<?php echo $filter_Reviews_row['id']; ?>)"> <i class="fa fa-trash"></i> </button> </td>
                          </tr>
                          <?php 
                        }
                      }
    
    
                  }
               else{
                    if ($Reviews_result->num_rows > 0) {
                      $i = 0;
                      while($Reviews_row = $Reviews_result->fetch_assoc()) { $i++; ?>
                        <tr class="">
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Reviews_row['notes']); ?>"><?php echo $i ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Reviews_row['notes']); ?>"><a href="view-notes.php?id=<?= $Reviews_row['id']; ?>" class="text-dark"><?php echo $Reviews_row['name']; ?></a></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Reviews_row['notes']); ?>"><?php echo $Reviews_row['agent_name']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Reviews_row['notes']); ?>"><?php echo $Reviews_row['filled_out']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Reviews_row['notes']); ?>"><?php echo $Reviews_row['status']; ?></td>
                          <td class="" data-toggle="popover" title="Notes:"  data-content="<?php echo strip_tags($Reviews_row['notes']); ?>"><?= date('d-m-Y h:i:sa',strtotime($Reviews_row['created_at'])) ;?></td>
                          <td class=""> 
                            <button type="button" class="btn btn-primary" onclick="edit(<?=$Reviews_row['id'] ?>,'<?=$Reviews_row['medium']?>')"> <i class="fa fa-edit"></i> </button>
                            <button type="button" class="btn btn-danger" onclick="del(<?php echo $Reviews_row['id']; ?>)"> <i class="fa fa-trash"></i> </button> </td>
                          </tr>
                          <?php 
                        }
                      }
                    }
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
                <div class="text-center pb-3">
                  <button type="button" class="btn btn-outline-primary" id="exp4"><i class="fas fa-save"></i> Save </button>
                  <button type="button" class="btn btn-outline-primary printBdc">
                    <i class="fa fa-print" aria-hidden="true"></i> Print 
                  </button>
                </div>
              </div>
              <!-- /.card -->
              

            </div><!-- col -->
          </div><!-- col -->
        </section>
        <?php 
        // } else{
          ?>
          <!-- <section class="content-header">
              <h1 class="text-danger">Result not found.</h1>
          </section> -->
       <?php  
      //  }  } else{
         ?>
           <!-- <section class="content-header">
              <h1 class="text-danger">No Results.</h1>
          </section> -->
       <?php 
      // } }
      ?>
        <!-- /.content -->
      </div>
      <button type="hidden" class="toastrDefaultSuccess"></button>
      <script>
  $(function () {
    $(".alltable").DataTable();
  });
</script>
      <script>
        $(document).ready(function() {
        $("#st_date").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
          $("#exp1").click(function() {
            $("#xls1").table2excel({
              exclude: ".excludeThisClass",
              name: "Internet UPS",
              filename: "Internet UPS",
              preserveColors: false
            });
          });
          $("#exp2").click(function() {
            $("#xls2").table2excel({
              exclude: ".excludeThisClass",
              name: "Phone UPS",
              filename: "Phone UPS",
              preserveColors: false
            });
          });
          $("#exp3").click(function() {
            $("#xls3").table2excel({
              exclude: ".excludeThisClass",
              name: "Extended Warranties",
              filename: "Extended Warranties",
              preserveColors: false
            });
          });
          $("#exp4").click(function() {
            $("#xls4").table2excel({
              exclude: ".excludeThisClass",
              name: "5 Star Reviews",
              filename: "5 Star Reviews",
              preserveColors: false
            });
          });
        });
      </script>
      <script>
        function del(_id) {
          $.ajax({
            method: "POST",
            url: "../forms/delete-agent-data.php",
            data: {id : _id}
          }).done(function( _res ) {
            var res = JSON.parse(_res);
            if (res.status == "200") {
             window.location.assign("agent-table.php?status=del");
           } else {
            window.location.assign("agent-table.php?status=err");
          }
        });
          
        }

        setTimeout(function(){ $("#alert").hide(); }, 3000);
      </script>

      <script>
        $('.printBdc').on('click', function() {  
          window.print();  
          return false;
        });

  </script>
  
<script>
	
  $("tr").not(':first').hover(
  function () {
    $(this).css("background","#b8d1f3");
    $(this).css("cursor","pointer");
  }, 
  function () {
    $(this).css("background","");
  }
);


$('.toastrDefaultSuccess').click(function() {
    let searchParams = new URLSearchParams(window.location.search);
    let param = searchParams.get('status')
    if (param == 'edit') {
        toastr.success('Record updated successfully!')
    } 
    else if (param == 'del') {
        toastr.success('Record deleted successfully!')
    } 
    else {
        toastr.success('Record added successfully!')
    }
});
</script>

  <!-- /.content-wrapper -->
  <?php include('../../footer.php'); ?>