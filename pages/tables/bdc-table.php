<?php
session_start();
if ($_SESSION["role"] !== "Admin") {
  header("location: /index.php");
}
$page = 'BDC-table';
include('../../header.php');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>BDC Performance</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">BDC Performance</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="bg-white mb-4 radio-select-type-dv">
      <div class="card-body">
        <div class="form-group clearfix text-center m-0">
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t1" checked="checked">
            <label for="t1">BDC INTERNET UPS</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t2">
            <label for="t2">BDC PHONE UPS</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t3">
            <label for="t3">LEAD PROVIDER</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t4">
            <label for="t4">MONTHS OF INTERNET UPS</label>
          </div>
          <div class="icheck-primary d-inline">
            <input type="radio" name="agent-table-radio" id="t5">
            <label for="t5">MONTHS OF PHONE UPS</label>
          </div>
        </div>
      </div>
    </div><!-- radio-select-type-dv -->

    <div class="row">

      <div class="col-lg-12">
        <div class="card custom-table-card table-dv table-internet d-block">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title py-2">BDC Internet UPS</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table text-center" id="xls1">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell width-set-1">AGENT</th>
                  <th class="jsgrid-cell width-set-1">LEADS</th>
                  <th class="jsgrid-cell width-set-1">LEADS RETURNED</th>
                  <th class="jsgrid-cell width-set-1">TOTAL LEADS</th>
                  <th class="jsgrid-cell width-set-1">APPT SET</th>
                  <th class="jsgrid-cell width-set-1">APPT SHOW</th>
                  <th class="jsgrid-cell width-set-1">SOLD</th>
                  <th class="jsgrid-cell width-set-1">APPT SET</th>
                  <th class="jsgrid-cell width-set-1">APPT SHOW</th>
                  <th class="jsgrid-cell width-set-1">SOLD/SET</th>
                  <th class="jsgrid-cell width-set-1">SOLD/SHOW</th>
                  <th class="jsgrid-cell width-set-1">OVERALL SOLD</th>
                  <!-- <th class="jsgrid-cell">OPERATION</th> -->
                </tr>
              </thead>

              <tbody>
                <?php
                $year = (!empty($_POST['years']))?$_POST['years']:"2019";
                $a = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' GROUP By `agent_id`";
                $r = $con->query($a);
                while ($s = $r->fetch_assoc()) {
                  $agent = $s['agent_id'];
                  $Lsql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND `agent_id` = '$agent'";
                  $Lresult = $con->query($Lsql);
                  $sql1 = "SELECT count(appt_set) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND `appt_set` != '' AND `agent_id` = '$agent'";
                  $sql2 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND `appt_show` != '' AND `agent_id` = '$agent'";
                  $sql3 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND `appt_sold` != '' AND `agent_id` = '$agent'";
                  $sql5 = "SELECT count(status) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND `status` = 'No Number' AND `agent_id` = '$agent'";

                  $result1 = $con->query($sql1);
                  $result2 = $con->query($sql2);
                  $result3 = $con->query($sql3);
                  $result5 = $con->query($sql5);

                  $IUPSrow1 = $result1->fetch_assoc();
                  $IUPSrow2 = $result2->fetch_assoc();
                  $IUPSrow3 = $result3->fetch_assoc();
                  $IUPSrow5 = $result5->fetch_assoc();

                  $agent_name =$s['agent_name'];
                  $IPUStotalL = $Lresult->num_rows;
                  $IPUSreturned = $IUPSrow5["total"];
                  $total = $IPUStotalL-$IPUSreturned;
                  $IPUSset = $IUPSrow1["total"];
                  $IPUSshow = $IUPSrow2["total"];
                  $IPUSsold = $IUPSrow3["total"];
                  $appset = round(($IPUSset/$total)*100);
                  $appshow = round(($IPUSshow/$total)*100);
                  $SoldSet = round(($IPUSsold/$IPUSset)*100);
                  $SoldShow = round(($IPUSsold/$IPUSshow)*100);
                  $Ioverall = round(($IPUSsold/$total)*100);
                  ?>
                  <tr class="jsgrid-row">
                    <td class="jsgrid-cell"><?=(!empty($agent_name))?$agent_name:"Agent Name";?></td>
                    <td class="jsgrid-cell"><?=(!empty($IPUStotalL))?$IPUStotalL:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($IPUSreturned))?$IPUSreturned:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($total))?$total:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($IPUSset))?$IPUSset:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($IPUSshow))?$IPUSshow:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($IPUSsold))?$IPUSsold:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($appset))?$appset:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($appshow))?$appshow:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($SoldSet))?$SoldSet:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($SoldShow))?$SoldShow:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($Ioverall))?$Ioverall:"0";?>%</td> 
                      <!-- <td class="text-center">                     
                      <button type="button" class="btn btn-primary" onclick="edit()"> <i class="fa fa-edit"></i> </button>
                      <button type="button" class="btn btn-danger" onclick="del()"> <i class="fa fa-trash"></i> </button>
                    </td> -->
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="text-center pb-3">
            <button type="button" class="btn btn-outline-primary" id="exp1"><i class="fas fa-save"></i> Save </button>
            <button type="button" class="btn btn-outline-primary printBdc">
              <i class="fa fa-print" aria-hidden="true"></i> Print 
            </button>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.card -->
        <div class="card custom-table-card table-dv table-phone">
          <div class="card-header bg-blue text-white">
            <h3 class="card-title py-2">BDC Phone UPS</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="jsgrid-table text-center" id="xls2">
              <thead class="jsgrid-grid-header">
                <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell width-set-1">AGENT</th>
                  <th class="jsgrid-cell width-set-1">PHONE-UPS</th>
                  <th class="jsgrid-cell width-set-1">APPT SET</th>
                  <th class="jsgrid-cell width-set-1">APPT SHOW</th>
                  <th class="jsgrid-cell width-set-1">SOLD</th>
                  <th class="jsgrid-cell width-set-1">APPT SET</th>
                  <th class="jsgrid-cell width-set-1">APPT SHOW</th>
                  <th class="jsgrid-cell width-set-1">SOLD/SET</th>
                  <th class="jsgrid-cell width-set-1">SOLD/SHOW</th>
                  <th class="jsgrid-cell width-set-1">OVERALL/SOLD</th>
                  <!-- <th class="jsgrid-cell">OPERATION</th> -->
                </tr>
              </thead>

              <tbody>
                <?php
                $ap = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' GROUP By `agent_id`";
                $rp = $con->query($ap);
                while ($P = $rp->fetch_assoc()) {
                  $Pagent_id = $P['agent_id'];
                  $PLsql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' AND `agent_id` = '$Pagent_id'";
                  $PLresult = $con->query($PLsql);
                  $Psql1 = "SELECT count(appt_set) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' AND `appt_set` != '' AND `agent_id` = '$Pagent_id'";
                  $Psql2 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' AND `appt_show` != '' AND `agent_id` = '$Pagent_id'";
                  $Psql3 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' AND `appt_sold` != '' AND `agent_id` = '$Pagent_id'";
                  $Presult1 = $con->query($Psql1);
                  $Presult2 = $con->query($Psql2);
                  $Presult3 = $con->query($Psql3);

                  $PUPSrow1 = $Presult1->fetch_assoc();
                  $PUPSrow2 = $Presult2->fetch_assoc();
                  $PUPSrow3 = $Presult3->fetch_assoc();

                  $Pagent_name = $P['agent_name'];
                  $PUStotalL = $PLresult->num_rows;
                  $PUSset = $PUPSrow1["total"];
                  $PUSshow = $PUPSrow2["total"];
                  $PUSsold = $PUPSrow3["total"];
                  $Pappset = round(($PUSset/$PUStotalL)*100);
                  $Pappshow = round(($PUSshow/$PUStotalL)*100);
                  $PSoldSet = round(($PUSsold/$PUSset)*100);
                  $PSoldShow = round(($PUSsold/$PUSshow)*100);
                  $Poverall = round(($PUSsold/$PUStotalL)*100);
                  ?>
                  <tr class="jsgrid-row">
                    <td class="jsgrid-cell"><?=(!empty($Pagent_name))?$Pagent_name:"Agent Name";?></td>
                    <td class="jsgrid-cell"><?=(!empty($PUStotalL))?$PUStotalL:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($PUSset))?$PUSset:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($PUSshow))?$PUSshow:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($PUSsold))?$PUSsold:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($Pappset))?$Pappset:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($Pappshow))?$Pappshow:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($PSoldSet))?$PSoldSet:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($PSoldShow))?$PSoldShow:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($Poverall))?$Poverall:"0";?>%</td>
                      <!-- <td class="text-center">
                      <button type="button" class="btn btn-primary" onclick="edit()" > <i class="fa fa-edit"></i> </button>
                      <button type="button" class="btn btn-danger" onclick="del()"> <i class="fa fa-trash"></i> </button> </td> -->
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="text-center pb-3">
              <button type="button" class="btn btn-outline-primary" id="exp2"><i class="fas fa-save"></i> Save </button>
              <button type="button" class="btn btn-outline-primary printBdc">
                <i class="fa fa-print" aria-hidden="true"></i> Print 
              </button>
            </div>
          </div>
          <!-- /.card -->


          <div class="card custom-table-card table-dv table-extended">
            <div class="card-header bg-blue text-white">
              <h3 class="card-title py-2">Lead Provider</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="jsgrid-table text-center" id="xls3">
                <thead class="jsgrid-grid-header">
                  <tr class="jsgrid-alt-row">
                    <th class="jsgrid-cell width-set-1">LEAD PROVIDER</th>
                    <th class="jsgrid-cell width-set-1">LEADS</th>
                    <th class="jsgrid-cell width-set-1">LEADS RETURNED</th>
                    <th class="jsgrid-cell width-set-1">NET LEADS</th>
                    <!-- <th class="jsgrid-cell">COST</th> -->
                    <th class="jsgrid-cell width-set-1">SOLD</th>
                    <!-- <th class="jsgrid-cell">COST PER LEADS</th> -->
                    <!-- <th class="jsgrid-cell">COST PER DELIVERY</th> -->
                    <!-- <th class="jsgrid-cell">CLOSING RATIO</th> -->
                    <th class="jsgrid-cell width-set-1">APPT SET</th>
                    <th class="jsgrid-cell width-set-1">SET %</th>
                    <th class="jsgrid-cell width-set-1">APPT SHOWN</th>
                    <th class="jsgrid-cell width-set-1">SHOWN %</th>
                    <!-- <th class="jsgrid-cell">operations</th> -->

                  </tr>
                </thead>

                <tbody>
                  <?php 
                  $leadsql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' GROUP By `source`";
                  $laedresult = $con->query($leadsql);
                  while ($leaddata = $laedresult->fetch_assoc()) {
                    $leadSource = $leaddata['source'];
                    $totalsql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `source` = '$leadSource' ";
                    $totalresult = $con->query($totalsql);

                    $leadsql1 = "SELECT count(appt_set) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `appt_set` != '' AND `source` = '$leadSource'";
                    $leadsql2 = "SELECT count(appt_show) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `appt_show` != '' AND `source` = '$leadSource'";
                    $leadsql3 = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `appt_sold` != '' AND `source` = '$leadSource'";
                    $leadsql4 = "SELECT count(status) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `status` = 'No Number' AND `source` = '$leadSource'";
                    $leadresult1 = $con->query($leadsql1);
                    $leadresult2 = $con->query($leadsql2);
                    $leadresult3 = $con->query($leadsql3);
                    $leadresult4 = $con->query($leadsql4);

                    $leadrow1 = $leadresult1->fetch_assoc();
                    $leadrow2 = $leadresult2->fetch_assoc();
                    $leadrow3 = $leadresult3->fetch_assoc();
                    $leadrow4 = $leadresult4->fetch_assoc();

                    $totalleads = $totalresult->num_rows;
                    $total = $IPUStotalL-$IPUSreturned;
                    $leadset = $leadrow1["total"];
                    $leadshow = $leadrow2["total"];
                    $leadsold = $leadrow3["total"];
                    $returnlead = $leadrow4["total"];
                    $neatlead = $totalleads-$returnlead;
                    $setper = round(($leadset/$neatlead)*100);
                    $showper = round(($leadshow/$leadset)*100);
                    ?>
                    <tr class="jsgrid-row">
                      <td class="jsgrid-cell"><?=(!empty($leadSource))?$leadSource:"Provider";?></td>
                      <td class="jsgrid-cell"><?=(!empty($totalleads))?$totalleads:"0";?></td>
                      <td class="jsgrid-cell"><?=(!empty($returnlead))?$returnlead:"0";?></td>
                      <td class="jsgrid-cell"><?=(!empty($neatlead))?$neatlead:"0";?></td>
                      <!-- <td class="jsgrid-cell">N/A</td> -->
                      <td class="jsgrid-cell"><?=(!empty($leadsold))?$leadsold:"0";?></td>
                      <!-- <td class="jsgrid-cell">N/A</td> -->
                      <!-- <td class="jsgrid-cell">N/A</td> -->
                      <!-- <td class="jsgrid-cell">N/A</td> -->
                      <td class="jsgrid-cell"><?=(!empty($leadset))?$leadset:"0";?></td>
                      <td class="jsgrid-cell"><?=(!empty($setper))?$setper:"0";?>%</td>
                      <td class="jsgrid-cell"><?=(!empty($leadshow))?$leadshow:"0";?></td>
                      <td class="jsgrid-cell"><?=(!empty($showper))?$showper:"0";?>%</td>
                      <!-- <td class="text-center"> 
                      <button type="button" class="btn btn-primary" > <i class="fa fa-edit"></i> </button>
                      <button type="button" class="btn btn-danger" > <i class="fa fa-trash"></i> </button> </td> -->
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="text-center pb-3">
              <button type="button" class="btn btn-outline-primary" id="exp3"><i class="fas fa-save"></i> Save </button>
              <button type="button" class="btn btn-outline-primary printBdc">
                <i class="fa fa-print" aria-hidden="true"></i> Print 
              </button>
            </div>
          </div>
          <!-- /.card -->


          <div class="card custom-table-card table-dv table-reviews">
            <div class="card-header bg-blue text-white">
              <div class="d-flex justify-content-between">
                <h3 class="card-title py-2">Months of Internet UPS</h3>
                <form id="a" action="" method="post">
                  <select onchange="this.form.submit()" name="years" id="years" class="form-control" style="width:200px;">
                    <option value="<?=$year;?>" selected><?=$year;?></option>
                    <?php for ($i=2000; $i < 2100; $i++) { ?>
                      <option value="<?=$i;?>"><?=$i;?></option>
                      <?php
                    }
                    ?>
                  </select>
                </form>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body" id="yearby">
              <table class="jsgrid-table text-center" id="xls4">
                <thead class="jsgrid-grid-header">
                 <tr class="jsgrid-alt-row">
                  <th class="jsgrid-cell width-set-1">MONTH</th>
                  <th class="jsgrid-cell width-set-1">GROSS LEADS</th>
                  <th class="jsgrid-cell width-set-1">LEADS RETD</th>
                  <th class="jsgrid-cell width-set-1">NET LEADS</th>
                  <th class="jsgrid-cell width-set-1">SALES</th>
                  <th class="jsgrid-cell width-set-1">TOTAL % SOLD/LEADS</th>
                  <th class="jsgrid-cell width-set-1">RETURNED LEADS</th>
                </tr>
              </thead>

              <tbody>
                <?php
                $imsql = "SELECT MONTH(created_at) AS Month FROM lead_capture WHERE `medium` = 'Internet UPS' AND `admin_id` = '$userID' GROUP BY Month ORDER BY Month ASC";
                $imresult = $con->query($imsql);
                while ($imdata = $imresult->fetch_assoc()) {
                  $imSource = $imdata['Month'];
                  $imtotalsql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND MONTH(created_at) = '$imSource' AND YEAR(created_at) = '$year' ";
                  $imretsql = "SELECT count(status) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND `status` = 'No Number' AND YEAR(created_at) = '$year' AND MONTH(created_at) = '$imSource' ";
                  $imsoldsql = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND `appt_sold` != '' AND YEAR(created_at) = '$year' AND MONTH(created_at) = '$imSource' ";
                  $imsalessql = "SELECT count(status) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Internet UPS' AND `status` = 'Purchased' AND YEAR(created_at) = '$year' AND MONTH(created_at) = '$imSource' ";
                  $imtotalresult = $con->query($imtotalsql);
                  $imretresult = $con->query($imretsql);
                  $imsalesresult = $con->query($imsalessql);
                  $imsoldresult = $con->query($imsoldsql);
                  $imrow1 = $imretresult->fetch_assoc();
                  $imsalesrow1 = $imsalesresult->fetch_assoc();
                  $imsoldrow1 = $imsoldresult->fetch_assoc();
                  $imtotal = $imtotalresult->num_rows;
                  $dateObj   = DateTime::createFromFormat('!m', $imSource);
                  $monthName = $dateObj->format('F'); // March
                  $imreturn = $imrow1["total"];
                  $imsold = $imsoldrow1["total"];
                  $imsales = $imsalesrow1["total"];
                  $imnet = $imtotal-$imreturn;
                  $imper = round(($imsales/$imnet)*100);
                  $imretper = round(($imreturn/$imtotal)*100);
                  ?>
                  <tr class="jsgrid-row">
                    <td class="jsgrid-cell"><?=(!empty($monthName))?$monthName:"Month";?></td>
                    <td class="jsgrid-cell"><?=(!empty($imtotal))?$imtotal:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($imreturn))?$imreturn:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($imnet))?$imnet:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($imsales))?$imsales:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($imper))?$imper:"0";?>%</td>
                    <td class="jsgrid-cell"><?=(!empty($imretper))?$imretper:"0";?>%</td>
                    <!-- <td class="jsgrid-cell">N/A</td> -->
                    <!-- <td class="jsgrid-cell">N/A</td> -->
                    <!-- <td class="jsgrid-cell">N/A</td> -->
                    <!-- <td class="jsgrid-cell">N/A</td> -->
                    <!-- <td class="jsgrid-cell">N/A</td> -->

                      <!-- <td>
                      <button type="button" class="btn btn-primary" > <i class="fa fa-edit"></i> </button>
                      <button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button> </td> -->
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="text-center pb-3">
              <button type="button" class="btn btn-outline-primary" id="exp4"><i class="fas fa-save"></i> Save </button>
              <button type="button" class="btn btn-outline-primary printBdc">
                <i class="fa fa-print" aria-hidden="true"></i> Print 
              </button>
            </div>
          </div>
          <!-- /.card -->
          <div class="card custom-table-card table-dv table-reviews-ex">
            <div class="card-header bg-blue text-white">
              <div class="d-flex justify-content-between">
                <h3 class="card-title py-2">Months of Phone UPS</h3>
                <form id="a" action="" method="post">
                  <select onchange="this.form.submit()" name="years" id="years" class="form-control" style="width:200px;">
                    <option value="<?=$year;?>" selected><?=$year;?></option>
                    <?php for ($i=2000; $i < 2100; $i++) { ?>
                      <option value="<?=$i;?>"><?=$i;?></option>
                      <?php
                    }
                    ?>
                  </select>
                </form>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="jsgrid-table text-center" id="xls5">
                <thead class="jsgrid-grid-header">
                  <tr class="jsgrid-alt-row">
                    <th class="jsgrid-cell width-set-1">MONTHS</th>
                    <th class="jsgrid-cell width-set-1">CALLS</th>
                    <th class="jsgrid-cell width-set-1">SALES</th>
                    <th class="jsgrid-cell width-set-1">TOTAL % SOLD / LEADS</th>
                    <!-- <th class="jsgrid-cell width-set-1">OPERATION</th> -->
                  </tr>
                </thead>

                <tbody>
                  <?php 
                  $ph_sql = "SELECT MONTH(created_at) AS Month FROM lead_capture WHERE `medium` = 'Phone UPS' AND `admin_id` = '$userID' GROUP BY Month ORDER BY Month ASC";
                  $ph_result = $con->query($ph_sql);
                  while ($ph_data = $ph_result->fetch_assoc()) {
                    $ph_Source = $ph_data['Month'];
                    $ph_totalsql = "SELECT * FROM `lead_capture` WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' AND MONTH(created_at) = '$ph_Source' AND YEAR(created_at) = '$year' ";
                    $ph_retsql = "SELECT count(status) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' AND `status` = 'No Number' AND YEAR(created_at) = '$year' AND MONTH(created_at) = '$ph_Source' ";
                    $ph_soldsql = "SELECT count(appt_sold) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' AND `appt_sold` != '' AND YEAR(created_at) = '$year' AND MONTH(created_at) = '$ph_Source' ";
                    $ph_salessql = "SELECT count(status) AS total FROM lead_capture WHERE `admin_id` = '$userID' AND `medium` = 'Phone UPS' AND `status` = 'Purchased' AND YEAR(created_at) = '$year' AND MONTH(created_at) = '$ph_Source' ";
                    $ph_totalresult = $con->query($ph_totalsql);
                    $ph_retresult = $con->query($ph_retsql);
                    $ph_salesresult = $con->query($ph_salessql);
                    $ph_soldresult = $con->query($ph_soldsql);
                    $ph_row1 = $ph_retresult->fetch_assoc();
                    $ph_salesrow1 = $ph_salesresult->fetch_assoc();
                    $ph_soldrow1 = $ph_soldresult->fetch_assoc();
                    $ph_total = $ph_totalresult->num_rows;
                    $dateObj1   = DateTime::createFromFormat('!m', $ph_Source);
                  $ph_monthName = $dateObj1->format('F'); // March
                  $ph_return = $ph_row1["total"];
                  $ph_sold = $ph_soldrow1["total"];
                  $ph_sales = $ph_salesrow1["total"];
                  $ph_net = $ph_total-$ph_return;
                  $ph_per = round(($ph_sales/$ph_net)*100);
                  ?>
                  <tr class="jsgrid-row">
                    <td class="jsgrid-cell"><?=(!empty($ph_monthName))?$ph_monthName:"Month";?></td>
                    <td class="jsgrid-cell"><?=(!empty($ph_total))?$ph_total:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($ph_sales))?$ph_sales:"0";?></td>
                    <td class="jsgrid-cell"><?=(!empty($ph_per))?$ph_per:"0";?>%</td>
                      <!-- <td>
                      <button type="button" class="btn btn-primary" > <i class="fa fa-edit"></i> </button>
                      <button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button>
                    </td> -->
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="text-center pb-3">
            <button type="button" class="btn btn-outline-primary" id="exp5"><i class="fas fa-save"></i> Save </button>
            <button type="button" class="btn btn-outline-primary printBdc">
              <i class="fa fa-print" aria-hidden="true"></i> Print 
            </button>
          </div>
        </div>
        <!-- /.card -->
        

      </div><!-- col -->
    </div><!-- col -->
  </section>
  <!-- /.content -->
</div>

<script>
  function del(_id) {
    $.ajax({
      method: "POST",
      url: "../forms/delete-agent-data.php",
      data: {id : _id}
    }).done(function( _res ) {
      var res = JSON.parse(_res);
      if (res.status == "200") {
       window.location.assign("agent-table.php?success=del");
     } else {
      window.location.assign("agent-table.php?success=err");
    }
  });

  }
  function edit(_id,_medium) {
    $.ajax({
      method: "POST",
      url: "../forms/set-session-for-agent-edit.php",
      data: {id : _id}
    }).done(function( _res ) {
      var res = JSON.parse(_res);
      if (res.status == "200") {

       window.location.assign("/pages/forms/agent-edit.php?medium="+_medium);
     } else {
      window.location.assign("agent-table.php?success=err");
    }
  });

  }
  setTimeout(function(){ $("#alert").hide(); }, 3000);
 // year
//  $(document).ready(function(){
//     $("select[name='years']").change(function () {
//         jQuery.ajax({
//             type: "POST",
//             data:  $("form#a").serialize(),
//             success: function(data) {
//               $("#yearby").load(" #yearby");
//                 console.log(data);
//             }
//         });
//     });
// });
</script>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#exp1").click(function() {
      $("#xls1").table2excel({
        exclude: ".excludeThisClass",
        name: "BDC Internet UPS",
        filename: "BDC Internet UPS",
        preserveColors: false
      });
    });
    $("#exp2").click(function() {
      $("#xls2").table2excel({
        exclude: ".excludeThisClass",
        name: "BDC Phone UPS",
        filename: "BDC Phone UPS",
        preserveColors: false
      });
    });
    $("#exp3").click(function() {
      $("#xls3").table2excel({
        exclude: ".excludeThisClass",
        name: "Lead Provider",
        filename: "Lead Provider",
        preserveColors: false
      });
    });
    $("#exp4").click(function() {
      $("#xls4").table2excel({
        exclude: ".excludeThisClass",
        name: "Month Internet UPS",
        filename: "Month Internet UPS",
        preserveColors: false
      });
    });
    $("#exp5").click(function() {
      $("#xls5").table2excel({
        exclude: ".excludeThisClass",
        name: "Month Phone UPS",
        filename: "Month Phone UPS",
        preserveColors: false
      });
    });
  });
</script>

<script>
  $('.printBdc').on('click', function() {  
    window.print();  
    return false;
  });
</script>

<?php include('../../footer.php'); ?>