<?php
session_start();
if ($_SESSION["role"] == "Super Admin") {
  header("location: /index.php");
}
$page = 'agent-form';
include('../../header.php');
if($_SESSION["role"] == "Admin"){
$source_sql = "SELECT * FROM `source` WHERE `admin_id` = '$userID'";
$agent_sql = "SELECT * FROM `users` WHERE `admin_id` = '$userID'";
}
if($_SESSION["role"] == "Agent"){
  $source_sql = "SELECT * FROM `source` WHERE `admin_id` = '$adminID'";
$agent_sql = "SELECT * FROM `users` WHERE `id` = '$userID'";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Add Customer</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Add Customer</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="bg-white mb-4 radio-select-type-dv">
        <div class="card-body">
          <div class="form-group clearfix text-center m-0">
            <div class="icheck-primary d-inline">
              <input type="radio" name="agent-radio" id="r1" checked="checked">
              <label for="r1">Internet UPS</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="agent-radio" id="r2">
              <label for="r2">Phone UPS</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="agent-radio" id="r3">
              <label for="r3">Extended Warranties</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="agent-radio" id="r4">
              <label for="r4">5 Star Reviews</label>
            </div>
          </div>
        </div>
      </div>


      <div class="row">
        <!-- left column -->
        <div class="col-md-8 mx-auto">
          <?php 
          if(isset($_GET['success']) && $_GET['success'] == 1){
            echo '<div class="alert alert-success" role="alert">';
            echo 'Lead <i>'.$_GET['source'].'</i> has been captured successfuly';
            echo '</div>';
          }
          ?>   
          <!-- general form elements -->
          <div class="card card-primary custom-form-card form-dv form-internet d-block">
            <div class="card-header">
              <h3 class="card-title">Internet UPS</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="internet-form" class="internet-form" action="/pages/forms/sub-lead.php" method="post" enctype="multipart/form-data" >
              <div class="card-body">
                <div class="form-group">

                  <label>Agent Name</label>
                    <?php
                    $agent_res = $con->query($agent_sql);
                    if ($_SESSION["role"] == "Agent"){?>
                     <input type="hidden" name="name" value="<?php echo $_SESSION['alais'];?>">
                     <?php }
                    if($_SESSION["role"] == "Admin"){?>
                    <select class="form-control" name="name" id="internet_agent_field">
                    <?php while($agents = $agent_res->fetch_assoc()) {?>
                        <option value="<?php echo $agents['first_name'].' '.$agents['last_name'];?>" agent_id="<?php echo $agents['id'];?>"><?php echo $agents['first_name'].' '.$agents['last_name'];?></option>
                        <?php
                      }
                    }else{
                      ?>
                       <select disabled class="form-control" name="name" id="internet_agent_field">
                       <option selected value="<?php echo $_SESSION['alais'];?>" agent_id="<?php echo $_SESSION['id'];?>"><?php echo $_SESSION["alais"];?></option>
                       <?php
                     }
                     ?>
                   </select>
                   <input type="hidden" name="agent_id" id="internet_agent_id"/>
                 </div>

                 <div class="form-group">
                  <label>Lead Name</label>
                  <input class="form-control" name="lead_name" required>
                 </div>
                 <div class="form-group">
                  <label>Email-ID</label>
                  <input type="email" class="form-control" name="Email" required>
                 </div>
                 <div class="form-group">
                  <label>Phone Number</label>
                  <input type="tel" class="form-control" name="PhoneNumber" maxlength="15" placeholder="Phone" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1')">
                 </div>
                 <div class="form-group">
                  <label>Appt Status</label>
                  <select class="form-control appt_status" name="appt_status" id="appt_status" required>
                    <option selected value="">NOT SET </option>
                    <option value="SET"> SET</option>
                    <option value="SHOW"> SHOW</option>
                    <option value="SOLD"> SOLD</option>
                  </select>
                </div>
                <?php /*
                <div class="form-group">
                  <label>Appt Set</label>
                  <select class="form-control" name="appt_set">
                    <option selected value="">NOT SET </option>
                    <?php
                    $option_sql = "SELECT set_code FROM source";
                    $option_res = $con->query($option_sql);
                    while ($row = $option_res->fetch_assoc()) {
                     echo "<option value='".$row["set_code"]."'>".$row["set_code"]."</option>";
                   }
                   ?>
                 </select>
               </div>

               <div class="form-group">
                <label>Appt Show</label>
                <select class="form-control" name="appt_show">
                  <option selected value="">NOT SET </option>
                  <?php
                  $option_sql = "SELECT show_code FROM source";
                  $option_res = $con->query($option_sql);
                  while ($row = $option_res->fetch_assoc()) {
                   echo "<option value='".$row["show_code"]."'>".$row["show_code"]."</option>";
                 }
                 ?>
               </select>
             </div>
             <div class="form-group">
              <label>Appt Sold</label>
              <select class="form-control" name="appt_sold">
                <option selected value="">NOT SET </option>
                <?php
                $option_sql = "SELECT sold_code FROM source";
                $option_res = $con->query($option_sql);
                while ($row = $option_res->fetch_assoc()) {
                 echo "<option value='".$row["sold_code"]."'>".$row["sold_code"]."</option>";
               }
               ?>
             </select>
           </div> */?>
           <div class="form-group">
            <label>Source</label>
            <select class="form-control" name="lead_source" required>
              <?php
              $source_res = $con->query($source_sql);
              while($sources = $source_res->fetch_assoc()) { ?>
                <option value="<?php echo $sources['source']; ?>"><?php echo $sources['source']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Status</label>
            <select class="form-control status" name="status" id="status" required>
              <option class="statusrmv" value="Purchased">Purchased</option>
              <option class="statusrmv" value="Cold">Cold</option>
              <option class="statusrmv" value="Hot">Hot</option>
              <option class="statusrmv" value="No Number">No Number</option>
              <option class="statusrmv" value="Came In/Did Not Buy">Came In/Did Not Buy</option>
              <option class="statusrmv" value="Not Buying">Not Buying</option>
              <option class="statusrmv" value="Flaky">Flaky</option>
            </select>
          </div>
          <div class="form-group note-div">
              <label>Notes:</label>
              <textarea name="notes1" id="notes1" rows="4"  class="form-control notes"></textarea>
              <p class="character_count" ></p>
          </div>
          <div><p class="requiredalert"></p></div>
        </div>
        <input type="hidden" name="source" value="Internet UPS">
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div><!-- form-dv -->
    <!-- /.card -->

    <div class="card card-primary custom-form-card form-dv form-phone">
      <div class="card-header">
        <h3 class="card-title">Phone UPS</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form id="phone-form" class="phone-form" action="/pages/forms/sub-lead.php" method="post" enctype="multipart/form-data">
        <div class="card-body">
          <div class="form-group">
            <label>Agent Name</label>
              <?php
              $agent_res = $con->query($agent_sql);
              if ($_SESSION["role"] == "Agent"){?>
                <input type="hidden" name="name" value="<?php echo $_SESSION['alais'];?>">
                <?php }
              if($_SESSION["role"] == "Admin"){?>
                <select class="form-control" name="name" id="phone_agent_field">
                <?php while($agents = $agent_res->fetch_assoc()) { 
                  ?>
                  <option value="<?php echo $agents['first_name'].' '.$agents['last_name'];?>" agent_id="<?php echo $agents['id'];?>"><?php echo $agents['first_name'].' '.$agents['last_name'];?></option>
                  <?php
                } }else{
                 ?>
                 <select disabled class="form-control" name="name" id="phone_agent_field">
                 <option selected value="<?php echo $_SESSION["alais"]?>" agent_id="<?php echo $_SESSION['id'];?>"><?php echo $_SESSION["alais"]?></option>
                 <?php
               }
               ?>
             </select>
             <input type="hidden" name="agent_id" id="phone_agent_id"/>
           </div>
           <div class="form-group">
            <label>Lead Name</label>
            <input class="form-control" name="lead_name" id="phone_lead_name">
          </div>
          <div class="form-group">
            <label>Email-ID</label>
            <input type="email" class="form-control" name="Email" required>
          </div>
          <div class="form-group">
            <label>Phone Number</label>
            <input type="tel" class="form-control" name="PhoneNumber" maxlength="15" placeholder="Phone" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1')">          </div>
          <div class="form-group">
            <label>Appt Status</label>
            <select class="form-control appt_status" name="appt_status">
              <option selected value="">NOT SET </option>
              <option value="SET"> SET</option>
              <option value="SHOW"> SHOW</option>
              <option value="SOLD"> SOLD</option>
            </select>
          </div>
          <!-- <div class="form-group">
            <label>Appt Set</label>
            <select class="form-control" name="appt_set" id="phone_appt_set">
              <option selected value="">NOT SET </option>
              <option value="SET"> SET</option>
              <option value="SHOW"> SHOW</option>
              <option value="SPLIT SHOW"> SPLIT SHOW</option>
              <option value="SPLIT SOLD"> SPLIT SOLD</option>
              <option value="SOLD"> SOLD</option>
            </select>
          </div>
          <div class="form-group">
            <label>Appt Show</label>
            <select class="form-control" name="appt_show" id="phone_appt_show">
              <option selected value="">NOT SET </option>
              <option value="SET"> SET</option>
              <option value="SHOW"> SHOW</option>
              <option value="SPLIT SHOW"> SPLIT SHOW</option>
              <option value="SPLIT SOLD"> SPLIT SOLD</option>
              <option value="SOLD"> SOLD</option>

            </select>
          </div>
          <div class="form-group">
            <label>Appt Sold</label>
            <select class="form-control" name="appt_sold" id="phone_appt_sold">
              <option selected value="">NOT SET </option>
              <option value="SET"> SET</option>
              <option value="SHOW"> SHOW</option>
              <option value="SPLIT SHOW"> SPLIT SHOW</option>
              <option value="SPLIT SOLD"> SPLIT SOLD</option>
              <option value="SOLD"> SOLD</option>

            </select>
          </div> -->
          <div class="form-group">
            <label>Status</label>
            <select class="form-control status" name="status" id="phone_status">
              <option class="statusrmv" value="Purchased">Purchased</option>
              <option class="statusrmv" value="Cold">Cold</option>
              <option class="statusrmv" value="Hot">Hot</option>
              <option class="statusrmv" value="No Number">No Number</option>
              <option class="statusrmv" value="Came In/Did Not Buy">Came In/Did Not Buy</option>
              <option class="statusrmv" value="Not Buying">Not Buying</option>
              <option class="statusrmv" value="Flaky">Flaky</option>
            </select>
          </div>
          <div class="form-group note-div">
              <label>Notes:</label>
              <textarea name="notes2" id="notes2" rows="4"  class="form-control status notes"></textarea>
              <p class="character_count" ></p>
          </div>
        </div>
        <input type="hidden" name="source" value="Phone UPS">
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div><!-- form-dv -->
    <!-- /.card -->

    <div class="card card-primary custom-form-card form-dv form-extended">
      <div class="card-header">
        <h3 class="card-title">Extended Warranties</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form id="extended-form" class="extended-form" action="/pages/forms/sub-lead.php" method="post" enctype="multipart/form-data">
        <div class="card-body">
          <div class="form-group">
            <label>Agent Name</label>
              <?php
              $agent_res = $con->query($agent_sql);
              if ($_SESSION["role"] == "Agent"){?>
                <input type="hidden" name="name" value="<?php echo $_SESSION['alais'];?>">
                <?php }
              if($_SESSION["role"] == "Admin"){?>
                <select class="form-control" name="name" id="warranty_agent_field">
                <?php while($agents = $agent_res->fetch_assoc()) { 
                  ?>
                  <option value="<?php echo $agents['first_name'].' '.$agents['last_name'];?>" agent_id="<?php echo $agents['id'];?>"><?php echo $agents['first_name'].' '.$agents['last_name'];?></option>
                  <?php
                } }else{
                 ?>
                 <select disabled class="form-control" name="name" id="warranty_agent_field">
                 <option selected value="<?php echo $_SESSION["alais"]?>" agent_id="<?php echo $_SESSION['id'];?>"><?php echo $_SESSION["alais"]?></option>
                 <?php
               }
               ?>
             </select>
             <input type="hidden" name="agent_id" id="warranty_agent_id"/>
           </div>
           <div class="form-group">
            <label>Lead Name</label>
            <input class="form-control" name="lead_name" id="extended_lead_name">

          </div>
          <div class="form-group">
            <label>Email-ID</label>
            <input type="email" class="form-control" name="Email" required>
            </div>
            <div class="form-group">
            <label>Phone Number</label>
            <input type="tel" class="form-control" name="PhoneNumber" maxlength="15" placeholder="Phone" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1')">            </div>
          <div class="form-group">
            <label>Appt Status</label>
            <select class="form-control" name="appt_status">
            <option selected value="SOLD"> SOLD</option>
              <!-- <option value="SET"> SET</option>
              <option value="SHOW"> SHOW</option>
              <option value="SOLD"> SOLD</option> -->
            </select>
          </div>
          <?php /*
          <div class="form-group">
            <label>Appt Set</label>
            <select class="form-control" name="appt_set" id="extended_appt_set">
              <option selected value="">NOT SET </option>
              <option value="SOLD/SET">SOLD/SET </option>
              <option value="30%">30%</option>
            </select>
          </div>
          <div class="form-group">
            <label>Appt Show</label>
            <select class="form-control" name="appt_show" id="extended_appt_show">
              <option selected value="">NOT SET </option>
              <option value="SOLD/SET">SOLD/SET </option>
              <option value="30%">30%</option>
            </select>
          </div>
          <div class="form-group">
            <label>Appt Sold</label>
            <select class="form-control" name="appt_sold" id="extended_appt_sold">
              <option selected value="">NOT SET </option>
              <option value="SOLD/SET">SOLD/SET </option>
              <option value="30%">30%</option>
            </select>
          </div> */?>
          <div class="form-group">
            <label>Status</label>
            <select class="form-control" name="status" id="extended_status">
              <option  selected value="Purchased">Purchased</option>
              <!-- <option value="Cold">Cold</option>
              <option value="Hot">Hot</option>
              <option value="No Number">No Number</option>
              <option value="Came In/Did Not Buy">Came In/Did Not Buy</option>
              <option value="Not Buying">Not Buying</option>
              <option value="Flaky">Flaky</option> -->
            </select>
          </div>
          <div class="form-group note-div">
            <label>Notes:</label>
            <textarea name="notes3" id="notes3" rows="4"  class="form-control notes"></textarea>
            <p class="character_count" ></p>
          </div>
        </div>
        <input type="hidden" name="source" value="Extended Warranties">
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div><!-- form-dv -->
    <!-- /.card -->

    <div class="card card-primary custom-form-card form-dv form-reviews">
      <div class="card-header">
        <h3 class="card-title">5 Star Reviews</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form id="reviews-form" class="reviews-form" action="/pages/forms/sub-lead.php" method="post" enctype="multipart/form-data">
        <div class="card-body">
          <div class="form-group">
            <label>Agent Name</label>
              <?php
              $agent_res = $con->query($agent_sql);
              if ($_SESSION["role"] == "Agent"){?>
                <input type="hidden" name="name" value="<?php echo $_SESSION['alais'];?>">
                <?php }
              if($_SESSION["role"] == "Admin"){?>
                <select class="form-control" name="name" id="review_agent_field">
                <?php while($agents = $agent_res->fetch_assoc()) { 
                  ?>
                  <option value="<?php echo $agents['first_name'].' '.$agents['last_name'];?>" agent_id="<?php echo $agents['id'];?>"><?php echo $agents['first_name'].' '.$agents['last_name'];?></option>
                  <?php
                } }else{
                 ?>
                 <select disabled class="form-control" name="name" id="review_agent_field">
                 <option selected value="<?php echo $_SESSION["alais"]?>" agent_id="<?php echo $_SESSION['id'];?>"><?php echo $_SESSION["alais"]?></option>
                 <?php
               }
               ?>
             </select>
             <input type="hidden" name="agent_id" id="review_agent_id"/>
           </div>
           <div class="form-group">
            <label>Lead Name</label>
            <input class="form-control" name="lead_name" id="review_lead_name">
          </div>
          <div class="form-group">
            <label>Email-ID</label>
            <input type="email" class="form-control" name="Email" required>
          </div>
          <div class="form-group">
            <label>Phone Number</label>
            <input type="tel" class="form-control" name="PhoneNumber" maxlength="15" placeholder="Phone" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1')">          </div>
          <div class="form-group">
            <label>Filled Out</label>
            <select class="form-control" name="filled_out" id="review_filled_out">
              <option value="Yes">Yes</option>
              <!-- <option value="No">No</option> -->
            </select>
          </div>
          <div class="form-group">
            <label>Status</label>
            <select class="form-control" name="status" id="review_status">
            <option  selected value="Purchased">Purchased</option>
              <!-- <option value="Cold">Cold</option>
              <option value="Hot">Hot</option>
              <option value="No Number">No Number</option>
              <option value="Came In/Did Not Buy">Came In/Did Not Buy</option>
              <option value="Not Buying">Not Buying</option>
              <option value="Flaky">Flaky</option> -->
            </select>
          </div>
          <div class="form-group note-div">
            <label>Notes:</label>
            <textarea name="notes4" rows="4"  id="notes4" class="form-control notes" ></textarea>
            <p class="character_count" ></p>
            <!-- <h6>only enter till 500 charecter <span  id="charNum">500</span></h6> -->
          </div>
        </div>
        <input type="hidden" name="source" value="5 Star Reviews">
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div><!-- form-dv -->
    <!-- /.card -->

  </div>
  <!--/.col (left) -->
</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
   window.onload = function () {
            tinymce.init({
                selector: 'textarea',
                // width: 400,
                browser_spellcheck: true,
                setup: function (ed) {
                    ed.on('input', function (e) {
                        var count = CountCharacters(this);
                        if(count==false){
                          var body = tinymce.get(this.id).getBody();
                          var content = tinymce.trim(body.innerText || body.textContent);
                          var curr_content = tinymce.get(this.id).getContent();
                          var tag_len = parseInt(curr_content.length) - parseInt(content.length);
                          // console.log(parseInt(curr_content.length) - parseInt(content.length));
                          tinymce.get(this.id).setContent(content.substr(0, 500));
                          // tinymce.get(this.id).innerText = content.substr(0, 20);
                        }
                        else{
                          $("#"+this.id).next().html("Characters: " + count + "&nbsp; &nbsp;  &nbsp; Limit: 500");
                        }
                    });
                }
            });
        }
   function CountCharacters(obj) {
        var body = tinymce.get(obj.id).getBody();
        var content = tinymce.trim(body.innerText || body.textContent);
        var limit = content.length;

        var max = 500;
        if (limit == max-1) {
            alert("Warning: Exceeding character limit(500) will remove the formatting!");
        }
        if (limit > max) {
            alert("Maximum " + max + " characters allowed.")
            return false;
        }
        else
        return parseInt(limit);
    };
    // $(document).ready(function(){
    //   $('#internet-form').submit(function (e) { 
    //     if($('#notes1').val() != ''){
    //       // alert('Name is empty');
    //       $('.character_count').text("Field is required");
    //       return false;
    //     } else{
    //       $(this).submit();
    //     }
       
    //   });
    // });  // $(document).ready(function() { 
    //   var note1 = $("#notes1").val();
    //   var note2 = $("#notes2").val();
    //   var note3 = $("#notes3").val();
    //   var note4 = $("#notes3").val();
    //   $(".requiredsubmit").click(function() { 
    //     if(note1 == " "){
    //       alert("Please fill is required!");
    //       $(".requiredalert").html("Please fill is required!")
    //     }
    //   });

    // });
	</script>
<?php include('../../footer.php'); ?>