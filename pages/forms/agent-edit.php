<?php 
  $page = 'agent-edit'; 
  include('../../header.php'); 
  $id = $_SESSION["agent_edit_id"];
  $medium = $_GET["medium"];
  $source_sql = "SELECT * FROM `source`";
  $agent_sql = "SELECT * FROM `lead_capture` WHERE `id`='$id'";
  $agent_res = $con->query($agent_sql);
  $data = $agent_res->fetch_assoc();
  
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Agent</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
                        <li class="breadcrumb-item active">Edit Agent</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- <div class="bg-white mb-4 radio-select-type-dv">
        <div class="card-body">
          <div class="form-group clearfix text-center m-0">
            <div class="icheck-primary d-inline">
              <input type="radio" name="agent-radio" id="r1" checked="checked">
              <label for="r1">Internet UPS</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="agent-radio" id="r2">
              <label for="r2">Phone UPS</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="agent-radio" id="r3">
              <label for="r3">Extended Warranties</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="agent-radio" id="r4">
              <label for="r4">5 Star Reviews</label>
            </div>
          </div>
        </div>
      </div> -->


            <div class="row">
                <!-- left column -->
                <div class="col-md-8 mx-auto">
                    <!-- general form elements -->
                    <?php if ( $medium == "Internet UPS") { ?>
                    <div class="card card-primary d-block">
                        <div class="card-header">
                            <h3 class="card-title">Internet UPS</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="/pages/forms/sub-agent-edit.php" method="post" enctype="multipart/form-data">
                            <?php
                              if (isset($_GET["from"])) {
                                echo "<input type='hidden' name='from' value='".$_GET["from"]."'>";
                              }
                              ?>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Agent Name: </label>
                                    <?php echo $data["agent_name"];
                                    ?>
                                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                                </div>
                                <div class="form-group">
                                    <label>Lead Name</label>
                                    <input class="form-control" name="lead_name" value="<?php echo $data["name"]; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Appt Status</label>
                                    <select class="form-control appt_status" name="appt_status">
                                        <?php if ($data["appt_sold"]=="SOLD") { $appt_status    = "SOLD"; } if ($data["appt_sold"]=="") { $appt_status = "SHOW"; } if ($data["appt_show"]=="") { $appt_status = "SET"; } if ($data["appt_set"]=="") {  $appt_status = ""; } ?>
                                        <option value="" <?php if ($appt_status=="") { echo "selected"; }?>>NOT SET
                                        </option>
                                        <option value="SET" <?php if ($appt_status=="SET") { echo "selected"; }?>> SET
                                        </option>
                                        <option value="SHOW" <?php if ($appt_status=="SHOW") { echo "selected"; }?>>
                                            SHOW</option>
                                        <option value="SOLD" <?php if ($appt_status=="SOLD") { echo "selected"; }?>>
                                            SOLD</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Source</label>
                                    <select class="form-control" name="lead_source">
                                        <?php
                                          $source_res = $con->query($source_sql);
                                          while($sources = $source_res->fetch_assoc()) { ?>
                                        <option value="<?php echo $sources['source']; ?>"
                                            <?php if ($sources['source']==$data["source"]) { echo "selected"; }?>>
                                            <?php echo $sources['source']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control status" name="status">
                                        <option class="statusrmv" value="Purchased"
                                            <?php if ($data["status"]=="Purchased") { echo "selected"; }?>>Purchased
                                        </option>
                                        <option class="statusrmv" value="Cold"
                                            <?php if ($data["status"]=="Cold") { echo "selected"; }?>>Cold</option>
                                        <option class="statusrmv" value="Hot"
                                            <?php if ($data["status"]=="Hot") { echo "selected"; }?>>Hot</option>
                                        <option class="statusrmv" value="No Number"
                                            <?php if ($data["status"]=="No Number") { echo "selected"; }?>>No Number
                                        </option>
                                        <option class="statusrmv" value="Came In/Did Not Buy"
                                            <?php if ($data["status"]=="Came In/Did Not Buy") { echo "selected"; }?>>
                                            Came In/Did Not Buy</option>
                                        <option class="statusrmv" value="Not Buying"
                                            <?php if ($data["status"]=="Not Buying") { echo "selected"; }?>>Not Buying
                                        </option>
                                        <option class="statusrmv" value="Flaky"
                                            <?php if ($data["status"]=="Flaky") { echo "selected"; }?>>Flaky</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea name="notes1" rows="4" id="notes1" class="form-control notes">
                                      <?= $data['notes']?>
                                    </textarea>
                                    <p class="character_count"></p>
                                </div>
                            </div>

                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <input type="hidden" value="<?php echo $medium; ?>" name="median">
                        </form>
                    </div><!-- form-dv -->
                    <!-- /.card -->
                    <?php }
                    elseif( $medium == "Phone UPS"){
                    ?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Phone UPS</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="/pages/forms/sub-agent-edit.php" method="post" enctype="multipart/form-data">
                            <?php
                              if (isset($_GET["from"])) {
                                echo "<input type='hidden' name='from' value='".$_GET["from"]."'>";
                              }
                              ?>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Agent Name: </label>
                                    <?php echo $data["agent_name"]; ?>
                                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                                </div>
                                <div class="form-group">
                                    <label>Lead Name</label>
                                    <input class="form-control" name="lead_name" value="<?php echo $data["name"]; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Appt Status</label>
                                    <select class="form-control appt_status" name="appt_status">
                                        <?php if ($data["appt_sold"]=="SOLD") { $appt_status    = "SOLD"; } if ($data["appt_sold"]=="") { $appt_status = "SHOW"; } if ($data["appt_show"]=="") { $appt_status = "SET"; } if ($data["appt_set"]=="") {  $appt_status = ""; } ?>
                                        <option value="" <?php if ($appt_status=="") { echo "selected"; }?>>NOT SET
                                        </option>
                                        <option value="SET" <?php if ($appt_status=="SET") { echo "selected"; }?>> SET
                                        </option>
                                        <option value="SHOW" <?php if ($appt_status=="SHOW") { echo "selected"; }?>>
                                            SHOW</option>
                                        <option value="SOLD" <?php if ($appt_status=="SOLD") { echo "selected"; }?>>
                                            SOLD</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control status" name="status">
                                        <option class="statusrmv" value="Purchased"
                                            <?php if ($data["status"]=="Purchased") { echo "selected"; }?>>Purchased
                                        </option>
                                        <option class="statusrmv" value="Cold"
                                            <?php if ($data["status"]=="Cold") { echo "selected"; }?>>Cold</option>
                                        <option class="statusrmv" value="Hot"
                                            <?php if ($data["status"]=="Hot") { echo "selected"; }?>>Hot</option>
                                        <option class="statusrmv" value="No Number"
                                            <?php if ($data["status"]=="No Number") { echo "selected"; }?>>No Number
                                        </option>
                                        <option class="statusrmv" value="Came In/Did Not Buy"
                                            <?php if ($data["status"]=="Came In/Did Not Buy") { echo "selected"; }?>>
                                            Came In/Did Not Buy</option>
                                        <option class="statusrmv" value="Not Buying"
                                            <?php if ($data["status"]=="Not Buying") { echo "selected"; }?>>Not Buying
                                        </option>
                                        <option class="statusrmv" value="Flaky"
                                            <?php if ($data["status"]=="Flaky") { echo "selected"; }?>>Flaky</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea name="notes2" rows="4" id="notes2" class="form-control notes">
                                    <?= $data['notes']?>
                                    </textarea>
                                    <p class="character_count"></p>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <input type="hidden" value="<?php echo $medium; ?>" name="median">
                        </form>
                    </div><!-- form-dv -->
                    <!-- /.card -->
                    <?php }
                      elseif( $medium == "Extended Warranties"){
                       ?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Extended Warranties</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="/pages/forms/sub-agent-edit.php" method="post" enctype="multipart/form-data">
                            <?php
                              if (isset($_GET["from"])) {
                                echo "<input type='hidden' name='from' value='".$_GET["from"]."'>";
                              }
                              ?>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Agent Name: </label>
                                    <?php echo $data["agent_name"]; ?>
                                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                                </div>
                                <div class="form-group">
                                    <label>Lead Name</label>
                                    <input class="form-control" name="lead_name" value="<?php echo $data["name"]; ?>">

                                </div>
                                <div class="form-group">
                                    <label>Appt Status</label>
                                    <select class="form-control appt_status" name="appt_status">
                                        <?php if ($data["appt_sold"]=="SOLD") { $appt_status    = "SOLD"; } if ($data["appt_sold"]=="") { $appt_status = "SHOW"; } if ($data["appt_show"]=="") { $appt_status = "SET"; } if ($data["appt_set"]=="") {  $appt_status = ""; } ?>
                                        <option value="SOLD" <?php if ($appt_status=="SOLD") { echo "selected"; }?>>
                                            SOLD</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control status" name="status">
                                        <option class="statusrmv" value="Purchased"
                                            <?php if ($data["status"]=="Purchased") { echo "selected"; }?>>Purchased
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea name="notes3" rows="4" id="notes3" class="form-control notes">
                                    <?= $data['notes']?>
                                    </textarea>
                                    <p class="character_count"></p>
                                </div>
                            </div>

                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <input type="hidden" value="<?php echo $medium; ?>" name="median">
                        </form>
                    </div><!-- form-dv -->
                    <!-- /.card -->
                    <?php }
                      else{
                       ?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">5 Star Reviews</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="/pages/forms/sub-agent-edit.php" method="post" enctype="multipart/form-data">
                            <?php
                              if (isset($_GET["from"])) {
                                echo "<input type='hidden' name='from' value='".$_GET["from"]."'>";
                              }
                              ?>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Agent Name: </label>
                                    <?php echo $data["agent_name"]; ?>
                                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                                </div>
                                <div class="form-group">
                                    <label>Lead Name</label>
                                    <input class="form-control" name="lead_name" value="<?php echo $data["name"]; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Filled Out</label>
                                    <select class="form-control" name="filled_out" id="">
                                        <option selected value="<?php  echo $data["filled_out"]; ?>">
                                            <?php  echo $data["filled_out"]; ?></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status">
                                        <option selected value="<?php  echo $data["status"]; ?>">
                                            <?php  echo $data["status"]; ?></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea name="notes4" rows="4" id="notes4" class="form-control notes">
                                    <?= $data['notes']?>
                                    </textarea>
                                    <p class="character_count"></p>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <input type="hidden" value="<?php echo $medium; ?>" name="median">
                        </form>
                    </div><!-- form-dv -->
                    <!-- /.card -->
                    <?php }
                       ?>
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
window.onload = function() {
    tinymce.init({
        selector: 'textarea',
        // width: 400,
        browser_spellcheck: true,
        setup: function(ed) {
            ed.on('input', function(e) {
                var count = CountCharacters(this);
                if (count == false) {
                    var body = tinymce.get(this.id).getBody();
                    var content = tinymce.trim(body.innerText || body.textContent);
                    var curr_content = tinymce.get(this.id).getContent();
                    var tag_len = parseInt(curr_content.length) - parseInt(content.length);
                    // console.log(parseInt(curr_content.length) - parseInt(content.length));
                    tinymce.get(this.id).setContent(content.substr(0, 500));
                    // tinymce.get(this.id).innerText = content.substr(0, 20);
                } else {
                    $("#" + this.id).next().html("Characters: " + count +
                        "&nbsp; &nbsp;  &nbsp; Limit: 500");
                }
            });
        }
    });
}

function CountCharacters(obj) {
    var body = tinymce.get(obj.id).getBody();
    var content = tinymce.trim(body.innerText || body.textContent);
    var limit = content.length;

    var max = 500;
    if (limit == max - 1) {
        alert("Warning: Exceeding character limit(500) will remove the formatting!");
    }
    if (limit > max) {
        alert("Maximum " + max + " characters allowed.")
        return false;
    } else
        return parseInt(limit);
};
</script>
<?php include('../../footer.php'); ?>