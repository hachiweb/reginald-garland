<?php 
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: /index.php");
}
    $page = 'create-source';
    include('../../header.php'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Create Source</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Create Source</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <!-- left column -->
        <div class="col-md-8 mx-auto">
            <?php 
                if(isset($_GET['success']) && $_GET['success'] == 1){
                    echo '<div class="alert alert-success" role="alert">';
                    echo 'Source <i>'.$_GET['source'].'</i> has been created successfuly';
                    echo '</div>';
                }
            ?>    
          <!-- general form elements -->
          <div class="card card-primary custom-form-card">
            <div class="card-header">
              <h3 class="card-title">Create Source</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="sub-create_source.php" method="post" enctype="multipart/form-data" >
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Source</label>
                      <input type="text" name="source" class="form-control" id="" placeholder="Source">
                    </div>
                  </div>
                </div>  

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>SET code</label>
                      <input type="text" name="set" class="form-control" id="" placeholder="SET code">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>SHOW code</label>
                      <input type="text" name="show" class="form-control" id="" placeholder="SHOW code">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>SOlD code</label>
                      <input type="text" name="sold" class="form-control" id="" placeholder="SOLD code">
                    </div>
                  </div>
                </div>           
                <!-- /.card-body -->

                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div><!-- form-dv -->
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('../../footer.php'); ?>