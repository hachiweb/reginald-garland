<?php
include('../../dbconfig.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../../PHPMailer/Exception.php';
require '../../PHPMailer/PHPMailer.php';
require '../../PHPMailer/SMTP.php';
$mail = new PHPMailer;
$phoneA   = $_POST['contcode'];
$phoneB   = $_POST['phone'];
$contact  = $phoneA." ".$phoneB;
if ($contact==" ") {
    $contact ="";
}
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https://"; 
else
$link = "http://"; 
$site_url = $link.$_SERVER['HTTP_HOST'];
$first_name   = $_POST['first_name'];
$last_name    = $_POST['last_name'];
$username     = $_POST['username'];
$password     = $_POST['password'];
$emailname = $first_name." ".$last_name;
$role = "Admin";
$confirm_password     = $_POST['confirm_password'];
$last_id = "SELECT id FROM `users` ORDER BY id DESC";
$last_id = $con->query($last_id);
$last_id = mysqli_fetch_assoc($last_id);
$current_id = (int)$last_id['id']+1;
$target_dir = "../../upload/admin/".$current_id;
if(!is_dir($target_dir)){
    mkdir($target_dir,0777, true);
}
if(!preg_match("/^[a-zA-Z'. -]+$/",$first_name) || !preg_match("/^[a-zA-Z'. -]+$/",$last_name) ){
    header('location: error.php?msg=Invalid User Name, Only alphabet are require');
    die();
}
if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
    header('location: error.php?msg=Invalid Email!');
    die();
}
if ($password != $confirm_password ) {
    header('location: error.php?msg=Password not match');
    die();
}
if ($_FILES["Profile"]["size"] > 2000000) { //.. 2000000 size is equal to 2MB.
    header('location: error.php?msg=Sorry, your file is too large.');
    die();
}
$uppercase = preg_match('@[A-Z]@', $password);
$lowercase = preg_match('@[a-z]@', $password);
$number    = preg_match('@[0-9]@', $password);

if(!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
    header('location: error.php?msg=Password Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters');
    die(); 
}




if(!empty($_POST)){
    function generateRandomString($length = 50) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
      }
      $emailvarified = generateRandomString();
    $mail->Host     = 'hachiweb.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'test@hachiweb.com';
    $mail->Password = 'hachi#123';
    $mail->SMTPSecure = 'tls';
    $mail->Port     = 587;
    $mail->setFrom('test@hachiweb.com', 'Garland Pro Solutions');
    // Add a recipient
    $mail->addAddress($username);
    // Email subject
    $mail->Subject = "Garland Pro Solutions";
    // Set email format to HTML
    $mail->isHTML(true);
    // Email body content
    $mailContent = '<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <title>Document</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <style>
            .main {
                background-color: #6f42c1;
                border-radius: 8px;
            }
    
            .main span {
                color: white;
                font-size: 20px;
            }
    
            .main img {
                width: 6%;
                padding: 10px;
                border-radius: 50%;
            }
    
            body {
                background-color: #f8f9fa;
            }
    
            .main-body {
                background-color: white;
                border-radius: 8px;
            }
        </style>
    </head>
    
    <body>
        <section class="container mt-4">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main">
                        <img src="http://garlandprosolutions.hachistaging.com/dist/img/logo2.png" alt="Best Performance analysis lead management system">
                        <span>Garland Pro Solutions</span>
                    </div>
                </div>
            </div>
        </section>
        <section class="container mt-4">
            <div class="row">
                <div class="col-sm-12">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h6 class="pb-2 mb-0">Dear '.$emailname.'</h6>
                        <p class="border-bottom border-gray pb-2 ml-4 mb-0">Thank you for registering to Garland Pro Solutions.</p>
                        <div class="media text-muted pt-3">
                            <img data-src="holder.js/32x32?theme=thumb&amp;bg=007bff&amp;fg=007bff&amp;size=1" alt="Technical Functionality for Lead management system" class="mr-2 rounded" style="width: 32px; height: 32px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16dd9234068%20text%20%7B%20fill%3A%23007bff%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16dd9234068%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23007bff%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.5390625%22%20y%3D%2216.9%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">@Username</strong> '.$username.'
                            </p>
                        </div>
                        <div class="media text-muted pt-3">
                            <img data-src="holder.js/32x32?theme=thumb&amp;bg=e83e8c&amp;fg=e83e8c&amp;size=1" alt="statistical population manage" class="mr-2 rounded" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16dd923406f%20text%20%7B%20fill%3A%23e83e8c%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16dd923406f%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23e83e8c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.5390625%22%20y%3D%2216.9%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 32px; height: 32px;">
                            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">@Password</strong>'.$password.'
                            </p>
                        </div>
                        <div class="media text-muted pt-3">
                            <img data-src="holder.js/32x32?theme=thumb&amp;bg=6f42c1&amp;fg=6f42c1&amp;size=1" alt="standard statistical procedure relationship management" class="mr-2 rounded" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16dd9234073%20text%20%7B%20fill%3A%236f42c1%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16dd9234073%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%236f42c1%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.5390625%22%20y%3D%2216.9%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 32px; height: 32px;">
                            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">@Registerd as</strong> '.$role.'
                            </p>
                        </div>
                        <div class="media text-muted pt-3">
                            <img data-src="holder.js/32x32?theme=thumb&amp;bg=e83e8c&amp;fg=e83e8c&amp;size=1" alt="best probability and statistics management system" class="mr-2 rounded" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16dd923406f%20text%20%7B%20fill%3A%23e83e8c%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16dd923406f%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23e83e8c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.5390625%22%20y%3D%2216.9%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 32px; height: 32px;">
                            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">@Varification Link</strong> <a href="'.$site_url.'/emailvarify.php?A='.$current_id.'&B='.$emailvarified.'">Plaese Varify Your Email</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    
    </html>';
    $mail->Body = $mailContent;
    // Send email
    $date = new DateTime(date("Y-m-d"));
    $date->modify('+31 day');
    $subscription = $date->format('Y-m-d');
    $sql = "SELECT count(username) as total FROM `users` WHERE `username` = '$username'";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    if ($row["total"] != 0) {
      header('location: error.php?msg=Email already Exist !');
      die();
      
    }
    if (!empty($_FILES["Profile"]["name"])) {
        $tempfilename = str_replace('\'','',str_replace(',','',str_replace(' ','',$_FILES["Profile"]["tmp_name"])));
        $filename = str_replace('\'','',str_replace(',','',str_replace(' ','',$_FILES["Profile"]["name"])));
        move_uploaded_file($tempfilename, "$target_dir/$filename");
    }else {
        $filename ='';
    }
    $agenttemppass = $password;
    $password = md5($password);
    $sql = "INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `role`, `filename`,`subscription`,`contact`,`varify`) VALUES ('$first_name', '$last_name', '$username','$password','$role', '$filename','$subscription','$contact','$emailvarified')";
    if($mail->send()){
        if ($con->query($sql) === TRUE) {
            header("location:register-admin.php?success=1&name=$first_name&pass=$agenttemppass");
        } else {
            header('location:error.php');
            die();
        }
    } else {
        header('location:error.php');
        die();
    }
   
}
$con->close(); 
?>