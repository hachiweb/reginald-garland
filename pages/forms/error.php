<?php
error_reporting(0);
session_start();
if ($_SESSION["role"] == !"") {
  include('../../header.php');
} else {
  include('../../pub-header.php');
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Error</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">Error</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page text-center">
        <div class="error-content">
          <h3><i class="fas fa-exclamation-triangle text-danger"></i> Oops! Something went wrong.</h3>

          <p> <?php echo $_GET["msg"]; ?></p>
          <p>Meanwhile, you may return to <a href="../../index.php">Home</a> or try again.
          </p>
          <div class="text-center">
        <?php
              if (isset($_GET["agent"])) {
                echo "Lead : <b>".$_GET["lead"]."</b> already registered with Agent : <b>".$_GET["agent"].'</b>';
              }
        ?>
        </div>
          
        </div>
       
      </div>
     
      <!-- /.error-page -->

    </section>
    <!-- /.content -->
  </div>

<?php include('../../footer.php'); ?>