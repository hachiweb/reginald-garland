<!DOCTYPE html>
<?php 
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: /index.php");
}
$page = 'register-agent';
include('../../header.php'); 


?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Register Agent Forms</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Register Agent Forms</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <!-- left column -->
        <div class="col-md-8 mx-auto">
          <?php 
          if(isset($_GET['success']) && $_GET['success'] == 1){
            echo '<div class="alert alert-success" role="alert">';
            echo 'Agent <i>'.$_GET['name'].' & Password '.$_GET['pass'].'</i> has been created successfuly';
            echo '</div>';
          }
          ?>    
          <!-- general form elements -->
          <div class="card card-primary custom-form-card">
            <div class="card-header">
              <h3 class="card-title">Register Agent</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="registerAgent" action="sub-agent.php" method="post" enctype="multipart/form-data" >
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>First Name</label>
                      <input type="text" name="first_name" class="form-control" id="first_nameee" placeholder="First Name">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Last Name</label>
                      <input  type="text" name="last_name" class="form-control" id="last_namee" placeholder="Last Name">
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label>Email</label>
                  <input  type="email" name="username" class="form-control" id="" placeholder="Email">
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input  type="password" name="password" class="form-control" id="agent_password" placeholder="Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
                </div>

                <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" name="confirm_password" class="form-control" id="agent_confirm_password" placeholder="Confirm Password">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Profile Photo</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" accept="image/*" name="Profile" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="source" value="Register Agent">
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div><!-- form-dv -->
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('../../footer.php'); ?>