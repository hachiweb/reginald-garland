<?php
include('../../dbconfig.php');
$first_name   = $_POST['first_name'];
$last_name    = $_POST['last_name'];
$username     = $_POST['username'];
$phone     = $_POST['phone'];
$current_id   = $_POST['id'];
$target_dir = "../../upload/admin/".$current_id;
if (!is_dir($target_dir)) {
    mkdir($target_dir, 0777, true);
}
if ($_FILES["Profile"]["size"] > 2000000) { //.. 2000000 size is equal to 2MB.
    header('location: error.php?msg=Sorry, your file is too large.');
    die();
}
if(!preg_match("/^[a-zA-Z'. -]+$/",$first_name) || !preg_match("/^[a-zA-Z'. -]+$/",$last_name) ){
    header('location: error.php?msg=Invalid User Name, Only alphabet are require');
    die();
}
if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
    header('location: error.php?msg=Invalid Email!');
    die();
}
if(!empty($_POST)){
    $sql = "SELECT * FROM `users` WHERE `id` != '$current_id' AND `username` = '$username'";
    $result = $con->query($sql);
    if ($result->num_rows != 0) {
      header('location: error.php?msg=Email already Exist !');
      die();
    }
    if (!empty($_FILES["Profile"]["name"])) {
        $tempfilename = str_replace('\'','',str_replace(',','',str_replace(' ','',$_FILES["Profile"]["tmp_name"])));
        $filename = str_replace('\'','',str_replace(',','',str_replace(' ','',$_FILES["Profile"]["name"])));
        move_uploaded_file($tempfilename, "$target_dir/$filename");
        $sql ="UPDATE `users` SET `first_name` = '$first_name', `last_name` = '$last_name', `username` = '$username', `contact` = '$phone', `filename` = '$filename' WHERE `id` = '$current_id'";
    }
    else {
        $sql ="UPDATE `users` SET `first_name` = '$first_name', `last_name` = '$last_name', `contact` = '$phone', `username` = '$username'  WHERE `id` = '$current_id'";
    }
    if ($con->query($sql) === TRUE) {
        header("location:../tables/view-admin.php?success=edit");
        } else {
        header('location:error.php');
        die();
        }
   
}
$con->close(); 
?>