<?php
    ob_start();
    session_start();
    // if ($_SESSION["role"] == "Super Admin") {
    // header("location: /index.php");
    // }
    $page = 'agent-form';
    include('../../header.php');
    // if($_SESSION["role"] == "Admin"){
    //     $source_sql = "SELECT * FROM `source` WHERE `admin_id` = '$userID'";
    //     $agent_sql = "SELECT * FROM `users` WHERE `admin_id` = '$userID'";
    // }
    // if($_SESSION["role"] == "Agent"){
    //     $source_sql = "SELECT * FROM `source` WHERE `admin_id` = '$adminID'";
    //     $agent_sql = "SELECT * FROM `users` WHERE `id` = '$userID'";
    // }

    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $notification = "SELECT * FROM `notification` WHERE `id`='$id'";
        $result = mysqli_query($con, $notification);
        if($result)
        {
            // echo '<pre>';
            $row = $result->fetch_assoc();
            $date_time = explode(" ",$row['date_time']);
            // print_r($row);

        }
        else
        {   
            echo $notification;
            echo mysqli_error($con);
        }    
    }
    else{
        echo '<div class="jumbotron jumbotron-fluid text-center "><div class="container alert alert-danger"><h4 class="display-6">No Notification id passed</h4></div></div>';
        exit;
    }

// updation

    if(isset($_POST['update']))
    {
        $title = $_POST['title'];
        $date   = $_POST['date'];
        $time   = $_POST['time'];
        $message = $_POST['message'];
        $date_time = $date.' '.$time; 
        $sql = "UPDATE `notification` SET `title`='$title',`date_time`='$date_time',`message`='$message' WHERE `id`='$id'";
        $query_run = mysqli_query($con, $sql);
        if($query_run)
        {
            header('location:'.$site_url.'/pages/tables/view-notification.php?id='.$id.'&status=update');
            exit();
        }
        else
        {
            echo $sql;
            echo mysqli_error($con);
        }

    }
//deletion

    if(isset($_GET['action']) && $_GET['action']=='delete'){
        $delete_query = "DELETE FROM `notification` WHERE `id`='$id'";
        mysqli_query($con, $delete_query);
        header('location:'.$site_url.'/pages/tables/view-notification.php?='.$id.'&status=delete');
        exit();
    }

    mysqli_close($con);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Notification</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
                        <li class="breadcrumb-item active">Update Notification</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8 mx-auto">
                    <!-- general form elements -->
                    <?php
                        if(isset($_GET['status']) && $_GET['status']=='true'){
                            echo '<h6 class="alert alert-success text-center deletesuccess">Record updated</h6>';
                        }
                    ?>
                    <div class="card card-primary custom-form-card form-dv form-internet d-block">
                        
                        <div class="card-header">
                            <h3 class="card-title text-center">Update Notification</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="" method="POST">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" id="field"
                                        value="<?php echo $row['title']; ?>" />
                                    <p>Characters left <span id="charNum"></span></p>
                                </div>

                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea name="message" class="form-control"
                                        rows="5"><?php echo $row['message']; ?></textarea>
                                </div>
                                <div class='row'>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Schedule Date</label>
                                            <input type="date" class="form-control" name="date"
                                                value="<?=$date_time[0];?>" required>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Schedule Time</label>
                                            <input type="time" class="form-control" name="time"
                                                value="<?=$date_time[1];?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <input type="submit" class="btn btn-primary" name="update" value="UPDATE">
                            </div>
                        </form>
                    </div><!-- form-dv -->
                    <!-- /.card -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
$(document).ready(function() {
    $("#field").on('input', function(e) {
        el = $(this);
        if (el.val().length >= 40) {
            el.val(el.val().substr(0, 40));
        } else {
            $("#charNum").text(40 - el.val().length);
        }
    });
});
</script>

<?php include('../../footer.php'); ?>