<?php
$page = 'agent-form';
include('../../header.php');
?>
<script>
 var OneSignal = window.OneSignal || [];
$(document).ready(function(){
    var base_url = window.location.origin;
    var host = window.location.host;
    var pathArray = window.location.pathname.split( '/' );

    if(host=='gpsdealers.lan'){
        $("#app_id").val('b0b3b234-ec36-4a6e-93a5-b988f2d49eae');
        OneSignal.push(function() {
            OneSignal.init({
                appId: "b0b3b234-ec36-4a6e-93a5-b988f2d49eae",
            });
        });
    }
    else{
        $("#app_id").val('f15a740d-065e-43b6-8f96-7cedaf0aaba8');    
        OneSignal.push(function() {
            OneSignal.init({
                appId: "f15a740d-065e-43b6-8f96-7cedaf0aaba8",
            });
        });
    }
    var flag = 0;
    var d = new Date();
    var n = -d.getTimezoneOffset();

    if(n<0){
        flag = 1;
        var offset = -n/60;
    }
    else{
        var offset = n/60;
    }
    var decimalTime = parseFloat(offset);
    decimalTime = decimalTime * 60 * 60;
    var hours = Math.floor((decimalTime / (60 * 60)));
    decimalTime = decimalTime - (hours * 60 * 60);
    var minutes = Math.floor((decimalTime / 60));
    decimalTime = decimalTime - (minutes * 60);
    var seconds = Math.round(decimalTime);

    if(flag == 1)
    {
        hours = "-0" + hours;
    }
    else{
        hours = "+0" + hours;
    }
    if(minutes < 10)
    {
        minutes = "0" + minutes;
    }
    // else{
    //     minutes = "+0" + minutes;
    // }
    // if(seconds < 0)
    // {
    //     seconds = "-0" + seconds;
    // }
    // else{
    //     seconds = "+0" + seconds;
    // }
    console.log(hours + ":" + minutes);
    $("#utc_time").val("" + hours + ":" + minutes);

});
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Create Notification</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
                        <li class="breadcrumb-item active">Create Notification</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-9 mx-auto">
                    <!-- general form elements -->
                    <div class="card card-primary custom-form-card form-dv form-internet d-block">
                        <div class="card-header">
                            <h3 class="card-title">Create Notification</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="internet-form" class="internet-form"  action="/pages/forms/sub-notification.php"
                            method="POST" enctype="multipart/form-data" onsubmit="return IsEmpty();">
                            <input type="hidden" name="app_id" id="app_id">
                            <input type="hidden" name="user_id"  id="user_id" value="<?php echo $_SESSION['id']; ?>">
                            <input type="hidden" name="device_token"  id="device_token">
                            <input type="hidden" name="utc_time"  id="utc_time">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" id="field" autofocus autocomplete="off"
                                        required />
                                    <p>Characters left <span id="charNum"></span></p>
                                </div>

                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea name="message" class="form-control" rows="5" required></textarea>
                                </div>
                                <div class='row'>
                                    <div class="col-6">
                                    <div class="form-group">
                                    <label>Schedule Date</label>
                                    <input type="date" class="form-control" name="date" required>
                                    </div>
                                    </div>
                                    <div class="col-6">
                                    <div class="form-group">
                                    <label>Schedule Time</label>
                                    <input type="time" class="form-control" name="time" required>
                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="notification_sub" id="pagereload" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div><!-- form-dv -->
                    <!-- /.card -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
        <button type="button" class="btn toastrDefaultWarning d-none"></button>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>
<script>
function IsEmpty() {
  if ($("input[name='device_token']").val()== "") {
    $('.toastrDefaultWarning').click();
    return false;
  }
    return true;
}
$('.toastrDefaultWarning').click(function() {
    toastr.warning('Device token is not loaded yet, please wait for few seconds or refresh the page and try again.');
    setTimeout(doSomething, 3000);
    function doSomething() {
        window.location.reload();
    }
});
$(document).ready(function() {
    $("#field").on('input', function(e) {
        el = $(this);
        if (el.val().length >= 40) {
            el.val(el.val().substr(0, 40));
        } else {
            $("#charNum").text(40 - el.val().length);
        }
    });
    
    // $("#pagereload").click(function(){
    //     window.location.reload(true);
    // });
    // $(document).on('click','#subscribe-button',function(){
    //     window.location.reload(true);
    // });
    
});

//Web Push notification via oneSignal 
OneSignal.push(function() {
  /* These examples are all valid */
  var isPushSupported = OneSignal.isPushNotificationsSupported();
    if (isPushSupported) {
        // Push notifications are supported
        console.log("supported");
        OneSignal.isPushNotificationsEnabled(function(isEnabled) {
        if (isEnabled){
            console.log("Push notifications are enabled!");
            
            /* These examples are all valid */
            OneSignal.getUserId(function(userId) {
                console.log("OneSignal User ID:", userId);
                // window.location.reload();
                // (Output) OneSignal User ID: 270a35cd-4dda-4b3f-b04e-41d7463a2316   
                document.getElementById('device_token').value = userId;    
            });
        }
        else{
            console.log("Push notifications are not enabled yet.");
            OneSignal.push(function() {
                OneSignal.showSlidedownPrompt();
            }); 
            // window.location.reload();
        }   
         });
    } else {
        // Push notifications are not supported
        console.log("Not supported");
    }
 });
</script>

 