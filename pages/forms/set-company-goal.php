<?php
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: /index.php");
}
$page = 'set-company-goal';
include '../../header.php';
$userID = $_SESSION['id'];
$sql = "SELECT * FROM company_goal WHERE `admin_id` = '$userID'";
$result = $con->query($sql);
if ($result->num_rows == 0) {
  $i_set_lead = "";
  $i_show_set = "";
  $i_sold_set = "";
  $i_sold_show = "";
  $i_sold = "";
  $p_set_lead = "";
  $p_show_set = "";
  $p_sold_set = "";
  $p_sold_show = "";
  $p_sold = "";
} else {
  $row = $result->fetch_assoc();
  $i_set_lead = $row["i_set_lead"];
  $i_show_set = $row["i_show_set"];
  $i_sold_set = $row["i_sold_set"];
  $i_sold_show = $row["i_sold_show"];
  $i_sold = $row["i_sold"];
  $p_set_lead = $row["p_set_lead"];
  $p_show_set = $row["p_show_set"];
  $p_sold_set = $row["p_sold_set"];
  $p_sold_show = $row["p_sold_show"];
  $p_sold = $row["p_sold"];
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Set Company Goals</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Set Company Goals</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="bg-white mb-4 radio-select-type-dv">
        <div class="card-body">
          <div class="form-group clearfix text-center m-0">
            <div class="icheck-primary d-inline">
              <input type="radio" name="set-goal-radio" id="r1" checked="checked">
              <label for="r1"> Set Company Goal for Internet Performance</label>
            </div>
            <div class="icheck-primary d-inline">
              <input type="radio" name="set-goal-radio" id="r2">
              <label for="r2"> Set Company Goal for Phone Performance</label>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- left column -->
        <div class="col-md-8 mx-auto">
          <!-- general form elements -->
          <div class="card card-primary custom-form-card set-internet">
            <div class="card-header">
              <h3 class="card-title">COMPANY GOAL FOR INTERNET PERFORMANCE</h3>
            </div>
            <!-- /.card-header -->
            <!-- form star/t -->
            <form action="sub-company-goal.php" method="post" enctype="multipart/form-data">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>APPT SET/LEADS</label>
                      <input type="number" name="i_set_lead" class="form-control" id="" value="<?= $i_set_lead ?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>APPT SHOW/SET</label>
                      <input type="number" name="i_show_set" class="form-control" id="" value="<?= $i_show_set ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>SOLD/SET</label>
                      <input type="number" name="i_sold_set" class="form-control" id="" value="<?= $i_sold_set ?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>SOLD/SHOW</label>
                      <input type="number" name="i_sold_show" class="form-control" id="" value="<?= $i_sold_show ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>SOLD</label>
                      <input type="number" name="i_sold" class="form-control" id="" value="<?= $i_sold ?>">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <input type="hidden" name="type" value="internet">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
          </div><!-- form-dv -->
          <!-- /.card -->
        </div>
        <!-- general form elements -->
        <div class="card card-primary custom-form-card set-phone">
          <div class="card-header">
            <h3 class="card-title">COMPANY GOAL FOR PHONE PERFORMANCE</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form action="sub-company-goal.php" method="post" enctype="multipart/form-data">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>APPT SET/LEADS</label>
                    <input type="number" name="p_set_lead" class="form-control" id="" value="<?= $p_set_lead ?>">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>APPT SHOW/SET</label>
                    <input type="number" name="p_show_set" class="form-control" id="" value="<?= $p_show_set ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>SOLD/SET</label>
                    <input type="number" name="p_sold_set" class="form-control" id="" value="<?= $p_sold_set ?>">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>SOLD/SHOW</label>
                    <input type="number" name="p_sold_show" class="form-control" id="" value="<?= $p_sold_show ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>SOLD</label>
                    <input type="number" name="p_sold" class="form-control" id="" value="<?= $p_sold ?>">
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <input type="hidden" name="type" value="phone">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div><!-- form-dv -->
        <!-- /.card -->
      </div>
      <!--/.col (left) -->
    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>