<?php
error_reporting(0);
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
  $link = "https://"; 
else
  $link = "http://"; 
$site_url = $link.$_SERVER['HTTP_HOST'];
if(!isset($_SESSION)){
  session_start(); 
}
include "dbconfig.php";
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Garland Pro Solutions | Reset Password</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/summernote/summernote-bs4.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/sweetalert2/sweetalert2.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jsgrid/jsgrid.min.css">
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jsgrid/jsgrid-theme.min.css">
  <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo $site_url ?>/css/custom.css">
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="<?php echo $site_url ?>/index.php"><b>Garland</b>Pro Solutions</a>
      <a href="<?php echo $site_url ?>/index.php" class="brand-link">
        <img src="<?php echo $site_url ?>/dist/img/logo2.png" alt="lead management system for sales marketing" class="brand-image img-circle elevation-3 reginald-logo" style="opacity: .8">
      </a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <?php
        if (isset($_GET['success']) && $_GET['success'] == 1) {
              echo '<div class="alert alert-danger" role="alert">';
              echo '<i>' .$_GET['msg']. '</i>';
              echo '</div>';
            }
        ?>
      <div class="card-body login-card-body">
        <div class="text-center" id="message"></div>
        <p class="login-box-msg">Reset Password</p>
        <form action="reset-sub.php" method="post" enctype="multipart/form-data">
          <div class="form-group">

          <div class="form-group">
            <label>OTP</label>
            <input required type="number" name="otp" class="form-control" id="" placeholder="OTP">
          </div>
          
            <label>New Password</label>
            <input required type="password" name="password" class="form-control" id="" placeholder="Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
          </div>

          <div class="form-group">
            <label>Confirm New Password</label>
            <input required type="password" name="confirm_password" class="form-control" id="" placeholder="Confirm Password">
          </div>
          <div class="row mb-3">
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
            </div>
          </div>
        </form>
        <div class="text-center">
          <a href="<?php echo $site_url ?>/about.php"> ABOUT US </a> |
          <a href="<?=$site_url?>/terms-of-service.php"> TERMS OF SERVICE </a> |
          <a href="<?=$site_url?>/careers.php"> CAREERS </a> <br>
          <a href="<?php echo $site_url ?>" data-toggle="modal" data-target="#myModal"> CONTACT US </a>
        </div>
        <button type="button" id="resbtn1" class="btn btn-success" style="display:none;"></button>
        <button type="button" id="resbtn2" class="btn btn-success" style="display:none;"></button>
        <button type="button" id="resbtn3" class="btn btn-success" style="display:none;"></button>
      </div>
      <?php include('../contact.php'); ?>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->

  <!-- jQuery -->
  <script src="<?php echo $site_url ?>/plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
  <script src="<?php echo $site_url ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo $site_url ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="<?php echo $site_url ?>/plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="<?php echo $site_url ?>/plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="<?php echo $site_url ?>/plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="<?php echo $site_url ?>/plugins/jqvmap/maps/jquery.vmap.world.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?php echo $site_url ?>/plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="<?php echo $site_url ?>/plugins/moment/moment.min.js"></script>
  <script src="<?php echo $site_url ?>/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?php echo $site_url ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="<?php echo $site_url ?>/plugins/summernote/summernote-bs4.min.js"></script>
  <script src="<?php echo $site_url ?>/plugins/jsgrid/demos/db.js"></script>
  <script src="<?php echo $site_url ?>/plugins/jsgrid/jsgrid.min.js"></script>

  <!-- overlayScrollbars -->
  <script src="<?php echo $site_url ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo $site_url ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo $site_url ?>/dist/js/adminlte.js"></script>
  <!-- SweetAlert2 -->
  <script src="<?php echo $site_url ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
  <!-- Toastr -->
  <script src="<?php echo $site_url ?>/plugins/toastr/toastr.min.js"></script>

  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo $site_url ?>/dist/js/demo.js"></script>
  <script src="<?php echo $site_url ?>/js/custom.js"></script>
  <script src="<?php echo $site_url ?>/js/contact.js"></script>
 </body>
 </html>
