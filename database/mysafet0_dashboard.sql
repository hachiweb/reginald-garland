-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2020 at 10:17 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mysafet0_dashboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `company` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `submitted_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `course_title` text NOT NULL,
  `course_date` date NOT NULL,
  `course_desc` text NOT NULL,
  `start_duration` text NOT NULL,
  `end_duration` text NOT NULL,
  `number_of_student` text NOT NULL,
  `city` text NOT NULL,
  `province_state` text NOT NULL,
  `postal_zip` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `course_title`, `course_date`, `course_desc`, `start_duration`, `end_duration`, `number_of_student`, `city`, `province_state`, `postal_zip`, `created_at`, `updated_at`) VALUES
(1, 'PHP', '2020-01-11', 'fdg', '01:00', '04:00', '10', 'df', 'Yukon', '533', '2020-01-10 19:57:52', '2020-01-10 19:57:52'),
(3, 'Adobe Flash', '2020-01-23', 'fkhj h jgjg gh gjg jg jg', '00:00', '07:00', '20', 'cityyy', 'Quebec', '889 890', '2020-01-11 16:24:47', '2020-01-11 16:24:47'),
(4, 'Adobe Illustrator', '2020-01-01', 'hjflg hlfvhflhjrlgbfjlblkfb hjhglghf', '19:00', '21:00', '10', 'tirunelveli', 'Yukon', '787 866', '2020-01-12 17:45:59', '2020-01-12 17:45:59');

-- --------------------------------------------------------

--
-- Table structure for table `instructor_record`
--

CREATE TABLE `instructor_record` (
  `id` int(11) NOT NULL,
  `courses` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `username` text NOT NULL,
  `address` text NOT NULL,
  `city` text NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructor_record`
--

INSERT INTO `instructor_record` (`id`, `courses`, `user_id`, `first_name`, `last_name`, `username`, `address`, `city`, `postal_code`, `created_at`, `updated_at`) VALUES
(10, 'PHP,Adobe Flash', 14, 'karthika', 'SG', 'karthi123@gmail.com', 'g', 'ghb', '789 797', '2020-01-13 14:55:11', '2020-01-13 14:55:11'),
(11, 'PHP,Adobe Flash,Adobe Illustrator', 15, 'j', 'jk', 'rohi', 'hjh', 'hjk', '786 866', '2020-01-13 18:57:17', '2020-01-13 18:57:17');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plan_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plan_price` float(10,2) NOT NULL,
  `paid_amount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` bigint(20) NOT NULL,
  `card_exp_month` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `card_exp_year` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(6) NOT NULL,
  `txnid` varchar(20) NOT NULL,
  `payment_amount` decimal(7,2) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `itemid` varchar(25) NOT NULL,
  `createdtime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `txnid`, `payment_amount`, `payment_status`, `itemid`, `createdtime`) VALUES
(1, '9YL373244F6611442', '5.00', 'Pending', '', '2019-12-02 12:28:24'),
(2, '2KX48593J9817171C', '5.00', 'Pending', '', '2019-12-02 12:37:08'),
(3, '3U996582PV194735V', '220.00', 'Pending', '123456', '2019-12-06 10:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `student_record`
--

CREATE TABLE `student_record` (
  `id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `middle_name` text NOT NULL,
  `last_name` text NOT NULL,
  `year_of_birth` text NOT NULL,
  `student_sr_no` int(11) NOT NULL,
  `mol_learner_id_no` text NOT NULL,
  `home_addr` text NOT NULL,
  `city` text NOT NULL,
  `postal_code` text NOT NULL,
  `phone_no` text NOT NULL,
  `email` text NOT NULL,
  `employer_name` text NOT NULL,
  `employer_contact` text NOT NULL,
  `practical_skills_achieved` text NOT NULL,
  `theory_test_score` float NOT NULL,
  `training_record` text NOT NULL,
  `trainer_certification` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `password` varchar(80) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'Agent',
  `filename` varchar(200) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `verify` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `contact`, `password`, `role`, `filename`, `admin_id`, `is_active`, `verify`, `created_at`, `updated_at`) VALUES
(1, 'S.', 'Sanja', 'su@mysafetyfirst.net', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Super Admin', '', 0, 1, '', '2019-08-15 22:51:56', '2019-10-10 15:17:38'),
(3, 'S.', 'Sanja', 'admin@mysafetyfirst.net', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', 'mewithprofessionallook.PNG', 0, 1, '', '2019-08-15 22:51:56', '2019-10-21 02:16:19'),
(4, 'Mariko', 'Mcfarland', 'instructor@mysafetyfirst.net', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Instructor', '', 3, 1, '', '2019-10-10 15:01:40', '2019-10-10 15:18:49'),
(13, 'vidya', 'raaj', 'vidyaraaj', '', '12345', 'Instructor', '', 0, 1, '', '2020-01-12 23:18:40', '2020-01-12 23:18:40'),
(14, 'karthika', 'SG', 'karthi123@gmail.com', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Instructor', '', 0, 1, '', '2020-01-13 20:25:11', '2020-01-13 20:25:11'),
(15, 'j', 'jk', 'rohi', '', '12345', 'Instructor', '', 0, 1, '', '2020-01-14 00:27:17', '2020-01-14 00:27:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor_record`
--
ALTER TABLE `instructor_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_record`
--
ALTER TABLE `student_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `instructor_record`
--
ALTER TABLE `instructor_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student_record`
--
ALTER TABLE `student_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
