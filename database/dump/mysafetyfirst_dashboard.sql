-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2020 at 03:24 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mysafetyfirst_dashboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `certification`
--

CREATE TABLE `certification` (
  `id` int(11) NOT NULL,
  `code` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certification`
--

INSERT INTO `certification` (`id`, `code`, `title`, `description`, `created_at`, `updated_at`) VALUES
(4, 'Dolore est pariatur', 'Cum culpa veritatis ', 'Mollitia totam perfe', '2020-01-21 09:08:39', '2020-01-21 09:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `company` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `submitted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `instructor_id` bigint(20) NOT NULL,
  `course_title` text NOT NULL,
  `course_date` date NOT NULL,
  `course_desc` text NOT NULL,
  `start_duration` text NOT NULL,
  `end_duration` text NOT NULL,
  `number_of_student` text NOT NULL,
  `city` text NOT NULL,
  `province_state` text NOT NULL,
  `postal_zip` text NOT NULL,
  `is_locked` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `instructor_id`, `course_title`, `course_date`, `course_desc`, `start_duration`, `end_duration`, `number_of_student`, `city`, `province_state`, `postal_zip`, `is_locked`, `created_at`, `updated_at`) VALUES
(29, 34, 'Cum culpa veritatis ', '1997-08-04', '', '', '', '25', 'Quaerat vel eiusmod ', 'NT', '50449', 0, '2020-01-21 09:44:03', '2020-01-21 09:44:03');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `email` text NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `instructor_record`
--

CREATE TABLE `instructor_record` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `courses` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `username` text NOT NULL,
  `address` text NOT NULL,
  `city` text NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructor_record`
--

INSERT INTO `instructor_record` (`id`, `user_id`, `courses`, `first_name`, `last_name`, `username`, `address`, `city`, `postal_code`, `created_at`, `updated_at`) VALUES
(22, 34, '4', 'Blake', 'Orr', 'vipegopiw', 'Earum et ad labore l', 'Excepturi harum omni', 'Mollit ', '2020-01-21 09:43:29', '2020-01-21 09:43:29');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plan_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plan_price` float(10,2) NOT NULL,
  `paid_amount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` bigint(20) NOT NULL,
  `card_exp_month` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `card_exp_year` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(6) NOT NULL,
  `txnid` varchar(20) NOT NULL,
  `payment_amount` decimal(7,2) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `itemid` varchar(25) NOT NULL,
  `createdtime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `txnid`, `payment_amount`, `payment_status`, `itemid`, `createdtime`) VALUES
(1, '9YL373244F6611442', '5.00', 'Pending', '', '2019-12-02 12:28:24'),
(2, '2KX48593J9817171C', '5.00', 'Pending', '', '2019-12-02 12:37:08'),
(3, '3U996582PV194735V', '220.00', 'Pending', '123456', '2019-12-06 10:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `student_record`
--

CREATE TABLE `student_record` (
  `id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `middle_name` text NOT NULL,
  `last_name` text NOT NULL,
  `year_of_birth` text NOT NULL,
  `student_sr_no` int(11) NOT NULL,
  `mol_learner_id_no` text NOT NULL,
  `home_addr` text NOT NULL,
  `city` text NOT NULL,
  `postal_code` text NOT NULL,
  `phone_no` text NOT NULL,
  `email` text NOT NULL,
  `employer_name` text NOT NULL,
  `employer_contact` text NOT NULL,
  `practical_skills_achieved` text NOT NULL,
  `theory_test_score` text NOT NULL,
  `training_record` text NOT NULL,
  `trainer_certification` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_record`
--

INSERT INTO `student_record` (`id`, `instructor_id`, `course_id`, `batch`, `first_name`, `middle_name`, `last_name`, `year_of_birth`, `student_sr_no`, `mol_learner_id_no`, `home_addr`, `city`, `postal_code`, `phone_no`, `email`, `employer_name`, `employer_contact`, `practical_skills_achieved`, `theory_test_score`, `training_record`, `trainer_certification`, `created_at`, `updated_at`) VALUES
(76, 34, 29, 1877407348, 'Stephen', 'Cynthia Hurley', 'Wilkerson', '2001', 455, '300', 'Voluptate numquam ul', 'Iusto exercitation m', 'Iste ve', '+14429781952', 'jomuq@mailinator.com', 'Mira Rhodes', 'Mira Rhodes', 'No', '45', '', '', '2020-01-21 09:45:19', '2020-01-21 09:45:19'),
(77, 34, 29, 1877407348, 'Maile', 'Mia Buckner', 'James', '1980', 985, '693', 'Adipisicing voluptas', 'Et ad hic et id magn', 'Veniam dolore sint', '+1 (2810) 956-81057', 'huhyfow@mailinator.com', 'Calista Jefferson', 'Calista Jefferson', 'Yes', '100', '', '', '2020-01-21 09:45:19', '2020-01-21 09:45:19'),
(78, 34, 29, 1877407348, 'Amir', 'Cally Andrews', 'Mejia', '2014', 589, '673', 'Consectetur aut reru', 'Nobis perferendis ea', 'Voluptas ex voluptat', '+1 (104) 166-5164', 'gidex@mailinator.com', 'Maile Bray', 'Maile Bray', 'Yes', '56', '', '', '2020-01-21 09:45:19', '2020-01-21 09:45:19'),
(79, 34, 29, 1877407348, 'Angelica', 'Elton Roberts', 'Farmer', '1986', 718, '157', 'Fugit eum consectet', 'Est officia officia ', 'Cillum ducimus et e', '+1 (779) 217-86710', 'seganicik@mailinator.net', 'Beck Knapp', 'Beck Knapp', 'No', '64', '', '', '2020-01-21 09:45:19', '2020-01-21 09:45:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `password` varchar(80) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'Agent',
  `filename` varchar(200) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `verify` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `contact`, `password`, `role`, `filename`, `admin_id`, `is_active`, `verify`, `created_at`, `updated_at`) VALUES
(1, 'S.', 'Sanja', 'su@mysafetyfirst.net', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Super Admin', '', 0, 1, '', '2019-08-15 22:51:56', '2019-10-10 15:17:38'),
(3, 'S.', 'Sanja', 'admin@mysafetyfirst.net', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', 'mewithprofessionallook.PNG', 0, 1, '', '2019-08-15 22:51:56', '2019-10-21 02:16:19'),
(34, 'Blake', 'Orr', 'vipegopiw', 'NA', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 'Instructor', '', 3, 1, '1', '2020-01-21 15:13:29', '2020-01-21 15:13:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certification`
--
ALTER TABLE `certification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor_record`
--
ALTER TABLE `instructor_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_record`
--
ALTER TABLE `student_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certification`
--
ALTER TABLE `certification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `instructor_record`
--
ALTER TABLE `instructor_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `student_record`
--
ALTER TABLE `student_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
