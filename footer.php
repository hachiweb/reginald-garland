<footer class="main-footer text-center">
  <strong>Copyright &copy; <?= date('Y'); ?> <a href="https://gpsdealers.net">Garland Pro Solutions</a></strong>
  All rights reserved.
  <div class="d-none d-sm-inline-block">
    <b>"The road to success runs through GPS."</b> <span style="font-size:20px;">&reg;</span>
  </div>
  <div class="text-center">
    <a href="<?php echo $site_url ?>/about.php"> ABOUT US </a> |
    <a href="<?=$site_url?>/terms-of-service.php"> TERMS OF SERVICE </a> |
    <a href="<?=$site_url?>/careers.php"> CAREERS </a> |
    <a href="<?php echo $site_url ?>" data-toggle="modal" data-target="#myModal"> CONTACT US </a>
  </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo $site_url ?>/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script src="<?php echo $site_url ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo $site_url ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo $site_url ?>/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo $site_url ?>/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo $site_url ?>/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo $site_url ?>/plugins/jqvmap/maps/jquery.vmap.world.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo $site_url ?>/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo $site_url ?>/plugins/moment/moment.min.js"></script>
<script src="<?php echo $site_url ?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo $site_url ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo $site_url ?>/plugins/summernote/summernote-bs4.min.js"></script>
<script src="<?php echo $site_url ?>/plugins/jsgrid/demos/db.js"></script>
<script src="<?php echo $site_url ?>/plugins/jsgrid/jsgrid.min.js"></script>

<!-- overlayScrollbars -->
<script src="<?php echo $site_url ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- DataTables -->
<script src="<?php echo $site_url ?>/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo $site_url ?>/plugins/datatables/dataTables.bootstrap4.js"></script>
<!-- FastClick -->
<script src="<?php echo $site_url ?>/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $site_url ?>/dist/js/adminlte.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo $site_url ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo $site_url ?>/plugins/toastr/toastr.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo $site_url ?>/dist/js/demo.js"></script>
<script src="<?php echo $site_url ?>/js/custom.js"></script>
<script src="<?php echo $site_url ?>/js/include.js"></script>
<script src="<?php echo $site_url ?>/js/contact.js"></script>
<script src="<?php echo $site_url ?>/js/intlTelInput.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-157175321-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-157175321-1');



</script>
</body>
</html>