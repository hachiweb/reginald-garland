-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2020 at 07:36 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gps_dealer`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_goal`
--

CREATE TABLE `company_goal` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `i_set_lead` varchar(10) NOT NULL,
  `i_show_set` varchar(10) NOT NULL,
  `i_sold_set` varchar(10) NOT NULL,
  `i_sold_show` varchar(10) NOT NULL,
  `i_sold` varchar(10) NOT NULL,
  `p_set_lead` varchar(10) NOT NULL,
  `p_show_set` varchar(10) NOT NULL,
  `p_sold_set` varchar(10) NOT NULL,
  `p_sold_show` varchar(10) NOT NULL,
  `p_sold` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_goal`
--

INSERT INTO `company_goal` (`id`, `user_id`, `admin_id`, `i_set_lead`, `i_show_set`, `i_sold_set`, `i_sold_show`, `i_sold`, `p_set_lead`, `p_show_set`, `p_sold_set`, `p_sold_show`, `p_sold`) VALUES
(1, 0, 2, '3434', '676767', '1', '1', '1', '', '', '', '', ''),
(2, 0, 3, '40', '70', '70', '60', '50', '50', '50', '50', '50', '50');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `company` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `submitted_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `first_name`, `last_name`, `email`, `phone`, `company`, `title`, `comments`, `submitted_at`) VALUES
(1, '', '', '', '', '', '', '', '2020-01-24 06:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `lead_capture`
--

CREATE TABLE `lead_capture` (
  `id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `agent_name` varchar(80) NOT NULL,
  `name` varchar(50) NOT NULL,
  `appt_set` varchar(80) DEFAULT NULL,
  `appt_show` varchar(50) DEFAULT NULL,
  `appt_sold` varchar(50) DEFAULT NULL,
  `filled_out` varchar(50) DEFAULT NULL,
  `medium` varchar(50) NOT NULL,
  `source` varchar(50) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `verification` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_capture`
--

INSERT INTO `lead_capture` (`id`, `agent_id`, `admin_id`, `agent_name`, `name`, `appt_set`, `appt_show`, `appt_sold`, `filled_out`, `medium`, `source`, `notes`, `status`, `verification`, `created_at`, `updated_at`) VALUES
(1, 4, 3, 'Mariko Mcfarland', 'Bob Johnson', 'SET', 'SHOW', '', NULL, 'Internet UPS', 'American Honda', NULL, 'Cold', 'approved', '2019-12-05 18:42:55', '2019-12-05 18:42:55'),
(2, 4, 3, 'Mariko Mcfarland', 'Dixie Chick', '', '', '', NULL, 'Internet UPS', 'Car Fax', NULL, 'Flaky', 'approved', '2019-12-05 18:46:30', '2019-12-05 18:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `date_time` text NOT NULL,
  `message` text NOT NULL,
  `date_set` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `admin_id`, `title`, `date_time`, `message`, `date_set`) VALUES
(38, 0, 'Totam anim et molest', '2020-01-01 08:41', 'Sit dolore providen', '2020-01-01'),
(70, 0, 'Deserunt obcaecati egfgfgsfdgsfhgfh', '1986-11-02 01:25', 'Modi dicta est illu', '1986-11-02'),
(202, 3, 'Vel omnis ut et reru', '2020-01-28 10:51', 'Rem magnam quo dolor', '2020-01-28'),
(203, 3, 'Autem totam ad nisi ', '2020-01-28 10:55', 'Voluptas vitae possi', '2020-01-28'),
(210, 3, 'Harum ea tempore la', '2020-01-28 11:11', 'Dolore ex eum molest', '2020-01-28'),
(211, 3, 'vfcvv', '2020-01-28 11:29', 'vzvc', '2020-01-28'),
(212, 3, 'Libero et aspernatur', '2020-01-28 11:30', 'Do eu fugiat cillum', '2020-01-28'),
(213, 3, 'dfdf', '2020-01-28 11:30', 'adfa', '2020-01-28'),
(214, 3, 'Consequatur Sit of', '1997-08-25 02:34', 'Velit perferendis e', '1997-08-25');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plan_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plan_price` float(10,2) NOT NULL,
  `paid_amount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` bigint(20) NOT NULL,
  `card_exp_month` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `card_exp_year` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(6) NOT NULL,
  `txnid` varchar(20) NOT NULL,
  `payment_amount` decimal(7,2) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `itemid` varchar(25) NOT NULL,
  `createdtime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `txnid`, `payment_amount`, `payment_status`, `itemid`, `createdtime`) VALUES
(1, '9YL373244F6611442', '5.00', 'Pending', '', '2019-12-02 12:28:24'),
(2, '2KX48593J9817171C', '5.00', 'Pending', '', '2019-12-02 12:37:08'),
(3, '3U996582PV194735V', '220.00', 'Pending', '123456', '2019-12-06 10:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `source`
--

CREATE TABLE `source` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `source` varchar(50) NOT NULL,
  `set_code` varchar(50) NOT NULL,
  `show_code` varchar(50) NOT NULL,
  `sold_code` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `source`
--

INSERT INTO `source` (`id`, `admin_id`, `source`, `set_code`, `show_code`, `sold_code`, `created_at`, `updated_at`) VALUES
(1, 3, 'Car Gurus', 'B1', 'B2', 'B3', '2019-10-08 00:05:07', '2019-10-08 00:05:07'),
(2, 3, 'Car Fax', 'C1', 'C2', 'C3', '2019-10-08 00:05:26', '2019-10-08 00:05:26'),
(3, 3, 'Dealer.com', 'D1', 'D2', 'D3', '2019-10-08 00:05:41', '2019-10-08 00:05:41'),
(7, 3, 'Quost Traders', 'Q1', 'Q2', 'Q3', '2019-10-17 09:23:04', '2019-10-17 09:23:04'),
(9, 3, 'American Honda', 'A1', 'A2', 'A3', '2019-10-21 02:19:03', '2019-10-21 02:19:03'),
(10, 3, 'Edmunds.com', 'E1', 'E2', 'E3', '2019-10-21 02:20:35', '2019-10-21 02:20:35'),
(11, 3, 'Facebook Marketplace', 'F1', 'F2', 'F3', '2019-10-21 02:20:53', '2019-10-21 02:20:53'),
(12, 3, 'Google.com', 'G1', 'G2', 'G3', '2019-10-21 02:21:06', '2019-10-21 02:21:06'),
(13, 3, 'Molestiae iure conse', 'K1', 'K2', 'K', '2019-10-22 08:48:08', '2019-10-22 08:48:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `device_token` text DEFAULT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `password` varchar(80) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'Agent',
  `filename` varchar(200) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `paid` tinyint(4) NOT NULL DEFAULT 0,
  `subscription_expired` int(11) NOT NULL DEFAULT 0,
  `subscription` datetime NOT NULL,
  `varify` varchar(500) NOT NULL,
  `is_block` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `device_token`, `first_name`, `last_name`, `username`, `contact`, `password`, `role`, `filename`, `admin_id`, `is_active`, `paid`, `subscription_expired`, `subscription`, `varify`, `is_block`, `created_at`, `updated_at`) VALUES
(1, '', 'Garland Pro', 'Solutions', 'su@gps.lan', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Super Admin', '', 0, 1, 0, 0, '2019-10-10 00:00:00', '', 0, '2019-08-15 22:51:56', '2019-10-10 15:17:38'),
(2, '', 'Mr. Ronald', 'Jackson, Sr.', 'su@gps.lan', '', '25f9e794323b453885f5181f1b624d0b', 'Super Admin', '', 0, 1, 0, 0, '2019-10-10 00:00:00', '', 0, '2019-08-15 22:51:56', '2019-10-18 03:55:19'),
(3, '', 'Garland Pro', 'Solutions', 'admin@gps.lan', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', 'mewithprofessionallook.PNG', 0, 1, 1, 0, '2020-12-31 00:00:00', '', 0, '2019-08-15 22:51:56', '2019-10-21 02:16:19'),
(4, ',6c9a9ebb-ca95-4798-9a16-75ef3f888065  ,,6c9a9ebb-ca95-4798-9a16-75ef3f888065,430943f2-e3dc-4aae-8151-b6353fdd7306,6c9a9ebb-ca95-4798-9a16-75ef3f888065', 'Mariko', 'Mcfarland', 'agent@gps.lan', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Agent', '', 3, 1, 0, 0, '2019-10-10 00:00:00', '', 0, '2019-10-10 15:01:40', '2019-10-10 15:18:49'),
(5, '', 'rayyan', 'qaiser', 'fauzanofficial74@gmail.com', '+60 01137169616', 'dca00d665baf8b0e12590010fa90b800', 'Admin', '', 0, 1, 0, 1, '2019-12-01 00:00:00', '', 0, '2019-10-30 23:18:06', '2019-10-30 23:23:38'),
(6, '', 'test', 'demo', 'testdemo@gmail.com', '+1 87451166655755', 'f925916e2754e5e03f75dd58a5733251', 'Admin', 'domestic.jpg', 0, 1, 0, 1, '2019-12-20 00:00:00', 'cHQKgpcPEuDB5K3eDEL0Os2INhtz6jYkPKygF1y4K4jXDB60yo', 0, '2019-11-19 07:34:01', '2019-11-19 07:34:01'),
(7, '', 'Kannan', 'Arumugavel', 'kannan.aru05@gmail.com', '+91 09976035193', '9511b386fb0c5a7f69f77c880de30ae0', 'Admin', '', 0, 1, 0, 1, '2019-12-20 00:00:00', '', 0, '2019-11-19 10:15:20', '2019-11-19 10:33:15'),
(8, '', 'Hello', 'Hello', 'amanbatra1996@gmail.com', '+91 7986050411', '8dd43ae0638e1ce2690e2e3cfa653923', 'Admin', 'adfad.jpg', 0, 1, 1, 1, '0000-00-00 00:00:00', '', 0, '2019-11-24 07:00:37', '2019-11-24 07:04:53'),
(9, '', 'Benjamin', 'Ramos', 'fosaluk@mailinator.net', '+1 (924) 743-4184', 'a2413bcdde8c5dcfb4ef275cb7ac2c5c', 'Admin', '', 0, 1, 0, 1, '2019-12-27 00:00:00', '', 0, '2019-11-26 07:34:02', '2019-11-26 07:34:02'),
(10, '', 'Aman', 'Batra', 'aman.batra@hachiweb.com', '+1 ', '8dd43ae0638e1ce2690e2e3cfa653923', 'Admin', '', 0, 1, 0, 0, '2020-01-30 00:00:00', '2alqR4nrM54xkeGN5IG3oLYkJz3lftruh09FawuRpwtpsjRamt', 0, '2019-12-29 22:31:20', '2019-12-29 22:31:20'),
(11, '', 'Aaron', 'Blankenship', 'dev@hachiweb.com', '', '0529af5107ee7b2ed7fab47265eee3c5', 'Agent', 'Screenshotfrom2019-12-2820-05-23.png', 3, 1, 0, 0, '0000-00-00 00:00:00', '', 0, '2019-12-30 21:41:28', '2019-12-30 21:41:28'),
(12, '', 'no', 'onon', 'addD@mm.com', '+1 201555201210', '326ab1fc75f23b43d43d3ed178dc2630', 'Admin', '', 0, 1, 0, 0, '2020-02-19 00:00:00', 'IROD5zbQpsfhrDcth6ou9EPZJouAlGESK7xLbybrlXlB9NV2a5', 0, '2020-01-19 11:50:46', '2020-01-19 11:50:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company_goal`
--
ALTER TABLE `company_goal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_capture`
--
ALTER TABLE `lead_capture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `source`
--
ALTER TABLE `source`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company_goal`
--
ALTER TABLE `company_goal`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lead_capture`
--
ALTER TABLE `lead_capture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `source`
--
ALTER TABLE `source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
