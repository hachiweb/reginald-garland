<?php
session_start();
include "dbconfig.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';
$mail = new PHPMailer;
$username = $_POST['username'];
if ($username != ""){
    $sql_query = "SELECT * FROM users WHERE username='$username' AND `role` = 'Admin'";
    $result = mysqli_query($con,$sql_query);
    $row = mysqli_fetch_array($result);
    $first_name   = $row['first_name'];
    $last_name    = $row['last_name'];
    $emailname = $first_name." ".$last_name;
    $otp = rand(1000,9999);
    if($result->num_rows>0){
        $mail->Host     = 'hachiweb.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'test@hachiweb.com';
        $mail->Password = 'hachi#123';
        $mail->SMTPSecure = 'tls';
        $mail->Port     = 587;
        $mail->setFrom('test@hachiweb.com', 'Garland Pro Solutions');
        // Add a recipient
        $mail->addAddress($username);
        // Email subject
        $mail->Subject = "Garland Pro Solutions";
        // Set email format to HTML
        $mail->isHTML(true);
        // Email body content
        $mailContent = '<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <title>Document</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        
            <!-- jQuery library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        
            <!-- Popper JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        
            <!-- Latest compiled JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
            <style>
                .main {
                    background-color: #6f42c1;
                    border-radius: 8px;
                }
        
                .main span {
                    color: white;
                    font-size: 20px;
                }
        
                .main img {
                    width: 6%;
                    padding: 10px;
                    border-radius: 50%;
                }
        
                body {
                    background-color: #f8f9fa;
                }
        
                .main-body {
                    background-color: white;
                    border-radius: 8px;
                }
            </style>
        </head>
        
        <body>
            <section class="container mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main">
                            <img src="http://garlandprosolutions.hachistaging.com/dist/img/logo2.png" alt="customer relationship management system">
                            <span>Garland Pro Solutions</span>
                        </div>
                    </div>
                </div>
            </section>
            <section class="container mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="my-3 p-3 bg-white rounded box-shadow">
                            <h6 class="pb-2 mb-0">Dear '.$emailname.'</h6>
                            <p class="border-bottom border-gray pb-2 ml-4 mb-0">Garland Pro Solutions Reset Password.</p>
                            <div class="media text-muted pt-3">
                                <img data-src="holder.js/32x32?theme=thumb&amp;bg=007bff&amp;fg=007bff&amp;size=1" alt="customer experience management system" class="mr-2 rounded" style="width: 32px; height: 32px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16dd9234068%20text%20%7B%20fill%3A%23007bff%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16dd9234068%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23007bff%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.5390625%22%20y%3D%2216.9%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">@Username</strong> '.$username.'
                                </p>
                            </div>
                            <div class="media text-muted pt-3">
                                <img data-src="holder.js/32x32?theme=thumb&amp;bg=e83e8c&amp;fg=e83e8c&amp;size=1" alt="Lead Management Architecture System" class="mr-2 rounded" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16dd923406f%20text%20%7B%20fill%3A%23e83e8c%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16dd923406f%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23e83e8c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.5390625%22%20y%3D%2216.9%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 32px; height: 32px;">
                                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <strong class="d-block text-gray-dark">@OTP</strong>'.$otp.'
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </body>
        
        </html>';
        $mail->Body = $mailContent;
        // Send email
        if($mail->send()){
            $_SESSION['otp'] = $otp;
            $_SESSION['resetid'] = $row['id'];
            header('location: reset.php');
            die();
        }else{
            header('location: forgot.php?success=1&msg=Something went wrong!');
            die();
        }
        $con->close();
    }
    else{
        header('location: forgot.php?success=1&msg=Email is invalid!');
    die();
    }
}
else {
    header('location: forgot.php?success=1&msg=Email is invalid!');
    die();
}
?>
