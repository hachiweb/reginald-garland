<div class="modal fade" id="modal-sm">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="<?=$sidebarpath;?>"
                       alt="digital marketing automation and analytics software">
                </div>
                <h3 class="profile-username text-center"><?= $_SESSION['alais']?></h3>
                <p class="text-muted text-center"><?= $_SESSION['role']?></p>
                <form action="<?=$site_url?>/sub-profile.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleInputFile">Profile Photo</label>
                    <div class="input-group">
                    <div class="custom-file">
                      <input type="file" accept="image/*" name="Profile" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    </div>
                    <input type="hidden" name="currenturl" value="<?=$site_url.$currenturl;?>">
                    <input type="hidden" name="id" value="<?= $_SESSION['id']?>">
                  </div>
                  <button type="submit" class="btn btn-primary btn-block mb-3">Change Image</button>
                  </form>
                <a href="<?=$site_url?>/sub-profile.php?id=<?= $_SESSION['id']?>&currenturl=<?=$site_url.$currenturl;?>" class="btn btn-danger btn-block"><b>Remove Image</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>