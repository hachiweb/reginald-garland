<?php
$page = 'account';
include 'header.php';
if (!empty($_GET["D"])) {
  $newID = $_GET["D"];
  $role = "Admin";
  $sql = "SELECT * FROM `users` WHERE `id` = '$newID'";
}else {
  $role = $_SESSION["role"];
  $name = $_SESSION['alais'];
  $newID = $_SESSION['id'];
  $path = $sidebarpath;
  $sql = "SELECT * FROM `users` WHERE `id` = '$newID'";
}
$result = $con->query($sql);
$row = $result->fetch_assoc();
if (!empty($_GET["D"])) {
    $name = $row['first_name']." ".$row['last_name'];
    $pic = $row['filename'];
    if (!empty($pic)) {
      $path = $site_url."/upload/admin/".$newID."/".$pic;
    } else {
        $path = $site_url."/dist/img/avatar.png";
    }
}
if ($row['paid'] == "1") {
  $paid = "Paid Version";
} elseif ($row['paid'] == "0" || $row['subscription'] == "0") {
  $paid = "Trail Version";
} else {
  $paid = "Not Paid";
}
$today = time();
$subscription = strtotime($row['subscription']);
$datediff = $subscription - $today;
$expire = round($datediff / (60 * 60 * 24));
if ($expire < 1) {
  $expire = "Expired";
} else {
  $expire = $expire . " days remaining";
}
$renewal_date = date('d-m-Y', strtotime($row['subscription']));
$renewal_date = 'Next renewal date: ' . $renewal_date;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
              <li class="breadcrumb-item active">My Account</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row <?php if ($role != "Admin") { echo"justify-content-center";}?>">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="<?=$path;?>"
                       alt="critical connectivity facilitates business profitability">
                </div>

                <h3 class="profile-username text-center"><?=$name?></h3>

                <p class="text-muted text-center"><?=$role?></p>
                <?php if (empty($_GET["D"])) {?>
                <form action="<?=$site_url?>/sub-profile.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleInputFile">Profile Photo</label>
                    <div class="input-group">
                    <div class="custom-file">
                      <input type="file" accept="image/*" name="Profile" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    </div>
                    <input type="hidden" name="currenturl" value="<?=$site_url.$currenturl;?>">
                    <input type="hidden" name="id" value="<?=$newID?>">
                  </div>
                  <button type="submit" class="btn btn-primary btn-block mb-3">Change Image</button>
                  </form>
                <a href="<?=$site_url?>/sub-profile.php?id=<?=$newID?>&currenturl=<?=$site_url.$currenturl;?>" class="btn btn-danger btn-block"><b>Remove Image</b></a>
                <?php }if (!empty($_GET["D"])) {
                  $hash = $row['password'];
                  $hash_type = "md5";
                  $email = "test@hachiweb.com";
                  $code = "0b31d71621b7a0cc";
                  $response = file_get_contents("https://md5decrypt.net/en/Api/api.php?hash=".$hash."&hash_type=".$hash_type."&email=".$email."&code=".$code);
                  ?>
                  <div class="form-group text-center">
                  <input type="hidden" id="pashow" value="<?=$response?>">
                    <label for="Password" class="control-label">Password</label>
                    <h5 id="newps">************</h5>
                    <a href="" class="spasc"><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </div>
                <?php }?>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-5">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <form class="form-horizontal" action="sub-myaccount.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputFName" class="col-sm-4 control-label">First Name</label>

                    <div class="col-sm-10">
                      <input required type="text" class="form-control" id="inputFName" name="first_name" value="<?=$row['first_name'];?>" placeholder="First Name" <?php if (!empty($_GET["D"])) { echo "disabled";}?>>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputLName" class="col-sm-4 control-label">Last Name</label>

                    <div class="col-sm-10">
                      <input required type="text" class="form-control" id="inputLName" name="last_name" value="<?=$row['last_name'];?>" placeholder="Last Name" <?php if (!empty($_GET["D"])) { echo "disabled";}?>>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Email</label>

                    <div class="col-sm-10">
                      <input required type="email" class="form-control" id="inputEmail" name="username" value="<?=$row['username'];?>" placeholder="Email" <?php if (!empty($_GET["D"])) { echo "disabled";}?>>
                    </div>
                  </div>
                  <?php if ($role == "Admin") {?>
                  <div class="form-group">
                    <label for="inputContact" class="col-sm-4 control-label">Contact No</label>

                    <div class="col-sm-10">
                    <input required type="text" class="form-control" id="inputContact" name="Contact" value="<?=$row['contact'];?>" placeholder="Contact No" <?php if (!empty($_GET["D"])) { echo "disabled";}?>>
                    </div>
                  </div>
                  <?php } ?>
                  <input type="hidden" name="id" value="<?=$newID?>">
                  <?php if (empty($_GET["D"])) {?>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                  <?php }?>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <?php if ($role == "Admin") {?>
          <div class="col-md-4">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Subscription</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-money-check-alt mr-1"></i> Paid</strong>

                <p class="text-muted">
                <?=$paid; ?>
                </p>

                <hr>

                <strong><i class="fas fa-wallet mr-1"></i> Subscription</strong>

                <p class="text-muted"><?=$expire; ?><br/>
                <?=$renewal_date; ?></p>

                <hr>
                <p></p>
                <strong><i class="fas fa-cube mr-1"></i> Packs</strong>

                <p class="text-muted">
                This is a license for access to Garland Pro Solutions services
                </p>
                <form action="payment/buy.php" method="post" >
                <div class="custom-control custom-radio custom-control-inline">
                  <input class="custom-control-input" type="radio" id="plan1" value="1" name="plan">
                  <label for="plan1" class="custom-control-label">Month ($20)</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input class="custom-control-input" type="radio" id="plan2" value="2" name="plan" checked>
                  <label for="plan2" class="custom-control-label">Year  ($220)</label>
                </div>
                <button type="submit" class="btn plan_buy btn-block btn-success mt-2">Buy</button>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <?php } ?>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include 'footer.php';?>

  <!-- <script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery(".plan_buy").click(function(){
            var radioValue = jQuery("input[name='plan']:checked").val();
            if(radioValue == 1){
                price = 20;
            } else {
                price = 220;
            }
            jQuery.ajax({
                type: "POST",
                url: 'cookieplan.php',
                data: {plan: price},
                success: function(response){
                }
            });
        }); 
    });
</script> -->