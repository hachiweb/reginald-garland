<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="<?php echo $site_url ?>" class="brand-link">
    <img src="<?php echo $site_url ?>/dist/img/logo2.png" alt="Garland Pro Solutions Logo" class="brand-image img-circle elevation-3"
    style="opacity: .8">
    <span class="brand-text font-weight-light">Garland Pro <br>Solutions </span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?=$sidebarpath;?>" class="img-circle elevation-2" data-toggle="modal" data-target="#modal-sm" style="cursor:pointer;" alt="garland pro solution lead management system">
      </div>
      <div class="info">
        <a href="<?php echo $site_url ?>/myaccount.php" class="d-block" ><?= $_SESSION['alais']?></a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <?php if ($_SESSION["role"] == "Super Admin") {
                ?>
                <li class="nav-item">
                <a href="<?php echo $site_url ?>" class="nav-link <?php if($page == 'home'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon text-yellow"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/view-admin.php" class="nav-link <?php if($page == 'view-admin'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon text-light"></i>
                  <p>All Customer</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/forms/register-admin.php" class="nav-link <?php if($page == 'register-admin'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon text-orange"></i>
                  <p>Register Customer</p>
                </a>
              </li>
                <?php
              }
              else{
              ?>
           <li class="nav-item has-treeview">
           <!-- SEARCH FORM -->
            <form action="<?php echo $site_url ?>/pages/tables/search-result.php" method="post" class="form-inline ml-3">
              <div class="border mb-1">
              <div class="input-group input-group-sm">
              <input type="hidden" name="LOC" value="HED">
                <input class="form-control form-control-navbar bg-dark" name="TERM" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                  <button class="btn btn-navbar border bg-dark" type="submit" style="border-radius:0px;">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
              </div>
            </form>
           </li>
           
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if($page == ''){echo 'active';} ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $site_url ?>" class="nav-link <?php if($page == 'home'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Forms
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <?php
                if ($_SESSION["role"] !== "Super Admin") {
                  ?>
                  <li class="nav-item">
                    <a href="<?php echo $site_url ?>/pages/forms/agent-forms.php" class="nav-link <?php if($page == 'agent-form'){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Add Customer</p>
                    </a>
                  </li>
                  <?php
                }
              if ($_SESSION["role"] == "Admin") {
                ?>
                <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/forms/register-agent.php" class="nav-link <?php if($page == 'register-agent'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Register Agent</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/forms/create-source.php" class="nav-link <?php if($page == 'create-source'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create Source</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/forms/set-company-goal.php" class="nav-link <?php if($page == 'set-company-goal'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Company Goals</p>
                </a>
              </li>
                <?php
              }
              ?>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
              Performance
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <?php if ($_SESSION["role"] == "Admin") { ?>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/agent-table.php" class="nav-link <?php if($page == 'agent-table'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Total Team Performance</p>
                </a>
              </li>
              <?php
              } if ($_SESSION["role"] == "Agent") {
              ?><li class="nav-item">
              <a href="<?php echo $site_url ?>/pages/tables/agent-table.php" class="nav-link <?php if($page == 'agent-table'){echo 'active';} ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>My Performance</p>
              </a>
              </li>
              <?php
              }
              ?>
              <li class="nav-item">        
              <?php if ($_SESSION["role"] == "Admin") {
                ?>
              <a href="<?php echo $site_url ?>/pages/tables/multi-table.php" class="nav-link <?php if($page == 'multi-table'){echo 'active';} ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Agent Performance</p>
              </a>
                <?php
              } if ($_SESSION["role"] == "Agent") {
              ?>
              <!-- <a href="<?php echo $site_url ?>/pages/tables/multi-table.php?agent_id=<?=$_SESSION['id']?>&agent_name=<?=$_SESSION['alais']?>" class="nav-link <?php if($page == 'multi-table'){echo 'active';} ?>"> -->
              <!-- <i class="far fa-circle nav-icon"></i>
              <p>Total Performance</p>
              </a> -->
              <?php
              }
              ?>
              </li>
              <?php
              if ($_SESSION["role"] == "Admin") {
                ?>
               <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/source-codes.php" class="nav-link <?php if($page == 'source-code'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Total Lead Stats</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/group-table.php" class="nav-link <?php if($page == 'group-table'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lead Source Legend</p>
                </a>
              </li>
              <?php if ($_SESSION["role"] == "Admin") { ?>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/bdc-table.php" class="nav-link <?php if($page == 'BDC-table'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BDC Performance</p>
                </a>
              </li>
              <?php } ?>
              <?php /*
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/service-mining-table.php" class="nav-link <?php if($page == 'Service-Mining-table'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Service Mining Table</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/lead-requests-table.php" class="nav-link <?php if($page == 'lead requests'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lead Requests</p>
                </a>
              </li>
            
                <?php */
              }
              ?>
                <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/search.php" class="nav-link <?php if($page == 'search'){echo 'active';} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Search</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-bell"></i>
              <p>Notification System
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/forms/notification.php" class="nav-link <?php if($page == 'create'){echo 'active';} ?>">
                <i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;
                  <p>Create Notification</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/view-notification.php" class="nav-link <?php if($page == 'view'){echo 'active';} ?>">
                <i class="far fa-eye"></i>&nbsp;&nbsp;
                  <p>View Notification</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/past-notification.php" class="nav-link <?php if($page == 'past'){echo 'active';} ?>">
                <i class="fas fa-history"></i>&nbsp;&nbsp;
                  <p>Past Notificatons</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-sticky-note"></i>
              <p>Notes
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $site_url ?>/pages/tables/view-notes.php" class="nav-link <?php if($page == 'create'){echo 'active';} ?>">
                <i class="far fa-eye"></i>&nbsp;&nbsp;
                  <p>View Notes</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
              Coming Soon
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $site_url ?>" class="nav-link">
                <i class="fab fa-servicestack"></i>&nbsp;&nbsp;
                  <p>Service Mining Tracker</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>" class="nav-link">
                <i class="far fa-map"></i>&nbsp;&nbsp;
                  <p>GPS On-Boarding</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>" class="nav-link">
                <i class="fas fa-chalkboard-teacher"></i>&nbsp;&nbsp;
                  <p>E-Learning Certification</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $site_url ?>" class="nav-link">
                <i class="fas fa-business-time"></i>&nbsp;&nbsp;
                  <p>Using GPS For More Business</p>
                </a>
              </li>
            </ul>
          </li>
          <?php
              }
              ?>
          <li class="nav-item">
            <a href="<?=$site_url?>/about.php" class="nav-link <?php if($page == 'about'){echo 'active';} ?>">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">ABOUT US</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=$site_url?>/terms-of-service.php" class="nav-link <?php if($page == 'terms-of-service'){echo 'active';} ?>">
              <i class="nav-icon far fa-circle text-warning"></i>
              <p>TERMS OF SERVICE</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=$site_url?>/careers.php" class="nav-link <?php if($page == 'careers'){echo 'active';} ?>">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>CAREERS</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="" data-toggle="modal" data-target="#myModal" class="nav-link">
              <i class="nav-icon far fa-circle text-success"></i>
              <p>CONTACT US</p>
            </a>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>