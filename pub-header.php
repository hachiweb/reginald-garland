<?php
error_reporting(0);
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https://"; 
else
$link = "http://"; 
$site_url = $link.$_SERVER['HTTP_HOST'];
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Garland Pro Solutions | About</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/summernote/summernote-bs4.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/sweetalert2/sweetalert2.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jsgrid/jsgrid.min.css">
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jsgrid/jsgrid-theme.min.css">
  <!-- telephone number -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/css/intlTelInput.css">
  <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo $site_url ?>/css/custom.css">
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href=""><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?=$site_url?>" class="nav-link">Home</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="<?=$site_url?>/dist/img/logo2.png"
           class="brand-image img-circle elevation-3"
           style="opacity: .8" alt="customer relationship management">
      <span class="brand-text font-weight-light">Garland Pro <br>Solutions </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
            <a href="<?=$site_url?>/about.php" class="nav-link <?php if($page == 'about'){echo 'active';} ?>">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">ABOUT US</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=$site_url?>/terms-of-service.php" class="nav-link <?php if($page == 'terms-of-service'){echo 'active';} ?>">
              <i class="nav-icon far fa-circle text-warning"></i>
              <p>TERMS OF SERVICE</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=$site_url?>/careers.php" class="nav-link <?php if($page == 'careers'){echo 'active';} ?>">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>CAREERS</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="" data-toggle="modal" data-target="#myModal" class="nav-link">
              <i class="nav-icon far fa-circle text-success"></i>
              <p>CONTACT US</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <?php include('contact.php'); ?>