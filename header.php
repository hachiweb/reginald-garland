<?php
error_reporting(0);
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https://"; 
else
$link = "http://"; 
$site_url = $link.$_SERVER['HTTP_HOST'];
$currenturl = $_SERVER['REQUEST_URI'];
$urlfind = '?'; 
if (strpos($currenturl, $urlfind) == false) { 
  $currenturl = $_SERVER['REQUEST_URI'];
} 
else { 
  $currenturl = substr($currenturl, 0, strpos($currenturl, "?"));
} 
if(!isset($_SESSION)){ 
  session_start(); 
}  
if(empty($_SESSION["username"])){
  header("location:/login");
  exit();
}
else{
  $alias = $_SESSION['alais'];  
  $role = $_SESSION['role'];
  $username = $_SESSION["username"];
  $userID = $_SESSION['id'];
  $adminID = $_SESSION['admin_id'];
}
$res = $_GET['res'];
include('dbconfig.php');
$exselsql = "SELECT * FROM `users` WHERE `role` = 'Admin'";
$exselresult = $con->query($exselsql);
while ($exselrow = $exselresult->fetch_assoc()) {
  $exid = $exselrow['id'];
  $exdtoday = time();
  $exdsubscription = strtotime($exselrow['subscription']);
  $exddatediff = $exdsubscription - $exdtoday;
  $exdexpire = round($exddatediff / (60 * 60 * 24));
  if ($exdexpire<1) {
    $exsql = "UPDATE `users` SET `subscription_expired` = '1' WHERE `id` = '$exid'";
    $exresult = $con->query($exsql);
  } elseif ($exdexpire>0) {
    $exsql = "UPDATE `users` SET `subscription_expired` = '0' WHERE `id` = '$exid'";
    $exresult = $con->query($exsql);
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Garland Pro Solutions</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
 
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/summernote/summernote-bs4.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/sweetalert2/sweetalert2.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jsgrid/jsgrid.min.css">
  <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/jsgrid/jsgrid-theme.min.css">
  <!-- telephone number -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/css/intlTelInput.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $site_url ?>/css/custom.css">
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
  <!--  Initialize of Onesignal js script-->
  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
  
</head>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item" >
          <a id="openSideBar" class="nav-link" data-widget="pushmenu" href="#" ><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="<?php echo $site_url ?>" class="nav-link">Home</a>
        </li>
        <?php if ($_SESSION["role"] != "Super Admin") {?>
           <li class="nav-item d-none d-sm-inline-block">
           <a href="<?php echo $site_url ?>/pages/forms/agent-forms.php" class="nav-link"></a>
         </li>
        <?php }?>
        <!-- <li class="nav-item d-none d-sm-inline-block">
          <a href="" class="nav-link">Contact</a>
        </li> -->
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <!-- <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a> -->
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo $site_url ?>/dist/img/user1-128x128.jpg" alt="customer acquisition management" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo $site_url ?>/dist/img/user8-128x128.jpg" alt="marketing automation system" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    John Pierce
                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">I got your message bro</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo $site_url ?>/dist/img/user3-128x128.jpg" alt="lead management and is the basis of software such as marketing automation.

" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Nora Silvester
                    <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">The subject goes here</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li>
          <a class="nav-link" href="/logout/index.php">
            <i class="fas fa-sign-out-alt"> Logout </i>
          </a>
        </li>
        <?php 
          $sidebarid = $_SESSION['id'];
          $sql_query = "SELECT * FROM `users` WHERE `id` = '$sidebarid'";
          $result = mysqli_query($con,$sql_query);
          $row = mysqli_fetch_array($result);
          $sidebarpic = $row['filename'];
          if ($_SESSION["role"] == "Super Admin") {
            if (!empty($sidebarpic)) {
                $sidebarpath = $site_url."/upload/superadmin/".$sidebarpic;
              } else {
                  $sidebarpath = $site_url."/dist/img/avatar2.png";
              }
          }
          if ($_SESSION["role"] == "Admin") {
            if (!empty($sidebarpic)) {
                $sidebarpath = $site_url."/upload/admin/".$sidebarid."/".$sidebarpic;
              } else {
                  $sidebarpath = $site_url."/dist/img/avatar.png";
              }
          }
          if ($_SESSION["role"] == "Agent") {
              if (!empty($sidebarpic)) {
                  $sidebarpath = $site_url."/upload/agent/".$sidebarid."/".$sidebarpic;
              } else {
                  $sidebarpath = $site_url."/dist/img/avatar5.png";
              }
          }
          ?>
      </ul>
    </nav>
    <!-- /.navbar -->
    <?php include('contact.php'); ?>
    <?php include('profile.php'); ?>
    <?php include('sidebar-main.php'); ?>
    <input type="hidden" value="<?=$res?>" id="res">
    <button type="button" id="resbtn1" class="btn btn-success" style="display:none;"></button>
    <button type="button" id="resbtn2" class="btn btn-success" style="display:none;"></button>
    <button type="button" id="resbtn3" class="btn btn-success" style="display:none;"></button>
    <button type="button" id="resbtn4" class="btn btn-success" style="display:none;"></button>