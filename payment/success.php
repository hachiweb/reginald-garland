<?php 
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: ../index.php");
}
    $page = 'payment';
    include('../header.php');
    include('config.php');
?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Success</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Success</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <!-- left column -->
        <div class="col-md-8 mx-auto">
        <?php
          $planname = $_POST['JwjNzETp'];
          $planprice = $_POST['WGgu2PIo'];
          $currency = $_POST['UHpZnDsb'];
          $email = $_SESSION["username"];
          $payment_id = $statusMsg = '';
          $ordStatus = 'error';
          $useroid = $_SESSION["id"];
          $today = time();
          $fetchsql = "SELECT * FROM `users` WHERE `id` = '$useroid' AND `role` = 'Admin'";
          $fetchresult = $con->query($fetchsql);
          $fetchrow = $fetchresult->fetch_assoc();
          $subscription = strtotime($fetchrow['subscription']);
          $datediff = $subscription - $today;
          $expire = round($datediff / (60 * 60 * 24));
          $date = new DateTime(date("Y-m-d"));
          if ($expire<1) {
              if ($planprice==20) {
                  $date->modify('+31 day');
                  $subscription = $date->format('Y-m-d');
              }
              if ($planprice==220) {
                  $date->modify('+366 day');
                  $subscription = $date->format('Y-m-d');
              }
          } else {
              if ($planprice==20) {
                  $oldsub = $expire+31;
                  $date->modify($oldsub.' day');
                  $subscription = $date->format('Y-m-d');
              }
              if ($planprice==220) {
                  $oldsub = $expire+366;
                  $date->modify($oldsub.' day');
                  $subscription = $date->format('Y-m-d');
              }
          }

          // Check whether stripe token is not empty 
          if(!empty($_POST['stripeToken'])){ 
              
              // Retrieve stripe token, card and user info from the submitted form data 
              $token  = $_POST['stripeToken']; 
              $name = $_POST['name']; 
              $card_number = $_POST['card_number']; 
              $card_exp_month = $_POST['card_exp_month']; 
              $card_exp_year = $_POST['card_exp_year']; 
              $card_cvc = $_POST['card_cvc']; 
              
              // Include Stripe PHP library 
              require_once 'stripe-php/init.php'; 
              
              // Set API key 
              \Stripe\Stripe::setApiKey(STRIPE_API_KEY); 
              
              // Add customer to stripe 
              $customer = \Stripe\Customer::create(array( 
                  'email' => $email, 
                  'source'  => $token 
              )); 
              
              // Unique order ID 
              $orderID = strtoupper(str_replace('.','',uniqid('', true))); 
              
              // Convert price to cents 
              $planprice = ($planprice*100); 
              
              // Charge a credit or a debit card 
              $charge = \Stripe\Charge::create(array( 
                  'customer' => $customer->id, 
                  'amount'   => $planprice, 
                  'currency' => $currency, 
                  'description' => $planname, 
                  'metadata' => array( 
                      'order_id' => $orderID 
                  ) 
              )); 
              
              // Retrieve charge details 
              $chargeJson = $charge->jsonSerialize(); 
          
              // Check whether the charge is successful 
              if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){ 
                  // Order details  
                  $transactionID = $chargeJson['balance_transaction']; 
                  $paidAmount = $chargeJson['amount']/100; 
                  $paidCurrency = $chargeJson['currency']; 
                  $payment_status = $chargeJson['status'];
                  // Insert tansaction data into the database 
                  $last_id = "SELECT id FROM `orders` ORDER BY id DESC";
                  $last_id = $con->query($last_id);
                  $last_id = mysqli_fetch_assoc($last_id);
                  $current_id = (int)$last_id['id']+1;
                  $ordersql = "INSERT INTO `orders` (`user_id`, `name`, `username`, `plan_name`, `plan_price`, `paid_amount`, `paid_amount_currency`, `payment_status`, `txn_id`, `card_number`, `card_exp_month`, `card_exp_year`) VALUES ('$useroid', '$name', '$email', '$planname', '$planprice', '$paidAmount', '$paidCurrency', '$payment_status', '$transactionID', '$card_number', '$card_exp_month', '$card_exp_year')";
                  $orderresult = $con->query($ordersql);
                  $sql = "UPDATE `users` SET `paid` = '1', `subscription` = '$subscription' WHERE `username` = '$email' AND `role` = 'Admin'"; 
                  $insert = $con->query($sql);
                  $payment_id = $insert; 
                  
                  // If the order is successful 
                  if($payment_status == 'succeeded'){ 
                      $ordStatus = 'success'; 
                      $statusMsg = 'Your Payment has been Successful!'; 
                  }else{ 
                      $statusMsg = "Your Payment has Failed!"; 
                  } 
              }else{ 
                  //print '<pre>';print_r($chargeJson); 
                  $statusMsg = "Transaction has been failed!"; 
              } 
          }else{ 
              $statusMsg = "Error on form submission."; 
          } 
          ?>
            <?php 
                if(isset($_GET['success']) && $_GET['success'] == 1){
                    echo '<div class="alert alert-success" role="alert">';
                    echo 'Source <i>'.$_GET['source'].'</i> has been created successfuly';
                    echo '</div>';
                }
            ?>    
          <!-- general form elements -->
          <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Subscription</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <!-- input states -->
                  <div class="form-group">
                    <label class="control-label"> Plan</label>
                    <input type="text" class="form-control is-warning" value="<?=$planname;?>" disabled>
                  </div>
                  <div class="form-group">
                    <label class="control-label"> Price</label>
                    <input type="text" class="form-control is-warning" value="<?=$paidAmount." ".$currency;?>" disabled>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="inputWarning"> Status</label>
                    <input type="text" class="form-control is-warning" value="<?=$statusMsg;?>" disabled>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name"> Reference Number</label>
                    <input type="text" class="form-control is-warning" value="<?=$current_id;?>" disabled>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name"> Transaction ID</label>
                    <input type="text" class="form-control is-warning" value="<?=$transactionID;?>" disabled>
                  </div>
                  <h1 class="display-3">Thank You!</h1>
              </div>
              <!-- /.card-body -->
            </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
// Set your publishable key
Stripe.setPublishableKey('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

// Callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        // Enable the submit button
        $('#payBtn').removeAttr("disabled");
        // Display the errors on the form
        $(".payment-status").html('<p>'+response.error.message+'</p>');
    } else {
        var form$ = $("#paymentFrm");
        // Get token id
        var token = response.id;
        // Insert the token into the form
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        // Submit form to the server
        form$.get(0).submit();
    }
}

$(document).ready(function() {
    // On form submit
    $("#paymentFrm").submit(function() {
        // Disable the submit button to prevent repeated clicks
        $('#payBtn').attr("disabled", "disabled");

        // Create single-use token to charge the user
        Stripe.createToken({
            number: $('#card_number').val(),
            exp_month: $('#card_exp_month').val(),
            exp_year: $('#card_exp_year').val(),
            cvc: $('#card_cvc').val()
        }, stripeResponseHandler);

        // Submit from callback
        return false;
    });
});
</script>
  <?php include('../footer.php'); ?>