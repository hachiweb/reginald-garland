<?php
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: ../index.php");
}
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https://"; 
else
$link = "http://"; 
$site_url = $link.$_SERVER['HTTP_HOST'];
include '../../dbconfig.php';

$email = $_SESSION['username'];
$userid = $_SESSION['id'];
$db = $con;
$today = time();
$planprice = $_SESSION['price'];

$fetchresult = $db->query("SELECT * FROM users WHERE id = '$userid' AND role = 'Admin'");
$fetchrow = $fetchresult->fetch_assoc();
$subscription = strtotime($fetchrow['subscription']);
$datediff = $subscription - $today;
$expire = round($datediff / (60 * 60 * 24));
$date = new DateTime(date("Y-m-d"));

if ($expire<1) {
  if ($planprice==20) {
      $date->modify('+31 day');
      $subscription = $date->format('Y-m-d');
  }
  if ($planprice==220) {
      $date->modify('+366 day');
      $subscription = $date->format('Y-m-d');
  }
} else {
  if ($planprice==20) {
      $oldsub = $expire+31;
      $date->modify($oldsub.' day');
      $subscription = $date->format('Y-m-d');
  }
  if ($planprice==220) {
      $oldsub = $expire+366;
      $date->modify($oldsub.' day');
      $subscription = $date->format('Y-m-d');
  }
}
$sql = $db->query("UPDATE users SET paid = '1', subscription = '$subscription' WHERE username = '$email' AND role = 'Admin'");

function removeSession() {
    unset($_SESSION['price']);
}

?>

	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://js.stripe.com/v2/"></script>
<!-- jQuery is used only for this example; it isn't required to use Stripe -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<h1 style="text-align:center" class="alert alert-success">Your subscription is successfully paid.</h1>
 <a onclick = 'removeSession()' style="margin-left:50%" href="<?=$site_url?>" class="btn btn-info">Home</a>
  <!--  <h1>Your Payment ID - <//?php echo $last_insert_id; ?>.</h1>
    <div class="card card-warning">
      <div class="card-header">
        <h3 class="card-title">Subscription</h3>
      </div>
      <!-- /.card-header ->
      <div class="card-body">
          <!-- input states ->
          <div class="form-group">
            <label class="control-label"> Plan</label>
            <input type="text" class="form-control is-warning" value="<//?php echo $planname;?>" disabled>
          </div>
          <div class="form-group">
            <label class="control-label"> Price</label>
            <input type="text" class="form-control is-warning" value="<//?php echo $planprice." ".$currency_code;?>" disabled>
          </div>
          <div class="form-group">
            <label class="control-label" for="inputWarning"> Status</label>
            <input type="text" class="form-control is-warning" value="<//?php echo$payment_status;?>" disabled>
          </div>
          <div class="form-group">
            <label class="control-label" for="name"> Reference Number</label>
            <input type="text" class="form-control is-warning" value="<//?php echo $last_insert_id;?>" disabled>
          </div>
          <div class="form-group">
            <label class="control-label" for="name"> Transaction ID</label>
            <input type="text" class="form-control is-warning" value="<//?php echo $txn_id;?>" disabled>
          </div>
          <h1 class="display-3">Thank You!</h1>
      </div>
      <!-- /.card-body ->
    </div>-->
<?php
//}else{
?>
	<!--<h1>Your payment has failed.</h1>-->
<?php
//}
?>