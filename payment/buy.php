<?php
session_start();
if ($_SESSION["role"] != "Admin") {
  header("location: ../index.php");
}
    $page = 'buy';
    $planprice = 0;
    include('../header.php');
    $plan = $_POST['plan'];
    $currency = "USD";
    if ($plan=="1") {
      $planname = "One Month";
      $planprice = 20;
    }
    if ($plan=="2") {
      $planname = "One Year";
      $planprice = 220;
    }
    setcookie("price", "planprice");

    //Set useful variables for paypal form
    $paypal_link = 'paypal/payments.php'; //'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
    $paypal_username = 'garlandprosolutions@gmail.com'; //Business Email
    include('config.php');
?>
<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://js.stripe.com/v2/"></script>
    <!-- jQuery is used only for this example; it isn't required to use Stripe -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Buy Plan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo $site_url ?>">Home</a></li>
            <li class="breadcrumb-item active">Buy Plan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <!-- left column -->
        <div class="col-md-8 mx-auto">
            <?php
                if(isset($_GET['success']) && $_GET['success'] == 1){
                    echo '<div class="alert alert-success" role="alert">';
                    echo 'Source <i>'.$_GET['source'].'</i> has been created successfuly';
                    echo '</div>';
                }
            ?>
          <!-- general form elements -->
          <div class="card3">
    <form action="<?php echo $paypal_link; ?>" method="post">

        <!-- Identify your business so that you can collect the payments. -->
        <?php /*<input type="hidden" name="business" value="<?php echo $paypal_username; ?>"> */?>
        
        <input type="hidden" name="cmd" value="_xclick" />
        <input type="hidden" name="no_note" value="1" />
        <input type="hidden" name="lc" value="UK" />
        <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
        
        <!-- Specify a Buy Now button. -->
        <!-- <input type="hidden" name="cmd" value="_xclick"> -->

        <!-- Specify details about the item that buyers will purchase. -->
       <?php /* <input type="hidden" name="planname" value="<?php echo $planname; ?>"> */?>
        <input type="hidden" name="item_name" value="GPS Dealers subscription">
        <input type="hidden" name="amount" value="<?=$planprice;?>">
        <input type="hidden" name="currency_code" value="USD">

        <!-- Specify URLs -->
     <?php /*   <input type='hidden' name='cancel_return' value='<?php echo $site_url;?>/payment/paypal/paypal_cancel.php'>
		<input type='hidden' name='return' value='<?php echo $site_url;?>/payment/paypal/paypal_success.php'> */?>


        <!-- Display the payment button. -->
        <!-- <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
        <script>paypal.Buttons().render('body');</script> -->
        <input type="hidden" name="item_number" value="123456" />
        <input type="submit" class="btn btn-warning" name="submit" border="0" value="Pay with PayPal"
         alt="PayPal - The safer, easier way to pay online">

    </form>
              <!--<a href="http://page3salon.com/site/payment/paypal/" class="btn btn btn-success mt-2 mx-auto" id="payBtn"style="display: block;width: 25%; margin: 0px 20px 20px 0px;"><span class="px-3">Pay with PayPal</span></a>-->
              </div>
              <div class="card3">
            <p style="text-align:center;font-size:30px;"> -OR-</p>
              </div>
          <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Subscription</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form role="form" action="success.php" method="POST" id="paymentFrm">
                  <!-- input states -->
                  <div class="form-group">
                    <label class="control-label"> Plan</label>
                    <input type="text" class="form-control is-warning" value="<?=$planname;?>" disabled>
                  </div>
                  <div class="form-group">
                    <label class="control-label"> Price</label>
                    <input type="text" class="form-control is-warning" value="<?=$planprice." ".$currency;?>" disabled>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name"> NAME</label>
                    <input type="text" class="form-control is-warning" name="name" id="name" placeholder="Enter ..." required>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="inputWarning"> CARD NUMBER</label>
                    <input type="text" class="form-control is-warning" name="card_number" id="card_number" placeholder="eg. 1234 1234 1234 1234" autocomplete="off" required>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                     <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>EXPIRY DATE</label>
                          <input type="text" class="form-control is-warning" name="card_exp_month" id="card_exp_month" placeholder="MM" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                        <label class="mt-3"></label>
                          <input type="text" class="form-control is-warning" name="card_exp_year" id="card_exp_year" placeholder="YYYY" required>
                        </div>
                      </div>
                     </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>CVC CODE</label>
                        <input type="text" class="form-control is-warning" name="card_cvc" id="card_cvc" placeholder="CVC" autocomplete="off" required>
                      </div>
                    </div>
                    <input type="hidden" class="form-control is-warning" name="JwjNzETp" value="<?=$planname;?>">
                    <input type="hidden" class="form-control is-warning" name="WGgu2PIo" value="<?=$planprice;?>">
                    <input type="hidden" class="form-control is-warning" name="UHpZnDsb" value="<?=$currency;?>">
                    <button type="submit" class="btn btn btn-success mt-2 mx-auto" id="payBtn"><span class="px-3">Submit Payment</span></button>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
// Set your publishable key
Stripe.setPublishableKey('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

// Callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        // Enable the submit button
        $('#payBtn').removeAttr("disabled");
        // Display the errors on the form
        $(".payment-status").html('<p>'+response.error.message+'</p>');
    } else {
        var form$ = $("#paymentFrm");
        // Get token id
        var token = response.id;
        // Insert the token into the form
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        // Submit form to the server
        form$.get(0).submit();
    }
}

$(document).ready(function() {
    // On form submit
    $("#paymentFrm").submit(function() {
        // Disable the submit button to prevent repeated clicks
        $('#payBtn').attr("disabled", "disabled");

        // Create single-use token to charge the user
        Stripe.createToken({
            number: $('#card_number').val(),
            exp_month: $('#card_exp_month').val(),
            exp_year: $('#card_exp_year').val(),
            cvc: $('#card_cvc').val()
        }, stripeResponseHandler);

        // Submit from callback
        return false;
    });
});
</script>
  <?php include('../footer.php'); ?>