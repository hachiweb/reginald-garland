<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-dark text-white">
                <h3 class="modal-title">Contact Us</h3>
                <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
            </div>
            <form id="contact">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-6 col-sm-6 col-12 border">
                            <div class="d-flex justify-content-between p-3">
                                <h3 class="">Send us a message</h3>
                                <h3><i class="far fa-envelope"></i></h3>
                            </div>
                            <div class='row p-3'>
                                <div class="col-sm-12">
                                    <p>For any questions, comments, or concerns please contact us. We will get back to you as soon as possible. We usually respond within one business-day.</p>
                                    <h6 class="italic-set">Required Field<span class="text-danger">*</span></h5>
                                </div>
                                <div class="col-sm-6">
                                    <label for="first_name" class="mb-0 mt-3 italic-set">First Name<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name">
                                </div>
                                <div class="col-sm-6">
                                    <label for="last_name" class="mb-0 mt-3 italic-set">Last Name<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name">
                                </div>
                                <div class="col-sm-6">
                                    <label for="email" class="mb-0 mt-3 italic-set">Email<span
                                            class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                                <div class="col-sm-6">
                                    <label for="phone" class="mb-0 mt-3 italic-set">Phone<span
                                            class="text-danger">*</span></label>
                                    <input type="number" class="form-control" id="phone" name="phone">
                                </div>
                                <div class="col-sm-12">
                                    <label for="company" class="mb-0 mt-3 italic-set">Company / Dealership<span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="company" name="company">
                                </div>
                                <div class="col-sm-12">
                                    <label for="title" class="mb-0 mt-3">Title</label>
                                    <input type="text" class="form-control" id="title" name="title">
                                </div>
                                <div class="col-sm-12">
                                    <label for="comments" class="mb-0 mt-3">Comments</label>
                                    <textarea name="comments" id="comments" rows="5" class="form-control"></textarea>
                                    *Please include first and last name, title, name of organization, email address (if applicable), best contact number to reach you and the preferred time you would like to be contacted.
                                </div>
                                <div class="modal-footer m-auto">
                                    <button type="submit" class="btn btn-dark border btn-lg"> Submit </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 border txt-padd-mar cotactuscolor">
                            <div class="txt-padd-mar-set">
                                <h2>Write To Us</h2>
                                <h3>Headquarters</h3>
                                <h5>1423 Yucaipa Circle,</h5>
                                <h5>Valdosta, GA 31601</h5>
                                <h5>(Use for billing inquiries).</h5>
                                <div class="social-icon">
                                    <i class="fab fa-facebook-f"></i>
                                    <i class="fab fa-linkedin-in"></i>
                                    <i class="fab fa-twitter"></i>
                                    <i class="fab fa-youtube"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>